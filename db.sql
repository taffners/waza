CREATE TABLE IF NOT EXISTS twenty_four_hour_plan_table (
twenty_four_hour_plan_id int NOT NULL AUTO_INCREMENT,
-- foreign key
user_id int,

-- data	
eat_healthy_score int,
move_daily_score int,
mindset_score int,
plan_date date,
follow_plan VARCHAR(1),
why_or_why_not_follow_plan text,
excuses_using VARCHAR(255)
plan_for_success_tomorrow VARCHAR(255),

time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
PRIMARY KEY (twenty_four_hour_plan_id),
UNIQUE KEY twenty_four_hour_plan_id (twenty_four_hour_plan_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1



CREATE TABLE IF NOT EXISTS log_table (
log_id int NOT NULL AUTO_INCREMENT,
-- foreign key
user_id int,

-- data	
ip_address VARCHAR(16),
action VARCHAR(20),
msg VARCHAR(255),

time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
PRIMARY KEY (log_id),
UNIQUE KEY log_id (log_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1


CREATE TABLE IF NOT EXISTS comment_table (
	comment_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	user_id int,
	
	-- data	
	comment_ref int,
	comment_type varchar(30),
	comment text,
	status varchar(30),

	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (comment_id),
	UNIQUE KEY comment_id (comment_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;


CREATE TABLE IF NOT EXISTS permission_user_xref (
	permission_user_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	user_id int,
	permission_id int,
	-- data	

	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (permission_user_id),
	UNIQUE KEY permission_user_id (permission_user_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS permission_table (
	permission_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key

	-- data	
	permission varchar(15),

	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (permission_id),
	UNIQUE KEY permission_id (permission_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS login_table(
	login_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key

	-- data	
	email_address varchar(40),
	ip_address varchar(16),
	ip_loc varchar(30),
	status varchar(20),

	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (login_id),
	UNIQUE KEY login_id (login_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS top_passwords_used_table(
	top_password_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key
	email_address varchar(40),
	-- data	
	password varchar(60),
	
	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (top_password_id),
	UNIQUE KEY top_password_id (top_password_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS user_table(
	user_id int NOT NULL AUTO_INCREMENT,
	
	-- foreign key

	-- data	
	first_name varchar(50),
	last_name varchar(50),
	email_address varchar(40) UNIQUE,
	credentials varchar(255),
	title varchar(50),
	password_need_reset varchar(1) DEFAULT 1,
	password varchar(60) DEFAULT 'ba816f76d6c8402851d3b849fa690892',

	time_stamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY (user_id),
	UNIQUE KEY user_id (user_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

INSERT INTO user_table (first_name, last_name, email_address, credentials, title) VALUES
('Samantha', 'Taffner', 'taffners@gmail.com', 'MS', 'Bioinformatics Analyst');
