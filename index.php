<?php
;
	ob_start();
	require_once('config.php');

	require_once('logic/'.$page.'.php');
	
	require_once('templates/header.php');

	require_once('templates/'.$page.'.php');

	require_once('templates/footer.php');		

	$db->close_db_conn();
	ob_end_flush();
?>
