<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

			<form class="form" method="post" class="form-horizontal">
				<fieldset>
					<legend >
						<?= $page_title;?>
					</legend>			

				<?php
				foreach($meals as $key => $meal)
				{
					$nested_meal = array(0 => $meal);
				?>
					<div class="form-group row alert alert-info">
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<label class="form-control-label"><?= $meal['meal_name'];?>:</label>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
									<label for="meal[<?= $meal['meal_id'];?>][calorie_count]" class="form-control-label">Calories</label>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
									<input type="text" class="form-control no-spaces" id="soft_path_num" name="meal[<?= $meal['meal_id'];?>][calorie_count]" maxlength="50" value="<?= $utils->GetValueForUpdateInput($nested_meal, 'calorie_count');?>"/>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-12 col-lg-12">
									<textarea class="form-control" name="meal[<?= $meal['meal_id'];?>][meal_description]" rows="4" maxlength="255" style="margin-bottom: 10px;"><?= $meal['meal_description'];?></textarea>
								</div>
							</div>
						</div>

						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
							<div class="form-group row">
								<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
									<label for="meal[<?= $meal['meal_id'];?>][wait_tile_hungry]" class="form-control-label">Did I wait for hunger?</label>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
									<div class="radio">
										<label class="radio-inline"><input type="radio" name="meal[<?= $meal['meal_id'];?>][wait_tile_hungry]" value="Y" <?php
										if(isset($meal['wait_tile_hungry']) && $meal['wait_tile_hungry'] == 'Y')
										{
											echo 'checked'; 								
										}
										?>>Yes</label>
									
										<label class="radio-inline"><input type="radio" name="meal[<?= $meal['meal_id'];?>][wait_tile_hungry]" value="N" <?php
										if(isset($meal['wait_tile_hungry']) && $meal['wait_tile_hungry'] == 'N')
										{
											echo 'checked'; 								
										}
										?>>No</label>
									</div>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
									<label for="meal[<?= $meal['meal_id'];?>][stop_when_satisfied]" class="form-control-label">Did I stop at satisfied?</label>
								</div>
								
								<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
									<div class="radio">
										<label class="radio-inline"><input type="radio" name="meal[<?= $meal['meal_id'];?>][stop_when_satisfied]" value="Y" <?php
										if(isset($meal['stop_when_satisfied']) && $meal['stop_when_satisfied'] == 'Y')
										{
											echo 'checked'; 								
										}
										?>>Yes</label>
									
										<label class="radio-inline"><input type="radio" name="meal[<?= $meal['meal_id'];?>][stop_when_satisfied]" value="N" <?php
										if(isset($meal['stop_when_satisfied']) && $meal['stop_when_satisfied'] == 'N')
										{
											echo 'checked'; 								
										}
										?>>No</label>
									</div>
								</div>
							</div>	
						</div>
					</div>	

				<?php				
				}
				?>
					<div class="form-group row alert alert-success">
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
							<div class="form-group row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<label class="form-control-label">Movement:</label>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<textarea class="form-control" name="description_of_movement" rows="4" maxlength="255" style="margin-bottom: 10px;"><?= $twenty_four_hour_plan[0]['description_of_movement'];?></textarea>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
							<div class="form-group row">
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<label for="num_steps_completed" class="form-control-label"># Steps:</label>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">									
									<div class="input-group">
						                <span class="input-group-prepend">
						                    <button type="button" class="btn btn-outline-secondary btn-number" disabled="disabled" data-type="minus" data-field="num_steps_completed" data-step="500">
						                        <span class="fa fa-minus"> 500</span>
						                    </button>
						                </span>
						                <input type="text" name="num_steps_completed" class="form-control input-number" value="<?= $utils->GetValueForUpdateInput($twenty_four_hour_plan, 'num_steps_completed');?>" min="1">
						                <span class="input-group-append">
						                    <button type="button" class="btn btn-outline-secondary btn-number" data-type="plus" data-field="num_steps_completed" data-step="500">
						                        <span class="fa fa-plus"> 500</span>
						                    </button>
						                </span>
						            </div>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<label for="num_work_outs_completed" class="form-control-label"># work outs:</label>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">									
									<div class="input-group">
						                <span class="input-group-prepend">
						                    <button type="button" class="btn btn-outline-secondary btn-number" disabled="disabled" data-type="minus" data-field="num_work_outs_completed" data-step="1">
						                        <span class="fa fa-minus"></span>
						                    </button>
						                </span>
						                <input type="text" name="num_work_outs_completed" class="form-control input-number" value="<?= $utils->GetValueForUpdateInput($twenty_four_hour_plan, 'num_work_outs_completed');?>" min="1">
						                <span class="input-group-append">
						                    <button type="button" class="btn btn-outline-secondary btn-number" data-type="plus" data-field="num_work_outs_completed" data-step="1">
						                        <span class="fa fa-plus"></span>
						                    </button>
						                </span>
						            </div>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
									<label for="num_strength_training_completed" class="form-control-label"># strength training:</label>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">		
									<div class="input-group">
						                <span class="input-group-prepend">
						                    <button type="button" class="btn btn-outline-secondary btn-number" disabled="disabled" data-type="minus" data-field="num_strength_training_completed" data-step="1">
						                        <span class="fa fa-minus"></span>
						                    </button>
						                </span>
						                <input type="text" name="num_strength_training_completed" class="form-control input-number" value="<?= $utils->GetValueForUpdateInput($twenty_four_hour_plan, 'num_strength_training_completed');?>" min="1">
						                <span class="input-group-append">
						                    <button type="button" class="btn btn-outline-secondary btn-number" data-type="plus" data-field="num_strength_training_completed" data-step="1">
						                        <span class="fa fa-plus"></span>
						                    </button>
						                </span>
						            </div>																
								</div>
							</div>
						</div>	
					</div>

					
					<div class="form-group row">
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
							<div class="form-group row">
								<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
									<label for="goals" class="form-control-label">My three goals for today:</label>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
									<textarea class="form-control" name="goals" rows="8" maxlength="6000" style="margin-bottom: 10px;"><?= $twenty_four_hour_plan[0]['goals'];?></textarea>
								</div>
							</div>
							<div class="form-group row">
								<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
									<label for="whys" class="form-control-label">My five "whys" for today:</label>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
									<textarea class="form-control" name="whys" rows="10" maxlength="6000" style="margin-bottom: 10px;"><?= $twenty_four_hour_plan[0]['whys'];?></textarea>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
							<div class="form-group row">
								<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
									<label for="follow_plan" class="form-control-label">Did I follow my plan today?</label>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
									<div class="radio">
										<label class="radio-inline"><input type="radio" name="follow_plan" value="Y" <?php
										if(isset($twenty_four_hour_plan[0]['follow_plan']) && $twenty_four_hour_plan[0]['follow_plan'] == 'Y')
										{
											echo 'checked'; 								
										}
										?>>Yes</label>
									
										<label class="radio-inline"><input type="radio" name="follow_plan" value="N" <?php
										if(isset($twenty_four_hour_plan[0]['follow_plan']) && $twenty_four_hour_plan[0]['follow_plan'] == 'N')
										{
											echo 'checked'; 								
										}
										?>>No</label>
									</div>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
									<label for="why_or_why_not_follow_plan" class="form-control-label">Why or why not?</label>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
									<textarea class="form-control" name="why_or_why_not_follow_plan" rows="4" maxlength="6000" style="margin-bottom: 10px;"><?= $twenty_four_hour_plan[0]['why_or_why_not_follow_plan'];?></textarea>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
									<label for="excuses_using" class="form-control-label">Are there any excuses I am using to eat when I'm not hungry?</label>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
									<textarea class="form-control" name="excuses_using" rows="4" maxlength="255" style="margin-bottom: 10px;"><?= $twenty_four_hour_plan[0]['excuses_using'];?></textarea>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
									<label for="plan_for_success_tomorrow" class="form-control-label">How can I set myself up for success tomorrow?</label>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
									<textarea class="form-control" name="plan_for_success_tomorrow" rows="4" maxlength="255" style="margin-bottom: 10px;"><?= $twenty_four_hour_plan[0]['plan_for_success_tomorrow'];?></textarea>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
									<label for="permission" class="form-control-label">Eat Healthy: </label><br>
									<input type="radio" name="eat_healthy_score" value="0" class="sad smileys" <?php
									if(isset($twenty_four_hour_plan[0]['eat_healthy_score']) && $twenty_four_hour_plan[0]['eat_healthy_score'] == 0)
									{
										echo 'checked'; 								
									}
									?>>
									<input type="radio" name="eat_healthy_score" value="1" class="neutral smileys" <?php
									if(isset($twenty_four_hour_plan[0]['eat_healthy_score']) && $twenty_four_hour_plan[0]['eat_healthy_score'] == 1)
									{
										echo 'checked'; 								
									}
									?>>
									<input type="radio" name="eat_healthy_score" value="2" class="happy smileys" <?php
									if(isset($twenty_four_hour_plan[0]['eat_healthy_score']) && $twenty_four_hour_plan[0]['eat_healthy_score'] == 2)
									{
										echo 'checked'; 								
									}
									?>>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
									<label for="permission" class="form-control-label">Move Daily: </label><br>
									<input type="radio" name="move_daily_score" value="0" class="sad smileys" <?php
									if(isset($twenty_four_hour_plan[0]['move_daily_score']) && $twenty_four_hour_plan[0]['move_daily_score'] == 0)
									{
										echo 'checked'; 								
									}
									?>>
									<input type="radio" name="move_daily_score" value="1" class="neutral smileys" <?php
									if(isset($twenty_four_hour_plan[0]['move_daily_score']) && $twenty_four_hour_plan[0]['move_daily_score'] == 1)
									{
										echo 'checked'; 								
									}
									?>>
									<input type="radio" name="move_daily_score" value="2" class="happy smileys" <?php
									if(isset($twenty_four_hour_plan[0]['move_daily_score']) && $twenty_four_hour_plan[0]['move_daily_score'] == 2)
									{
										echo 'checked'; 								
									}
									?>>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
									<label for="permission" class="form-control-label">Mindset: </label><br>
									<input type="radio" name="mindset_score" value="0" class="sad smileys" <?php
									if(isset($twenty_four_hour_plan[0]['mindset_score']) && $twenty_four_hour_plan[0]['mindset_score'] == 0)
									{
										echo 'checked'; 								
									}
									?>>
									<input type="radio" name="mindset_score" value="1" class="neutral smileys" <?php
									if(isset($twenty_four_hour_plan[0]['mindset_score']) && $twenty_four_hour_plan[0]['mindset_score'] == 1)
									{
										echo 'checked'; 								
									}
									?>>
									<input type="radio" name="mindset_score" value="2" class="happy smileys" <?php
									if(isset($twenty_four_hour_plan[0]['mindset_score']) && $twenty_four_hour_plan[0]['mindset_score'] == 2)
									{
										echo 'checked'; 								
									}
									?>>
								</div>
								
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
							<button type="submit" id="<?= $page;?>_submit" name="<?= $page;?>_submit" value="Submit"  class="btn btn-primary btn-primary-hover">
								Submit
							</button>
						</div>
					</div>
				</fieldset>
			</form>
		</div>
	</div>
</div>