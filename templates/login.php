<?php
	if ($brute_force_occuring)
	{
?>
	<div class="container-fluid">

		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<img src="images/homer_doh.jpg" alt="homer doh cartoon">
				<br>
				<h1>Since to many failed logins have occurred recently <?= htmlspecialchars(SITE_TITLE);?> is temporarily not available.</h1>
				<br><br>
				This application complies with PCI Data Security Standards.   
				<ul>
					<li>8.5.13 (Limit repeated access attempts by locking out after not more than five attempts)</li>
					<li>8.5.14 (Set the lockout duration to thirty minutes or until administrator enables the account).</li>
				</ul>
			</div>
		</div>
	</div>
<?php
	}
	else
	{
?>
	<div class='container-fluid' id="loginContainer" style="padding: 0px;">
		

	<?php
		if (isset($_GET['error']) && !empty($_GET['error']) && $_GET['error'] === 'redirect_wrong_app')
		{
	?>
		<div class='row' >
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="alert alert-danger">
					<span class="glyphicon glyphicon-warning-sign"></span> It looks like you tried to login to the wrong App. 
				</div>
			</div>
		</div>
	<?php
		}
	?>

	<?php
		if (isset($_GET['inactive_time']))
		{
	?>
		<div class='row' >
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="alert alert-danger">
					<span class="glyphicon glyphicon-warning-sign"></span> No activity within <?= $_GET['inactive_time'];?> seconds.  Please log in again.
				</div>
			</div>
		</div>
	<?php
		}
	?>		
		<div id="login-div" class="row" style="text-align:justify;border-radius:10px;font-size:18px;padding-left:20px;">
			<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
				<div style="margin-top:50px;">
					<?= SITE_TITLE; ?> is a tool to speed up Clinical Microbiology Research at University of Rochester Medical Center.    
				</div>  

				<!-- Hide all login information if they are using a browser which is not supported -->
				<div id="all_login_fields" class="show" style="margin-top:80px;">
					<div class="FormBoxCustom">
						<form class="form" method="post" class="form-horizontal">
							
							<div class="form-group row">
								<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
									<label for="inputEmailAddress" class="form-control-label">Email Address: </label>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
									<input type='email' id="inputEmailAddress" name='email_address' class="form-control" placeholder='Full Email Address' required>
								</div>
							</div>
							<div class="form-group row">
								<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
									<label for="password" class="form-control-label">Password:</label>
								</div>

								<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
									<input type="password" class="form-control" name="password" placeholder="Password" required>
								</div>
							</div>
							<div class="form-group row">
								<div class="col-xs-8 col-sm-8 col-md-8">
									<button type="submit" name="login_submit" value="submit" class='btn btn-primary btn-lg'>Sign In</button>
								</div>
							</div>	
						</form>
					</div>
				</div>
			</div>

			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
				<img src="images/login_guy.png" id="login-img" align="right">
			</div>
		</div>

		<div id="browser_warning" class="d-none">
			<div class="col-xs-12 col-sm-12 col-md-12 pull-left alert alert-danger">
				Currently only the <a href="https://www.google.com/chrome/browser/desktop/"> Chrome web browser </a> is supported.  Please switch your browser.
				<div class="row">
					<div class="col-xs-2 col-sm-2 col-md-2">
						<a href="https://www.google.com/chrome/browser/desktop/">
							<img src="images/Chrome.png" alt="chrome" height="50" width="50">
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php
	}
?>
