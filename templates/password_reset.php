<div class="container-fluid">

	<div class="row">
		
		<div  class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="height:500px;">
			<form class="form" method="post" class="form-horizontal">
				<fieldset>
					<legend>
						Reset Password
					</legend>
					
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="email_address" class="form-control-label">email address:<span class="required-field">*</span> </label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="email_address" name="email_address" maxlength="40" required placeholder="Enter full email address" />
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="current_password" class="form-control-label">Current Password:<span class="required-field">*</span> </label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="password" class="form-control"  name="current_password" maxlength="60" required placeholder="Enter current password" />
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="new_password1" class="form-control-label">New Password 1:<span class="required-field">*</span> </label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="password" class="form-control"  name="new_password1" minlength="7" maxlength="60" required placeholder="Enter new password"/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="new_password2" class="form-control-label">New Password 2:<span class="required-field">*</span> </label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="password" class="form-control"  name="new_password2" minlength="7" maxlength="60" required placeholder="Enter new password"/>
						</div>
					</div>
					<div class="col-xs-offset-4 col-sm-offset-4 col-md-offset-4 col-lg-offset-4 col-xs-2 col-sm-2 col-md-2 col-lg-2">
						<button type="submit" id="<?= $page;?>_submit" name="<?= $page;?>_submit" value="Submit"  class="btn btn-primary btn-primary-hover">
							Done
						</button>
					</div>
				</fieldset>				
			</form>
		</div>

	</div>
</div>