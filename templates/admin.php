<?php
     if (isset($user_permssions) && strpos($user_permssions, 'admin') !== false)
     {                        
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<form id="add_new_user" class="form" method="post" class="form-horizontal">
				<fieldset>
					<legend class='show'>
						Add User
					</legend>

					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="first_name" class="form-control-label">First Name:</label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="first_name" name="first_name" maxlength="50" required/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="last_name" class="form-control-label">last Name:</label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="last_name" name="last_name" maxlength="50" required/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="email_address" class="form-control-label">email address:</label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="email_address" name="email_address" maxlength="40" required/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="credentials" class="form-control-label">credentials:</label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="credentials" name="credentials" maxlength="255"/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="title" class="form-control-label">title:</label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="title" name="title" maxlength="50"/>
						</div>
					</div>					
					<div class="row">	
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label class="form-control-label">CAPTCHA:</label>
						</div>

						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<img src="<?= $src_captcha; ?>"/>	
						</div>	
					</div>	
					<div class="form-group row">
						<div class="col-xs-12 col-sm-4 col-md-4">
							<label for="captcha" class="form-control-label">Type CAPTCHA text here:</label>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 pull-left">
							<input type="text" class="form-control" id="captcha" name="captcha" maxlength="8" required/>
						</div>
					</div>	
				     <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
						<button type="submit" name="add_user_submit" value="Submit"  class="btn btn-primary btn-primary-hover">
							Add User
						</button>
					</div>	
				</fieldset>
			</form>
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<fieldset>
				<legend>
					Unlock user Account
				</legend>		
<?php

	if (isset($user_list) && isset($reporter_permission_list))
	{
?>
				<table class="formated_table sort_table" style="margin-bottom:20px;">
					<thead>
						<th>User Name</th>
						<th>Email Address</th>
						<th>Unlock Account</th>
						<th>Account Status</th>
						<th>Lock Account</th>
			<!-- Add a new header column for each reporter_permission_list -->
		<?php
			foreach($reporter_permission_list as $key => $permission)
			{ 
		?>

						<th><?= $permission['permission'];?></th>
		<?php
			}
		?>
					</thead>
					<tbody>
<?php
			foreach ($user_list as $key => $ea_user)
			{				
				// find user permission
				$this_user_permission = $db->listAll('user-permissions', $ea_user['user_id']);
				if (empty($this_user_permission))
				{
					$this_user_permission = '';
				}
				else
				{
					$this_user_permission = $this_user_permission[0]['permissions'];
				}
?>
						<tr>
							<td><?= $ea_user['user_name'];?></td>
							<?=$utils->toggleMoreLess($ea_user['email_address'], 'email_address', $key);?>
							<td><button class="unlock-account btn btn-primary" data-email-address="<?= $ea_user['email_address'];?>">Unlock</button></td>
							<td><?php
							if ($ea_user['password_need_reset'] == 0)
							{
								echo 'unlocked';
							}
							else if ($ea_user['password_need_reset'] == 1)
							{
								echo 'pass reset';
							}
							else if ($ea_user['password_need_reset'] == 2)
							{
								echo '<span class="fas fa-lock"></span>';
							}
							?></td>
							<td><button class="lock-account btn btn-primary" data-email-address="<?= $ea_user['email_address'];?>"><span class="fas fa-lock"></span></button></td>							
							<!-- Add Permission -->
			<?php
				foreach($reporter_permission_list as $key => $permission)
				{ 
			?>
							<td><button  class="add-permission btn <?php
								if (isset($this_user_permission) && strpos($this_user_permission, $permission['permission']) !== false)
								{
									echo 'btn-success';
								}
								else 
								{
									echo 'btn-danger';
								}
								?>" data-user-id="<?= $ea_user['user_id'];?>" data-permission="<?=$permission['permission'];?>" data-curr-permissions="<?= $this_user_permission;?>"><span class="fas fa-plus"></span> <?=$permission['permission'];?></button>
							</td>
			<?php
				}
			?>							
						</tr>
<?php
			}
?>
					</tbody>
				</table>
<?php
	}
?>
			</fieldset>	
		</div>


	</div>
<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<fieldset>
				<legend class='show'>
					Access Summary
				</legend>	

				<div class="row">
					<div class="col-xs-6 col-sm-4 col-md-2 col-lg-12">
						<a href="?page=admin&access_filter=all" class="btn btn-primary">
							All Time                                       
                              </a>
					</div>
					<div class="col-xs-6 col-sm-4 col-md-2 col-lg-12">
						<a href="?page=admin&access_filter=month" class="btn btn-primary">
							Last Month                                     
                              </a>
					</div>
					<div class="col-xs-6 col-sm-4 col-md-2 col-lg-12">
						<a href="?page=admin&access_filter=seven_days" class="btn btn-primary">
							Last 7 Days                                       
                              </a>
					</div>
					<div class="col-xs-6 col-sm-4 col-md-2 col-lg-12">
						<a href="?page=admin&access_filter=today" class="btn btn-primary">
							Today                                    
                              </a>
					</div>
					<div class="col-xs-6 col-sm-4 col-md-2 col-lg-12">
						<a href="?page=admin&access_filter=half_hour" class="btn btn-primary">
							Last Half an Hour                                    
                              </a>
					</div>
				</div>
			
    				<div id="login_details" style="width: 900px; height: 900px;"></div>
    				<div id="time_access_bar_chart" style="width: 900px; height: 500px;"></div>

    			</fieldset>
    		</div>
    	</div>
</div>

<?php
}
?>

