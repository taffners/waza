     </body>
     <footer class="fixed-bottom d-print-none">
<?php
     if ($page != 'bed_trace_timeline')
     {
?>           
          <div class="row">
               <div class="text-center col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                    <?= SITE_TITLE; ?> <span id="site-version-num"><?= VERSION; ?></span>
               </div>
          </div>
<?php
     }
?>
          <script src="js/libraries/libraries.js?ver=<?= VERSION; ?>"></script>

<?php
     $pages_using_google_charts = array('admin', 'bed_trace_timeline');     
     if (isset($page) && in_array($page, $pages_using_google_charts))
     {
?>          
          <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<?php
     }
     if (isset($page) && $page === 'login')
     {
?>   
          <script type="text/javascript">
               

               function adjustIMGHeight(divs,img_id, set_height)
               {
                    // (array, id, int) -> resize img_id
                    // Resize img to be the same size as screen minus sections of the layout added in divs array.  Include either #, or . in divs 
                    // if necessary for ids and classes.  Provide an int for any set margins.
                    var  window_height = $(window).height(),
                         included_height = 0,
                         i;

                    for (i = 0; i < divs.length; i++)
                    {
                         included_height += $(divs[i]).height();

                    }
                    
                    $('#'+img_id).height(window_height - included_height - set_height);

                    resizeIMGHeight(divs,img_id, set_height);
               }
               
               function resizeIMGHeight(divs,img_id, set_height)
               {
                    $(window).resize(function()
                    {            
                         adjustIMGHeight(divs,img_id, set_height);
                    });
               }

               adjustIMGHeight(['header', 'nav', 'footer'],'login-img', 60);

          </script>
<?php
     }
     
     if (isset($page) && $page !== 'login')
     {
?>
          <script data-main="js/main" src="js/libraries/require.js?ver=<?= VERSION; ?>"></script>
<?php
     }
?>

     </footer>
</html>
