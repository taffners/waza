	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-center">
			<a href="?page=home#pending-sequenicng-table" type="button" class="btn btn-primary btn-primary-hover">Pending Samples</a>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-center">
			<a href="?page=home#sequencing-library-pools" type="button" class="btn btn-primary btn-primary-hover">Sequencing Library Pools</a>

		</div>
		<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-center">
			<a href="?page=home#pending-reports" type="button" class="btn btn-primary btn-primary-hover">Pending Infectious Disease Reports</a>

		</div>
		<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 text-center">
			<a href="?page=home#completed-reports" type="button" class="btn btn-primary btn-primary-hover">Completed Infectious Disease Reports</a>

		</div>
	</div>