<!DOCTYPE html>
<html>
     <head>
          <meta charset="UTF-8">
          <meta name="Description" CONTENT="">
          <meta name="robots" content="INDEX,FOLLOW">
          <meta name="googlebot" content="INDEX,FOLLOW">
          <meta name="viewport" content="width=device-width, initial-scale=1">
          <meta name="google" content="notranslate" />

          <title><?= SITE_TITLE; ?></title>

          <!-- <link rel="icon" href="images/Cool_penguin.png"> -->
          <link rel="icon" href="images/login_guy_only.png">
          <!-- <link rel="icon" href="images/login_guy_only_zoom_eyes.png"> -->
          <link rel="preload" as="font" href="fonts/glyphicons-halflings-regular.woff2" type="font/woff2" crossorigin="anonymous">
          <link rel="stylesheet" type="text/css" media="all" href="css/libraries.min.css"/>
                    
          <link rel="stylesheet" type="text/css" media="all" href="css/style.css?ver=<?= VERSION; ?>">          
     </head>
     <body>
          <header class="container-fluid">
<?php
     if ($page != 'bed_trace_timeline')
     {
?>               
               <div class="row d-print-none">
                    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 page_title d-print-none">
                         <?= $page_title; ?>
                    </div>
                    
                    <div class="hidden-xs hidden-sm col-md-4 col-lg-4 d-print-none">
                         <img src="images/urmc_logo.png" Height="40" align="right" style="margin-top:10px;">
                    </div>
               </div>
<?php
     }
?>
               <div class="row d-none d-print-block" style="margin-top:10px;">
                    <div class="col-print-3 d-none d-print-block">
                         <img src="images/urmc_logo.png" Height="40" style="margin-top:20px;">
                    </div>
                    <div class="col-print-4 d-none d-print-block">
                    </div>
                    <div class="col-print-5 d-none d-print-block" style="text-align:right;">
                         <b>MICROBIOLOGY</b><br>
                         UR MEDICINE LABS<br>
                         Department of Pathology and Laboratory Medicine<br>
                         601 Elmwood Avenue Rochester, New York 14642<br>
                         Phone: (585)275-7574 / Fax:(585)276-2272
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 page_title_print d-none d-print-block">
                         <hr class="section-mark">
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 page_title_print d-none d-print-block">
                         <?= $page_title; ?>
                    </div>
               </div>
          </header>

          <nav class="navbar navbar-expand-lg navbar-dark bg-dark ">
               <a class="navbar-brand" href="?page=home" title="Go to Main Page (Home)">
                    <img src="images/house.png" style="height:23px;width:23px;margin-right:5px;" alt="house icon">
               </a>

               <div class="collapse navbar-collapse" >
                    <ul class="navbar-nav mr-auto">                       
          <?php
          if (isset($all_pdfs) && !empty($all_pdfs))
          {
          ?>
                         <li class="nav-item dropdown">
                              <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              Show PDFS
                              </a>
                              <div class="dropdown-menu">
                    <?php
                    foreach ($all_pdfs as $key => $f)
                    {
                    ?>
                                   <a class="dropdown-item" href="?page=show_pdf&f=<?= $f;?>&loc=pdfs">
                                        <?= $f;?>
                                   </a>        
                    <?php
                    }
                    ?>
                              </div>
                         </li> 
          <?php
          }
          ?>
<?php

     if (isset($user_permssions) && strpos($user_permssions,'admin') !== false)
     {
?>
                         <li class="nav-item mr-2">
                              <a href="?page=admin&access_filter=all" title="Admin Page" style="padding-top:5px;padding-bottom:5px;">
                                   <img src="images/Admin.png" style="margin-left:5px;height:40px;width:40px;">
                              </a>
                         </li>
<?php
     }
?>                         
                         
                    </ul>
                    <li>
                         <a href="utils/logoff.php" title="Log Off" style="padding-top:5px;padding-bottom:5px;"><img src="images/log_out.png" style="margin-left:5px;height:45px;width:30px;">    
                              Logout
                         </a>
                    </li>
               </div>      
          </nav>                   

          <!-- Jquery Dialogs -->
          <!-- All the confirm windows -->
          <div id='ConfirmId' title='Update'></div>

          <div id='AlertId' title='Ooops!'></div>

          <form id="danger" class="form-horizontal" method="post">
               <?php
               // print_r($roleList);
                    if(isset($message)  && $message !== '')
                    {
               ?>
                         <div class="alert alert-danger" style="font-size:20px;font-weight: bold;">
                              <?= $message; ?>
                         </div>
               <?php
                    }
               ?>
          </form>


          
