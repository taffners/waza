<?php
	$today = date("Y-m-d");

	if (isset($_GET['look_up_date']))
	{
		$look_up_date = $_GET['look_up_date'];
		$page_title = '24 hour plan for '.$look_up_date;
	}
	else
	{
		$look_up_date = $today;
		$page_title = '24 hour plan for today';
	}

	
	// find if a twenty four hour plan is available.  If not make one.
	$search_arr = array(
		'user_id' 		=> USER_ID,
		'plan_date' 	=> 	$look_up_date
	);

	$twenty_four_hour_plan = $db->listAll('24-hour-plan-by-date-and-user', $search_arr);


	if (empty($twenty_four_hour_plan))
	{
		$db->addOrModifyRecord('twenty_four_hour_plan_table', $search_arr);
		$twenty_four_hour_plan = $db->listAll('24-hour-plan-by-date-and-user', $search_arr);
	}

	if (isset($twenty_four_hour_plan[0]['twenty_four_hour_plan_id']))
	{
		$search_arr['twenty_four_hour_plan_id'] = $twenty_four_hour_plan[0]['twenty_four_hour_plan_id'];

		// make the 4 meals if empty
		$meals = $db->listAll('meals-for-day', $search_arr);	

		if (empty($meals))
		{
			$meal_types = $db->listAll('meal-types');

			foreach($meal_types as $key => $t)
			{
				$to_add = array(
					'user_id'					=> 	USER_ID,
					'twenty_four_hour_plan_id' 	=> 	$twenty_four_hour_plan[0]['twenty_four_hour_plan_id'],
					'meal_type_id'				=>	$t['meal_type_id']
				);

				$db->addOrModifyRecord('meal_table', $to_add);
			}

			$meals = $db->listAll('meals-for-day', $search_arr);	
		}
	}
	else
	{
		$message = 'no twenty four hour plan';
	}

	


?>