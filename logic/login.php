<?php
	$page_title = 'Welcome to '.SITE_TITLE;
	
	md5('urmc_ngs_reporter');

	if (empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] === "off") 
	{
	    $location = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
	    header('HTTP/1.1 301 Moved Permanently');
	    header('Location: ' . $location);
	    exit;
	}

	// find number of failed login's in the last 30 mins
	$num_failed_last_30_mins = $db->listAll('failed-logins-last-30-mins');
	$brute_force_occuring = False;
	if 	(
			isset($num_failed_last_30_mins[0]['login_count']) && 
			is_numeric($num_failed_last_30_mins[0]['login_count']) &&
			intval($num_failed_last_30_mins[0]['login_count']) >= 10
		)
	{
		$brute_force_occuring = True;
		$email_utils->emailAdmins('Brute Force logins is occurring on '.SITE_TITLE.':'.ROOT_URL.'.  In the last 30 mins '.$num_failed_last_30_mins[0]['login_count'].' failed logins have occurred.', 'Brute Force Occurring.');
	}
	else
	{
		if (isset($_POST['login_submit']) && (empty($_POST['email_address']) || empty($_POST['password'])))
		{
			$message = 'Please enter email address and password';
		}

		// Make sure a valid email address
		else if (	
					isset($_POST['login_submit']) && 
					!filter_var($_POST['email_address'], FILTER_VALIDATE_EMAIL) 
				)
		{
			$message = 'Enter an email address';
		}

		// Make sure either my gmail account or the email account contains urmc.rochester.edu
		else if (	
					isset($_POST['login_submit']) &&  				
					$_POST['email_address']	!== 'taffners@gmail.com' &&
					strpos($_POST['email_address'], '@urmc.rochester.edu') === false
				)
		{
			$message = 'Enter an email address';
		}

		/////////////////////////////////////////////////////////////////
		// Make sure password is at least 7 chars long and no more than 60 chars
		////////////////////////////////////////////////////////////////
		else if (isset($_POST['login_submit']) && strlen($_POST['password']) < 7)
		{
			$message = 'Password too short';
		}

		else if (isset($_POST['login_submit']) && strlen($_POST['password']) > 60)
		{
			$message = 'Password too long';
		}

		else if(isset($_POST['login_submit']) && !empty($_POST['email_address']) && !empty($_POST['password']))
		{	

			$log_in_array = array();
			$log_in_array['email_address'] = strip_tags($_POST['email_address']);
			$log_in_array['password'] = strip_tags($_POST['password']);

			$logged_in = $db->login($log_in_array);

			if(!$logged_in)
			{
				$message = 'login failed';
			}

			else
			{		
				header('Location:'.REDIRECT_URL.'?page=home');
			}
		}
		else if(isset($_POST['login_submit']) && 
			( empty($_POST['email_address']) || !empty($_POST['password']) ) )
		{
			$message = 'enter email_address and password';
		}
	}

?>
