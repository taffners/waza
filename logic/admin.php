<?php
	$page_title = 'Admin';

	$user_list = $db->listAll('all-users');
	
	$captcha_file = SERVER_SAVE_LOC.'captcha/captcha'.USER_ID.'.jpeg';

	$reporter_permission_list = $db->listAll('reporter-permission-list');

	// if user does not have admin permissions log off and redirect to login page
	if (!isset($user_permssions) || strpos($user_permssions, 'admin') === false)
	{		
		$db->logoff();
		header('Location:'.REDIRECT_URL.'?page=login');
	}

	if (!isset($_POST['add_user_submit']))
	{

		// make captcha file outside of the web directory
		$utils->generateCaptcha($captcha_file);

		// to load file outside of the web directory use read contents into a
		// variable using file_get_contents	
		$src_captcha = $utils->getVirtualImageSRC($captcha_file);	
	}

	else if (isset($_POST['add_user_submit']) && $_POST['captcha'] !== $_SESSION['code'])
	{
		$message = 'captcha does not match';
	}
	else if(isset($_POST['add_user_submit']) && isset($_POST['email_address']))
	{
		$email = filter_var($_POST['email_address'], FILTER_SANITIZE_EMAIL);

		if (filter_var($email, FILTER_VALIDATE_EMAIL))
		{

			// randomly generate password
			$random_password = $utils->randomStr(10);
			
			$add_user_array = array(
				'first_name'			=> 	$_POST['first_name'],
				'last_name'			=>	$_POST['last_name'],
				'email_address'		=> 	$_POST['email_address'],
				'password'			=>	md5($random_password),
				'credentials'			=>	$_POST['credentials'],
				'title'				=>	$_POST['title']
			);

			// make sure user is not already added
			$search_for_user = $db->listAll('check-user-by-email', $_POST['email_address']);
			
			if (empty($search_for_user))
			{
				// add new user			
				$add_result = $db->addOrModifyRecord('user_table', $add_user_array);

				// get user info
				$user_info = $db->listAll('user', USER_ID);

				//////////////////////////////////////////////////////////////////////////
				// Email New User
				////////////////////////////////////////////////////////////////////////// 
				$to = $_POST['email_address'];
				$subject = 'New '.SITE_TITLE.' Account';
				$body = "A ".SITE_TITLE." account has been generated for you.  Use the login information below to login and reset your password.\r\n\r\nuser_name: ".$_POST['email_address']."\r\npassword: ".$random_password."\r\n\r\n".SITE_TITLE;
				
				$email_utils->sendEmail($to, $subject, $body, 'skip');

				// notify admins of new user
				$body = 'Information below was sent to the new user '.$_POST['first_name'].':

					"'.$body.'"';
				$email_utils->emailAdmins($body, $subject);

				//////////////////////////////////////////////////////////////////////////
				// Email in a separate email the user on how to access URMC reporter
				//////////////////////////////////////////////////////////////////////////
				$admin_names = $db->listAll('all-users-with-permission', 'admin');

				$subject = 'Instructions on '.SITE_TITLE.' access';
				$site_url = explode('?', 'http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'])[0];

				$body = 'Hello '.$user_info[0]['first_name'].',
					
					To access '.SITE_TITLE.' go to <br><br><a href="'.$site_url.'">'.$site_url.'</a> in Chrome.  Your login credentials were sent in a separate email.
					
					For assistance please contact '.$admin_names[0]['users'];

				$email_utils->sendEmail($to, $subject, $body, 'skip');

				// reset captcha 
				unset($_POST['add_user_submit']);
				session_start();
				unset($_SESSION['code']);
				session_write_close(); 
				unlink($captcha_file);

				header("Refresh:0");
			}
			else
			{
				$message = 'email address already used';
			}
		}
		else
		{
			$message = 'Format input for an email address was incorrect. '.$_POST['email_address'];
		}
	}	
?>