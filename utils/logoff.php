<?php
	
	// set up db connection
	if (explode('/', $_SERVER['REQUEST_URI'])[1] === 'devs')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/devs/micro_infotrack/config.php');
	}
	elseif (explode('/', $_SERVER['REQUEST_URI'])[1] === 'micro_infotrack')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/micro_infotrack/config.php');
	}

	$db->logoff();

	exit();
?>

