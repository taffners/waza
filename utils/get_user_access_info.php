<?php
	$_GET['page'] = 'get_user_access_info';
	// set up db connection
	if (explode('/', $_SERVER['REQUEST_URI'])[1] === 'devs')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/devs/micro_infotrack/config.php');
	}
	elseif (explode('/', $_SERVER['REQUEST_URI'])[1] === 'micro_infotrack')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/micro_infotrack/config.php');
	}





	if (isset($_POST['access_filter']) && $_POST['access_filter'] === 'all')
	{
		// Get all users in time period
		echo json_encode($db->listAll('user-login-summary', ''));
	}
	else if (isset($_POST['access_filter']) && $_POST['access_filter'] === 'seven_days')
	{
		echo json_encode($db->listAll('user-login-summary', 'WHERE time_stamp >= (CURDATE() - INTERVAL 1 WEEK)')); 
	}
	else if (isset($_POST['access_filter']) && $_POST['access_filter'] === 'month')
	{
		echo json_encode($db->listAll('user-login-summary', 'WHERE time_stamp >= (CURDATE() - INTERVAL 1 MONTH)')); 
	}
	else if (isset($_POST['access_filter']) && $_POST['access_filter'] === 'today')
	{
		echo json_encode($db->listAll('user-login-summary', 'WHERE DATE(time_stamp) = CURDATE()')); 
	}
	else if (isset($_POST['access_filter']) && $_POST['access_filter'] === 'half_hour')
	{
		echo json_encode($db->listAll('user-login-summary', 'WHERE time_stamp >= (now() - INTERVAL 30 MINUTE)')); 
	}
	else
	{
		return 'Post Does not contain access_filter';
	}

	exit();
?>