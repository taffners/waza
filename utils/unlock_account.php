<?php
	$_GET['page'] = 'unlock_account';
	// set up db connection
	if (explode('/', $_SERVER['REQUEST_URI'])[1] === 'devs')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/devs/micro_infotrack/config.php');
	}
	elseif (explode('/', $_SERVER['REQUEST_URI'])[1] === 'micro_infotrack')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/micro_infotrack/config.php');
	}

	$random_password = $utils->randomStr(10);
	$db->unlockAccount($_POST['email_address'], $random_password);

	echo $random_password;
	
	// email user of new password
	if ($_POST['email_address'] === 'taffners@gmail.com')
	{
		$_POST['email_address'] = 'samantha_taffner@urmc.rochester.edu';
	}

	$to = $_POST['email_address'].',samantha_taffner@urmc.rochester.edu';
	$subject = SITE_TITLE.' Account Unlocked';
	$body = "Your ".SITE_TITLE." account has been reset.  Use the login information below to login and reset your password.\r\n\r\nuser_name: ".$_POST['email_address']."\r\npassword: ".$random_password."\r\n\r\n".str_replace('&amp;', '&',SITE_TITLE);
	
	$email_utils->sendEmail($to, $subject, $body, 'skip');
	$email_utils->emailAdmins($body, $subject);

	exit();
?>