<?php
	
	// set up db connection
	if (explode('/', $_SERVER['REQUEST_URI'])[1] === 'devs')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/devs/micro_infotrack/config.php');
	}
	elseif (explode('/', $_SERVER['REQUEST_URI'])[1] === 'micro_infotrack')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/micro_infotrack/config.php');
	}

	// for permissions already set remove 
	$permission_id = $db->listAll('get-permission-id', $_POST['permission']);
	$permission_id = $permission_id[0]['permission_id'];

	$update_array = array(
		'permission_id' 	=> $permission_id,
		'user_id'			=> $_POST['user_id']
	);

	// find if the user already has the permission and it needs to be removed or 
	// permission needs to be added
	if (isset($_POST['curr_permissions']) && strpos($_POST['curr_permissions'], $_POST['permission']) !== false)
	{		
		// for permissions not set add 

		$db->deleteRecord('permission_user_xref', $update_array);
	}
	else if (isset($_POST['curr_permissions']) && strpos($_POST['curr_permissions'], $_POST['permission']) === false)
	{
		
		// for permissions not set add 

		$db->addOrModifyRecord('permission_user_xref', $update_array);
	}

	exit();
?>