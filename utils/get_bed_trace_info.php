<?php
	$_GET['page'] = 'get_bed_trace_info';
	// set up db connection

	if (explode('/', $_SERVER['REQUEST_URI'])[1] === 'devs')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/devs/micro_infotrack/config.php');
	}
	elseif (explode('/', $_SERVER['REQUEST_URI'])[1] === 'micro_infotrack')
	{
		require_once($_SERVER['DOCUMENT_ROOT'].'/micro_infotrack/config.php');
	}

	// #########################################################
	echo json_encode($db->listAll('bed-trace-info'));

	exit();
?>