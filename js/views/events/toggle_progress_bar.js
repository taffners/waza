define(function()
{
     function _StartEvents(php_vars)
     {
          // toggle progress report bar
          $('.toggle-report-progress-bar-btn').on('click', function()
          {
               var  _btn_id = $(this).attr('id').replace('toggle-report-progress-bar-btn-', '');
               
               switch(_btn_id)
               {
                    // if minus button was clicked
                    case 'minus':
                  
                         // hide progress bar
                         $('#report-nav').hide();

                         // expand contents of page
                         $('#toggle-report-progress-bar').removeClass('col-xs-10 col-sm-10 col-md-10 col-lg-10').addClass('col-xs-12 col-sm-12 col-md-12 col-lg-12');

                         // hide minus button
                         $('#toggle-report-progress-bar-btn-minus').hide();

                         // show plus button
                         $('#toggle-report-progress-bar-btn-plus').show();

                         break;

                    // if plus button was clicked
                    case 'plus':
                  
                         // show progress bar
                         $('#report-nav').show();

                         // shrink contents of page
                         $('#toggle-report-progress-bar').removeClass('col-xs-12 col-sm-12 col-md-12 col-lg-12').addClass('col-xs-10 col-sm-10 col-md-10 col-lg-10');

                         // hide plus button
                         $('#toggle-report-progress-bar-btn-plus').hide();

                         // show minus button
                         $('#toggle-report-progress-bar-btn-minus').show();

                         break;
               }
                    
          });
     }

     return {
          StartEvents:_StartEvents
     }
});
