define(function()
{
     function _StartEvents(php_vars)
     {
          // Add date picker to fields for a workaround for firefox not having type="date"
          $('.date-time-picker').each(function()
          {
               $(this).datetimepicker(
               {
                    sideBySide: true,
                    format: 'MM/DD/YYYY HH:mm:ss'
               });
          });

          // var current_year = (new Date()).getFullYear();
          // $('.date-picker').each(function()
          // {
          //      $(this).datepicker(
          //      {
          //           dateFormat: 'dd-mm-yyyy',
          //           changeMonth: true,
          //           changeYear: true,
          //           yearRange:  (current_year - 125) + ':' + current_year                    
          //      });
          // });
     }

     return {
          StartEvents:_StartEvents
     }
});
