define(['models/utils'],function(utils)
{
     // Use obj to activate all buttons to add and delete new fields with a set field name 
     
     // Requirements:
          // Place template to clone in HTML in a non visible div.  DO NOT PLACE IN A HTML5 TEMPLATE TAG.  These
          // tags are not visible to JQuery clone.

          // activate btn with 
               // class append-sample-input
               // data-clone_template => id to the data to be cloned
               // data-rm_clone_template_btn => id to the btn to rm newly appended field
               // Example:
                    // <button type="button" class="btn btn-success btn-success-hover append-sample-input" data-clone_template="div-to-clone-linked-sample-inputs" data-rm_clone_template_btn="clone-linked-sample-remove-btn" data-append_to="div-to-append-linked-sample-inputs" style="float: right;">
                    //    <span class="fas fa-plus"></span> Insert another linked sample
                    // </button>

          // rm_clone_tempalte_btn 
               // id the same as data-rm_clone_template
               // class => remove-cloned-input
               // Add a title to the delete button telling the user what it does.
               // Example:
                    // <button id="clone-linked-sample-remove-btn" type="button" class="btn btn-danger btn-danger-hover remove-cloned-input" title="delete this linked sample.">
                    //      <span class="fas fa-minus-circle"></span>
                    // </button>

          // clone template
               // id the same as data-clone_template
               // include rm_clone_template_btn
               // Add under name a list to append all inserted fields into.
               // Example:
                    // <div id="div-to-clone-linked-sample-inputs" class="form-group row d-none">
                    //      <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    //           <label for="linked_sample" class="form-control-label">
                    //                linked sample: 
                    //                <span class="required-field">*</span>
                    //           </label>
                    //      </div>
                    //      <div class="col-xs-11 col-sm-8 col-md-8 col-lg-8 mr-0 pr-0">
                    //           <input type="text" class="form-control no-spaces no-period" name="linked_sample[]" maxlength="40"/>
                    //      </div>                             
                    //      <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 ml-0 pl-0">
                    //           <button id="clone-linked-sample-remove-btn" type="button" class="btn btn-danger btn-danger-hover remove-cloned-input" title="delete this linked sample.">
                    //           <span class="fas fa-minus-circle"></span>
                    //      </button>
                    //      </div>
                    // </div>   

          // a type restriction field and field to restrict it to.
               // type restriction drop down
                    // Add a class to the type restriction select menu of 
                         // field-type-restriction-clone 
                    // Add a data field that links to the field to restrict
                         // data-field_apply_restriction_to_clone="extra_field_val_clone"
               // field to restrict
                    // add id the same as under data-field_apply_restriction_to_clone in the type restriction drop down
                         // <div class="col-xs-12 col-sm-5 col-md-5 mr-0 pr-0">
                         //      <input id="extra_field_val_clone" type="text" class="form-control div-to-append-previous-sample-names-required-field" name="text_value[]" maxlength="40"/>
                         // </div>

          // Make the First field provided automatically required if a clone is present
               // Add to all inputs the class the id of the class append-sample-input followed by -required-field
                    // Example: 
                         // class append-sample-input => div-to-append-linked-sample-inputs
                         // input class => div-to-append-linked-sample-inputs-required-field

          // show the require asterisk in span
               // Add to all inputs the class the id of the class append-sample-input followed by -required-span.  Also hide span.
                    // Example: 
                         // class append-sample-input => div-to-append-linked-sample-inputs
                         // input class => div-to-append-linked-sample-inputs-required-span d-none

     var  clone_num = 0,
          required_fields = new Array();

     function _StartEvents(field_lens)
     {
          _bindAddNewFieldEvents();

          // When the form is in update mode at that point the user will have the ability to delete previously 
          // added extra fields
          _bindUpdateEvents();
     }

     function _bindAddNewFieldEvents()
     {
          $('.remove-cloned-input').on('click', function()
          {
               let  rm_id = $(this).data('rm_temp_id'),
                    rm_class = $(this).attr('class').split(' ');

               $('#' + rm_id).remove();

               _hideRequirementField(rm_class);
          })

          $('.append-sample-input').on('click', function()
          {
               let  clone_template = $(this).data('clone_template'),
                    rm_clone_template_btn = $(this).data('rm_clone_template_btn'),
                    append_to_div = $(this).data('append_to'),
                    $curr_clone = $('#' + clone_template).clone(true, true),
                    new_temp_id = clone_template + '_' + clone_num;

               //  change id
               $curr_clone.attr('id', new_temp_id);

               // Make new template visible.  To make backwards compatible with bootstrap remove both classes
               $curr_clone.removeClass('d-none').removeClass('hide').show();

               // change id of button
               $curr_clone.find('#' + rm_clone_template_btn)
                    .attr('id', rm_clone_template_btn + '_' + clone_num)
                    .attr('data-rm_temp_id', new_temp_id)

                    // Add the clone added-clone to all new clones.  This will be used to see if a clone is present.  If not the remove function can remove the required restrictions on the initially added field.
                    .addClass(append_to_div + '-added-clone');

               $curr_clone.find('.field-type-restriction')
                    .attr('data-field_apply_restriction_to', 'extra_field_val_clone_' + clone_num);
               

               $curr_clone.find('#extra_field_val_clone')
                    .attr('id', 'extra_field_val_clone_'  + clone_num);


               $curr_clone.find('.selectpicker-clone').selectpicker();
               $curr_clone.find('.editable-select-clone').editableSelect();

               $curr_clone.appendTo('#' + append_to_div);
              
               // Make new template visible.  Do this afterward appending div just encase div is just set to display:none and does not have the class d-none.  This makes it bootstrap backwards compatible better. 
               $('#' + append_to_div).show();

               _fieldTyperestrictionBindEvent();

               clone_num++;

               _requireField(append_to_div);

               // for each editable-select add restrictions on data length          
               utils.typeRestrictionField(field_lens);
          });
     }

     function _bindUpdateEvents()
     {
          $('.extra-field-toggle').on('click', function()
          {
               let  _toggle_type = $(this).data('toggle_type'),
                    _shared_name = $(this).data('shared_name');

               if (_toggle_type === 'minus')
               {
                    $('#'+_shared_name+'_keep_status').val('delete');
                    $('#'+_shared_name+'_shown_to_user_data').addClass('strike-through');
               }
               else if (_toggle_type === 'plus')
               {
                    $('#'+_shared_name+'_keep_status').val('keep');
                    $('#'+_shared_name+'_shown_to_user_data').removeClass('strike-through');
               }
          });

     }

     function _fieldTyperestrictionBindEvent()
     {
          $('.field-type-restriction').on('change', function()
          {
               let  field_to_restrict = $(this).data('field_apply_restriction_to'),
                    field_restriction_type = $(this).val();

               // since selectpicker makes two changes ignore the non useful change
               if (field_to_restrict != undefined && field_to_restrict.length != 0)
               {
                    $('#'+field_to_restrict).prop('type', field_restriction_type);
               }
          });
     }
          
     function _requireField(append_to_div)
     {
          if (!required_fields.includes(append_to_div))
          {
               required_fields.push(append_to_div);

               $('.' + append_to_div + '-required-span').show().removeClass('d-none').removeClass('hide');

               $('.' + append_to_div + '-required-field').attr('required', 'required');
          }
     }

     function _hideRequirementField(rm_classes_arr)
     {
          // (array)
          // find the class in the array that ends with -added-clone.  Find if there are any more fields with this class.  If not remove required elements on initially added field

          let rm_class_arr = rm_classes_arr.filter(c => c.includes('-added-clone'));        

          // Only remove required fields if this was the last clone to remove
          if (rm_class_arr.length == 1 && !$('.' + rm_class_arr[0])[0])
          {
               let  rm_class = rm_class_arr[0].replace('-added-clone', '');

               $('.' + rm_class + '-required-span').hide();

               $('.' + rm_class + '-required-field').removeAttr('required');    
          }
     }

     return {
          StartEvents:_StartEvents
     }
});