define(['models/utils'], function(utils)
{
     function _BindEvents(php_vars)
     {

          // activate clicking on a variant row and scrolling to knowledge base
          $('.scroll_to_variant').on('click', function(event)
          {
               // get the data-scroll-to-raised-card and scroll to this div in the raised cards
               var  _scroll_id = $(this).data('scroll-knowledge-id'); 

               // do not scroll if first cell is clicked
               if ($(event.target).attr('type') !== 'checkbox')
               {
                   
                    var  _id_exists = utils.scroll('#' + _scroll_id) // if true div exists otherwise search name checker
                    // if id does not exists search name checker
                    if (_id_exists === 'nope' )
                    {
                         utils.dialog_window('This variant is missing from the knowledge base.  Please notify Samantha about the missing data in the knowledge base.');
                    }
                    // if there are more than one of this variant alert user
                    else if (_id_exists === 'not unique')
                    {
                         utils.dialog_window('There are multiple entries for this variant in the knowledge base.  Please notify Samantha about the duplication in the knowledge base.');
                    }                      
               }
                            
               
          });

          // activate scrolling to top of page
          $('.scroll_to_top_page').on('click', function()
          {             
               window.scrollTo(0,0);
          });



     }

  

     function _StartEvents()
     {

          _BindEvents();
     }

     return {
          StartEvents:_StartEvents
     }
});