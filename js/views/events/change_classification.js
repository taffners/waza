define(function()
{
     function _StartEvents(php_vars)
     {
          $('select.change-classification').on('change', function()
          {
               var  _classify = {
                         'classification'    :    $(this).val(),
                         'genes'             :    $(this).data('genes'),
                         'coding'            :    $(this).data('coding'),
                         'amino_acid_change' :    $(this).data('amino_acid_change'),
                         'panel'             :    $(this).data('panel')
                    }; 
                    
               //change value in database
               $.ajax(
               {
                    type:'POST',
                    url: 'utils/change_classification.php?',

                    dataType: 'text',
                    data: _classify,
                    success: function(response)
                    {
                         window.location.reload(true)
                    },
                    error: function(textStatus, errorThrown)
                    {
                         dialog_window('problem with ajax call ;( ' + errorThrown + ' ' + textStatus);
                    }
               });              
          }); 

     }

     return {
          StartEvents:_StartEvents
     }
});
