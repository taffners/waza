define(function()
{
     function _StartEvents(paging_status)
     {
          // Purpose:  Add to tables with a class of dataTable the sorting ability from bootstraps datatables https://datatables.net/manual/styling/bootstrap

          var  paging_status = paging_status || false;

          // make all tables sortable
          $('.sort_table').dataTable(
          {
               // "aaSorting": [],
               // turns off keeping a limited number of records on page.
               paging:paging_status
          });


          // make all tables sortable
          $('.reverse_sort_table').dataTable(
          {
               // "aaSorting": [],
               // turns off keeping a limited number of records on page.
               paging:paging_status,
               order:[[0,'desc']]
          });

          $('.sort_table_no_inital_sort').dataTable(
          {
               "aaSorting": [],
               // turns off keeping a limited number of records on page.
               paging:paging_status
          });


          $('.loader').fadeOut(500);
     }

     return {
          StartEvents:_StartEvents
     }
});
