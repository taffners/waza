define(['models/utils'], function(utils)
{
     function _StartEvents(row_type)
     {
         
          //////////////////////////////////////////////////////////////////
          // Include sample toggle
          //////////////////////////////////////////////////////////////////
          $('.'+row_type).on('click', function()
          {
              
               // check value of check box
               var  current_lib_tube_num,
                    increase_val = $(this).is(':checked'),
                    visit_id = $(this).attr('id').split('_')[1],
                    current_lib_tube_num = utils.update_increament_local_storage_var('current_lib_tube_num', increase_val);
            
               // set value of library tube cell to current_lib_tube_num
               if (increase_val == false)
               {
                    current_lib_tube_num = '';
               }

               $('#lib_tube_'+visit_id).html(current_lib_tube_num);
          });

          //////////////////////////////////////////////////////////////////
          // Edit Cell events
          //////////////////////////////////////////////////////////////////
          // Start editing cell
          // reference https://codewithmark.com/easily-edit-html-table-rows-or-cells-with-jquery
          $('.row_data').on('click', function(event)
          {
               event.preventDefault(); 

               if($(this).attr('edit_type') == 'button')
               {
                    return false; 
               }

               //make div editable
               $(this).closest('div').attr('contenteditable', 'true');

               //add bg css
               $(this).addClass('bg-warning').css('padding','5px');

               $(this).focus();             
          });

          //--->save single field data > start
          $('.row_data').on('focusout', function(event) 
          {
               event.preventDefault();

               if($(this).attr('edit_type') == 'button')
               {
                    return false; 
               }

               var  row_id = $(this).closest('tr').attr('row_id'), 
                    row_div = $(this);              
                    col_name = row_div.attr('col_name');
             
               // if col_name = total_vol calculate DNA vol and AE vol
               if ((col_name == 'total_vol' || col_name == 'qubit') && ($('#total_vol_'+row_id).html() !== undefined && $('#qubit_'+row_id).html() !== undefined))
               {                 
                    var  total_vol_val = $('#total_vol_'+row_id).html().trim(),
                         qubit_val = $('#qubit_'+row_id).html().trim();

                    // make sure col_val is an integer and qubit is an integer.  If not notify user
                    
                    if (utils.isPositveNumber(total_vol_val) && utils.isPositveNumber(qubit_val))
                    {
                         total_vol_val = parseInt(total_vol_val);
                         qubit_val = parseInt(qubit_val);
                                                 
                         // calculate DNA vol and AE vol
                         dna_vol = Math.round(((5 * total_vol_val) / qubit_val) * 10) / 10;                         
                         ae_vol = Math.round((total_vol_val - dna_vol) * 10) / 10;
                         
                         // empty out cell 
                         $('#DNA_vol_'+row_id).empty();
                         $('#AE_vol_'+row_id).empty();

                         // add new calculation
                         $('#DNA_vol_'+row_id).html(dna_vol);
                         $('#AE_vol_'+row_id).html(ae_vol);

                         // remove edit feature
                         row_div.removeClass('bg-warning').css('padding','');
                    }

                    // Make sure value just edited is an integer.  Otherwise 
                    // notify user.
                    else if (utils.isPositveNumber($('#'+col_name+'_'+row_id).html().trim()))
                    {
                         error_message = 'Enter an integer';
                         utils.dialog_window(error_message, 'AlertId');
                    }

                    // This is for rows added by user.  The since both qubit and dil total vol needs to be added
                    else
                    {
                         // remove edit feature
                         row_div.removeClass('bg-warning').css('padding','');
                    }
               }
               else
               {
                    row_div.removeClass('bg-warning').css('padding','');
               }
          });   
     }

     return {
          StartEvents:_StartEvents
     }
});
