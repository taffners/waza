define(function()
{
     function _StartEvents(php_vars)
     {
         
          // once a pending run is clicked show edit report
          $('.open_report').on('click',function()
          {
               var  ids = $(this).attr('id').replace('run_id_', '&run_id=').replace('_visit_id_', '&visit_id=').replace('_patient_id_', '&patient_id=');
            
               // go to a showEditReport with id of run
               window.location.href = '?page=show_edit_report' + ids;
          });
     }

     return {
          StartEvents:_StartEvents
     }
});
