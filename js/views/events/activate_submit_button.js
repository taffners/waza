define(function()
{
     function _StartEvents()
     {
          // enable submit button once a change has occurred on form
          $('input[type="text"], input[type="number"], select, textarea').on('input',function()
          {
               $('button[type="submit"]').removeClass('disabled-link'); 
          });

          // need to use dp. for any events using bootstrap-datetimepicker
          // https://eonasdan.github.io/bootstrap-datetimepicker/Events/
          $('.date-time-picker').on('dp.change',function()
          {
               $('button[type="submit"]').removeClass('disabled-link'); 
          });


          $('input[type="radio"]').on('change', function()
          {
               $('button[type="submit"]').removeClass('disabled-link'); 
          });
     }

     return {
          StartEvents:_StartEvents
     }
});