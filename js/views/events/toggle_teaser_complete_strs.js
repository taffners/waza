define(function()
{
     function _StartEvents(php_vars)
     {

          // In the variant table cells larger than 20 characters are cut.  This will enable the more button and less button
          // enable the show_complete button
          $('span.show-complete').on('click', function()
          {
               // get the id of the clicked span
               var  span_id = $(this)
                         .attr('id')
                         .replace('show_complete_', '');  // other teaser complete toggle end with the same id 

               // hide teaser
               $('#teaser_' + span_id).hide();

               // hide more span
               $(this).hide();

               // show complete
               $('#complete_' + span_id).show().removeClass('d-none');

               // show less span
               $('#show_teaser_' + span_id).show().removeClass('d-none');
          });

          // enable the show-teaser button
          $('span.show-teaser').on('click', function()
          {
               // get the id of the clicked span
               var  span_id = $(this)
                         .attr('id')
                         .replace('show_teaser_', '');  // other teaser complete toggle end with the same id 

               // hide less span
               $('#show_teaser_' + span_id).hide();

               // hide complete span
               $('#complete_' + span_id).hide();

               // show more span
               $('#show_complete_' + span_id).show().removeClass('d-none');

               // show teaser
               $('#teaser_' + span_id).show().removeClass('d-none');
          });  
     }

     return {
          StartEvents:_StartEvents
     }
});
