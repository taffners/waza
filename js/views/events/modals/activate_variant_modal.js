define(function()
{
     function _StartEvents(php_vars)
     {
          // toggle variant_comment_modal
          $('.variant_comment_modal_btn').on('click', function()
          {

               var  _tr, _i,
                    _modal_title = $(this).data('variant-name').replace('_', ' '),
                    _comment_ids = $(this).data('comment-id'),
                    _observed_variant_id  = $(this).data('observed-variant-id'),
                    _multiple_ids = true;             

               // if only one id is present the type will be a number.  Since it isn't possible to take an indexOf of number to find if '-' is in string i'm using _multiple_ids to see if I can split on '-'  it is a round about way.  Maybe a better way can be found.
               if (typeof(_comment_ids) === 'number')
               {
                    _multiple_ids = false;
               }

               // if more than one comment id is present it will be seperated by -.  first see if - is present in comment_ids then split
               // otherwise convert to an array
               if (_multiple_ids)
               {
                   
                    _comment_ids = _comment_ids.split('-');
               }
               else
               {
                    _comment_ids = [_comment_ids];
                   
               }

               // add observed variant id to local storage             
               localStorage.setItem('observed_variant_id', _observed_variant_id);

               // change modal title
               $('#variant-comment-modal-title').html('<b>Comments</b> <br>' +_modal_title);

               // if the data-comment-id is not equal to none there are comments to add to the modal. 
               // Perform ajax call to get all comments
               if (_comment_ids[0] !== 'none' && _comment_ids.length >= 1)
               {
                    // clear out table body variant-comment-table-body
                    $('#variant-comment-table-body').empty();
                    
                    for (_i in _comment_ids)
                    {
                         
                         // for each id in list make an ajax call and append to variant-comment-table-body
                         $.ajax(
                         {
                              type:'GET',
                              url:'utils/get_comments.php',
                              dataType: 'json',
                              data:{ 'comment_id': _comment_ids[_i], 'comment_type': 'observed_variant'}
                         }).done(function(response)
                         {              
                              // Build new row for table of returned comment
                              _tr = '<tr><td>' + response['comment'] + '</td>';
                              _tr += '<td>' + response['user_name'] + '</td>';   
                              _tr += '<td>' + response['comment_time_stamp'] + '</td></tr>';

                              $('#variant-comment-table-body').append(_tr);                        
                         });   
                    }                    
               }

               // activate the submit_variant_comment btn to add a new comment
               $('#submit_variant_comment').on('click', function()
               {
                    var  _comment = $('#variant-comment-modal-textarea').val();

                    if (_comment !== '')
                    {
                         $.ajax(
                         {
                              type: 'POST',
                              url: 'utils/add_comment.php',
                              
                              data: {
                                        'comment_ref': localStorage.getItem('observed_variant_id'),
                                        'comment_type': 'observed_variant',
                                        'comment': _comment
                                   },
                              success: function(response)
                                   {
                                        // reload page
                                        window.location.reload();
                                   }  
                         });
                         
                    }
                    else
                    {
                         $('#add-variant-msg-center').show();
                    }
               });                 

               $('#variant_comment_modal').modal('toggle');
          });
     }

     return {
          StartEvents:_StartEvents
     }
});
