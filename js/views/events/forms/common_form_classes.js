define(['models/utils'], function(utils)
{
     function _StartEvents()
     {
          $('input.no-spaces').bind(
          {
               keydown: function(keyPress)
               {
                    if (keyPress.keyCode == 32)
                    {
                         utils.dialog_window('Spaces are not allowed for this field');
                         
                         return false;
                    }
               }
          });

          $('input.no-period').bind(
          {
               keydown: function(keyPress)
               {
                    if (keyPress.keyCode == 190)
                    {
                         utils.dialog_window('Periods are not allowed for this field');
                         return false;
                    }
               }
          });

          // capitalize data entry.  Example aDAM -> Adam
          $('.capitalize').on('keyup paste', function(e)
          {
               var  textBox = e.target,
                    start = textBox.selectionStart,
                    end = textBox.selectionStart;
                    
                    textBox.value = textBox.value.charAt(0).toUpperCase() + textBox.value.slice(1).toLowerCase();
                    textBox.setSelectionRange(start, end);
          });

          
          // prevent enter from submitting form
          // $('form').on('keyup keypress', function(e) 
          // {
          //      var keyCode = e.keyCode || e.which;
               
          //      if (keyCode === 13) 
          //      { 
          //           e.preventDefault();
          //           return false;
          //      }
          // });
     }

     return {
          StartEvents:_StartEvents
     }
});
