define(['models/utils'], function(utils)
{
     var  hex_color_list = {
            
               // 'Cyan':'#42d4f4',
               
               // 'Green':'#3cb44b',
                              
               'Navy':'#000075',        
               'Fluorescent_pink':'#e6194B',
               'Orange':'#f58231',
               'Yellow':'#ffe119',
               'Lime':'#bfef45',             
               'Independence':'#4D516D',
               // 'Azure':'#4363d8',
               'Purple':'#911eb4',
               // 'Magenta':'#f032e6',
               'Grey':'#a9a9a9',
               'Apricot':'#ffd8b1',
               'Mint':'#aaffc3',
               'Lavender':'#e6beff',
               'Pink':'#f79ebd',
               // 'Maya':'#73C2FB',
               'Scarlet':'#FF2400',
               'Blue':'#0018F9',
               'Electric':'#8F00FF',         
               'Forest':'#0B6623',
               'Yellow_too':'#f2ff40',
               'bright_purple':'#ee00ff',
               'bright_green':'#44ff00',               
               'dark_purlple':'#4d0073',
               'Olive': '#808000',
               'Maroon':'#800000',
               'Brown':'#9A6324',
               'pink':'#cc2586', 
               'blue_grey':'#004d73',
               'blue':'#4287f5',               
               'dark_grey':'#827f75',
               'green_grey':'#728f83',
               'Teal':'#469990',
          };     

     // https://www.december.com/html/spec/color0.html
     var  grey_scale_hex = {

          'black'  :'#000000',
          'gray39' :'#636363',
          'gray51' :'#828282',
          'darkgrey':'#A9A9A9',
          'silver' :'#C0C0C0',
          'verylight':'#CDCDCD',
          'gray85' :'#D9D9D9',
          'gray92' : '#EBEBEB',
          'whitesmoke':'#F5F5F5',
          'white':'#FFFFFF'
     }     

     function dragstarted(d) 
     {
          d3.select(this).raise().attr("stroke", "black");
     }

     function dragged(d) 
     {
          d3.select(this)
               .attr("x", x = d3.event.x)
               .attr("y", y = d3.event.y);
     }

     function dragended(d) 
     {
          d3.select(this).attr("stroke", null);
     }

     function _changeSVGFontSize(fontSize)
     {         
          d3.select('svg')
               .style('font-size', fontSize+'px');
     }

     function _drawTimeline(row_data, options)
     {            
          var  _options   = options;     
          // (JSON, JSON) -> draw a timeline in options(insert_id)
          google.charts.load('current', {packages:['timeline']});
          google.charts.setOnLoadCallback(drawChart);

          function drawChart() 
          {            
               var  container = document.getElementById(_options['insert_id']),
                    chart = new google.visualization.Timeline(container),
                    dataTable = new google.visualization.DataTable(),
                    i, 
                    rows=[],
                    curr_row, 
                    start_utcDate, 
                    start_local_date,
                    end_utcDate, 
                    end_local_date,
                    map_catergory,
                    bar_color,
                    firstKey,
                    _hex_color_list = utils.clone(hex_color_list),
                    extra_no_color_samples = '<b>The following categories are set to black because I ran out of colors &#128546</b>:<br>',
                    extra_samples = false, 
                    row_name,
                    on_types = _options['types'],
                    on_sts = _options['st_types_on'],
                    hidden_samps = _options['hidden_samples'],              
                    _order = JSON.parse(JSON.stringify(_options['order'])),
                    colorMap = {},
                    bar_name,
                    init_bar_name;

               if (_options['paperScaleBar'])
               {
                    colorMap = {

                         // blues
                         // 'NYH01':'#0D47A1',
                         'NYH01':'blue',
                         // 'NYH02':'#1E88E5',
                         'NYH02':'#0D47A1',
                         'NYH03':'#42A5F5',
                         'NYH04':'#badbf5',

                         // eds reds
                         // 'NYH01 ED':'#B71C1C',
                         'NYH01 ED':'red',
                         // 'NYH02 ED':'#E53935',
                         // 'NYH03 ED':'#EF5350',
                         'NYH03 ED':'#B71C1C',
                         'NYH04 ED':'#EF9A9A',

                         // greys
                         'NYLTCF003':'#D9D9D9',
                         'NYLTCF004':'#828282',
                         'NYLTCF005':'#C0C0C0',
                         'NYLTCF011':'#000000',
                         'NYLTCF18':'#EBEBEB',
                         'NYLTCF029':'#FFFFFF',

                         'WGS_test':'#33FF00',
                         'test':'#339900 '
                    };
               }

               // if greyScaleBar is true switch color list to grey scale
               if (_options['greyScaleBar'])
               {
                    _hex_color_list = utils.clone(grey_scale_hex);
               }

               $('#message').empty();

               dataTable.addColumn({ type: 'string', id: 'Group' });
               dataTable.addColumn({ type: 'string', id: 'Category' });
               dataTable.addColumn({ type: 'string', role: 'style' });
               dataTable.addColumn({ type: 'date', id: 'Start' });
               dataTable.addColumn({ type: 'date', id: 'End' });
                      
               for (i = 0; i < row_data.length; i++)
               {                    
                    curr_row = row_data[i];

                    // make sure the sample is not hidden before adding
                    if   (
                              on_types.includes(curr_row['type']) && 
                              on_sts.includes(curr_row['st_type']) &&
                              !hidden_samps.includes(curr_row['specimen_id']) &&
                              curr_row !== undefined
                         )
                    {
                         start_utcDate = new Date(curr_row['start_date']); //Date object a day behind
                         start_local_date = new Date(start_utcDate.getTime() + start_utcDate.getTimezoneOffset() * 60000);

                         end_utcDate = new Date(curr_row['end_date']); //Date object a day behind
                         end_local_date = new Date(end_utcDate.getTime() + end_utcDate.getTimezoneOffset() * 60000);

                         // make sure start date is before end date
                         if (start_utcDate > end_utcDate)
                         {
                              console.log('end date before start date ');
                              console.log(curr_row);
                         }

                         if (curr_row['start_date'] == curr_row['end_date'])
                         {
                              end_local_date.setDate(end_local_date.getDate() + 1);
                         }                  
                    
                         //////////////////////////////////////////////////////
                         // set color of bar depending column indicated in rect_color_col
                         ///////////////////////////////////////////////////////
                         map_catergory = curr_row[curr_row['rect_color_col']];

                         // Do not split ED off map_category for paper
                         if (_options['paperScaleBar'] && map_catergory.includes('ED'))
                         {
                               map_catergory = map_catergory;
                         }


                         // Split at space in map_catergory if true.
                         else if   (_options['id_split_space'])
                         {
                       
                              map_catergory = map_catergory.split(' ')[0];
                         }

                         // find if color is already assigned and if not pick a select color hex_color_list 
                         if (map_catergory in colorMap)
                         {
                              bar_color = colorMap[map_catergory];
                         }
                         else
                         {
                              if (Object.keys(_hex_color_list).length === 0)
                              {
                                   if (extra_samples)
                                   {
                                        extra_no_color_samples+= ', ';
                                   }

                                   extra_samples = true;

                                   extra_no_color_samples+= map_catergory;

                                   // set color to black
                                   colorMap[map_catergory] = '#000000';

                                   // assign color to current bar
                                   bar_color = colorMap[map_catergory];
                              }
                              else
                              {
                                   // get the first object in the hex_color_list
                                   firstKey = Object.keys(_hex_color_list)[0];

                                   // add color for this map_catergory to colorMap
                                   colorMap[map_catergory] = _hex_color_list[firstKey];

                                   // assign color to current bar
                                   bar_color = colorMap[map_catergory];

                                   // remove color from hex_color_list so it isn't used again
                                   delete _hex_color_list[firstKey];
                              }                         
                         }

                         // sometimes names get very long and this is a way to only show 
                         // name until the first _  For instance M2016019446_S3_L001 
                         // becomes M2016019446
                         if (_options['split_name_at_'])
                         {
                              row_name = curr_row['specimen_id'].split('_')[0];
                         }
                         else
                         {
                              row_name = curr_row['specimen_id'];
                         }

                         // Add ST type to row name
                         if (_options['add_st_type'] && curr_row['st_type'])
                         {
                              row_name = row_name + ' ST' + curr_row['st_type'];
                         }
                         
                         if (_options['paperScaleBar'])
                         {
                              init_bar_name = curr_row[curr_row['rect_color_col']];
                              bar_name = init_bar_name.replace('NYLTCF', 'L').replace('NYH','H').replace('0','');
                              bar_name = bar_name.split(' ')[0];
                              
                         }
                         else
                         {
                              bar_name = curr_row[curr_row['rect_color_col']];
                         }


                         if (_options['paperScaleBar'] && bar_name !== 'HO')
                         {
                              _order[curr_row['specimen_id']].push([row_name, bar_name, bar_color, start_local_date, end_local_date]);
                         }
                         else if (!_options['paperScaleBar'])
                         {
                              _order[curr_row['specimen_id']].push([row_name, bar_name, bar_color, start_local_date, end_local_date]);
                         }
                         
                    }                               
               }

               // add data int he correct order to the dataTable
               var  i, k, samp_data,
                    samps = Object.keys(_order),
                    samps_drawn = [];

               for (i = 0; i < samps.length; i++)
               {        
                    samp_data = _order[samps[i]];
                    if (samp_data != [])
                    {
                         for (k = 0; k < samp_data.length; k++)
                         {
                              rows.push(samp_data[k]);

                              // keep track how many samples are being drawn 
                              // to get accurate svg height
                              if (!samps_drawn.includes(samp_data[k][0]))
                              {
                                   samps_drawn.push(samp_data[k][0]);
                              }                          
                         }
                    }                    
               }

               dataTable.addRows(rows);
           
               var  rowHeight = 80,
                    keyRowHeight = 24,
                    keyHeight = Object.keys(colorMap).length * keyRowHeight,
                    svgHeight = (samps_drawn.length + 1) * rowHeight,
                    chartHeight = svgHeight + keyHeight;

               var   options = 
                    {
                         timeline: 
                         { 
                              groupByRowLabel: true,
                              rowLabelStyle: 
                              {
                                   fontName: 'Roboto Condensed',
                                   fontSize: 14,
                                   color: '#333333'
                              },
                              barLabelStyle: 
                              {
                              fontName: 'Roboto Condensed',
                              fontSize: 14
                              },
                              showBarLabels:_options['showBarLabels']
                         },                          
                         avoidOverlappingGridLines: true,
                         height: chartHeight,
                         width: '90%'
                    };

               if (extra_samples)
               {

                    $('#message').html(extra_no_color_samples).show();
               }
               else
               {
                    $('#message').hide();
               }

               chart.draw(dataTable, options);
               _changeSVGFontSize(10);
               _legend(colorMap, keyRowHeight);              
          }
     }  

     function _legend(colorMap, keyRowHeight)
     {
          $('#legend').remove();

          // Since the chart is made separately with google charts and the legend is made by d3 I need to find where to insert the legend.  Therefore I need to find the Y location of the last text entered in the x axis.  d3 is modified to include last in main.js 
          var  i, curr_y,
               svgHeight = parseInt(d3.selectAll('text').last().attr('y')) + keyRowHeight, 
               bar_names = Object.keys(colorMap),
               square_size = 20,
               spacing = keyRowHeight - square_size,
               x_legend_start = 30,
               x_legend_text = x_legend_start + square_size + spacing,
               legend_g = d3.select('svg')
                              .append('g')
                              .attr('x', x_legend_start)
                              .attr('y', svgHeight + keyRowHeight)
                              .attr('id', 'legend')
                              .style('cursor', 'pointer'); 

          for (i=0; i < bar_names.length; i++)
          {
               curr_y = svgHeight + keyRowHeight * (i+1);

               legend_g.append('rect')
                    .attr('x', x_legend_start)
                    .attr('y', curr_y)
                    .attr('width', square_size)
                    .attr('height', square_size)
                    .style('fill', colorMap[bar_names[i]])   

               legend_g.append('text')
                    .attr('x', x_legend_text)
                    .attr('y', curr_y + (keyRowHeight/2))
                    .text(bar_names[i])           
          }
     }

     function _drawMaterialBarChart(insert_id, chart_title, data_array, sub_title, bar_type, x_axis_label)
     {
          // NOT USING THIS.  I CAN NOT FIGURE HOW TO CHANGE TICKS. MATERIAL IS STILL BETA
          google.charts.load('current', {'packages':['bar']});
          
          google.charts.setOnLoadCallback(drawChart);

          function drawChart() 
          {
               var data = new google.visualization.arrayToDataTable(data_array);

               var options = 
               {
                    chart: 
                    {
                         title: chart_title,
                         subtitle: sub_title
                    },
                    bars: bar_type, // Required for Material Bar Charts.   
                    axes: 
                    {
                         x: 
                         {
                              0: 
                              { 
                                   label: x_axis_label,
                              } 
                         }
                    },                    
                    hAxis: 
                    {                         
                         ticks: [1,3]
                    },
                    bar: { groupWidth: "90%" }           
               }
                            
               var chart = new google.charts.Bar(document.getElementById(insert_id));
               
               chart.draw(data, google.charts.Bar.convertOptions(options));
          }
     }

     function _drawBarChart(insert_id, chart_title, data_array, sub_title, bar_type, x_axis_label, isStackedVar=false)
     {
          if (data_array.length > 1)
          {
               google.charts.load('current', {packages: ['corechart', 'bar']});
               google.charts.setOnLoadCallback(drawAxisTickColors);

               function drawAxisTickColors() 
               {
                    var data = google.visualization.arrayToDataTable(data_array);

                    var options = 
                    {
                         title: chart_title,
                         titleTextStyle: 
                         {
                              bold: true,
                              fontSize: 14,
                              color: '#4d4d4d'
                         },
                         chartArea: 
                         {
                              width: '40%',
                              height: '50%'
                         },
                         hAxis: 
                         {
                              title: x_axis_label,
                              minValue: 0,
                              textStyle: 
                              {
                                   bold: true,
                                   fontSize: 12,
                                   color: '#4d4d4d'
                              },
                              titleTextStyle: 
                              {
                                   bold: true,
                                   fontSize: 12,
                                   color: '#4d4d4d'
                              },
                              gridlines:{count:4}
                         },
                         vAxis: 
                         {
                              // title: 'City',
                              textStyle: 
                              {
                                   fontSize: 12,
                                   bold: false,
                                   color: '#4d4d4d'
                              } //,
                              // titleTextStyle: 
                              // {
                              //      fontSize: 12,
                              //      bold: true,
                              //      color: '#4d4d4d'
                              // }
                         },
                         isStacked: isStackedVar
                    };
                    var chart = new google.visualization.BarChart(document.getElementById(insert_id));
                    chart.draw(data, options);
               }
          }
          else
          {
               $('#'+insert_id).append(chart_title + ' is empty');
          }
     }

     function _drawQuantiyTimeBarChart()
     {
          google.charts.load('current', {'packages':['bar']});
          
          google.charts.setOnLoadCallback(drawChart);

          function drawChart() 
          {
             var data = new google.visualization.DataTable();
      data.addColumn('timeofday', 'Time of Day');
      data.addColumn('number', 'Emails Received');

      data.addRows([
        [[8, 30, 45], 5],
        [[9, 0, 0], 10],
        [[10, 0, 0, 0], 12],
        [[10, 45, 0, 0], 13],
        [[11, 0, 0, 0], 15],
        [[12, 15, 45, 0], 20],
        [[13, 0, 0, 0], 22],
        [[14, 30, 0, 0], 25],
        [[15, 12, 0, 0], 30],
        [[16, 45, 0], 32],
        [[16, 59, 0], 42]
      ]);

      var options = {
        title: 'Total Emails Received Throughout the Day',
        height: 450
      };

      var chart = new google.charts.Bar(document.getElementById('time_access_bar_chart'));

      chart.draw(data, google.charts.Bar.convertOptions(options));  
          }
          

     }

     return {
          drawBarChart:_drawBarChart,
          drawQuantiyTimeBarChart:_drawQuantiyTimeBarChart,
          drawTimeline:_drawTimeline,
          changeSVGFontSize:_changeSVGFontSize
     };
});
