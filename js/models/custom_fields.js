define(function()
{
     function _InsertCustomField(insert_id, ref_table_id, ref_table)
     {
          // (int,str) -> return field to insert
          // There are fields which need to be inserted into 

          $.ajax(
          {
               type:'POST',
               url: 'utils/get_custom_fields.php?',

               dataType: 'text',
               data: {
                         'ref_table_id':ref_table_id, 
                         'ref_table': ref_table
               },
               success: function(response)
               {

                    var  new_field = JSON.parse(response),
                         label, field_name, radio_html;
                    
                    // make sure fields not empty
                    if (new_field[0] && new_field[0]['field_name'])
                    {
                         field_name = new_field[0]['field_name'];

                         // add label html for new field
                         label = '<div class="form-group row"><div class="col-xs-12 col-sm-4 col-md-4"><label for="'+ref_table+'" class="form-control-label">'+field_name+': <span class="required-field">*</span></label></div>';

                         // send radio buttons to be made.  Inputs are also a possibility however, this hasn't been built.  Also currently everything is required.  If in the future a field will not be required add a field to custom_field_table. 
                         if (new_field[0]['field_type'] === 'radio')  
                         {                  
                              radio_html = MakeRadioButton(new_field[0]['options'], ref_table);
                              radio_html = label + radio_html + '</div>';
                              
                              $('#'+insert_id).append(radio_html);
                         }

                    }
               },
               error: function(textStatus, errorThrown)
               {
                    dialog_window('problem with ajax call ;( ' + errorThrown + ' ' + textStatus);
               }
          });
     }

     function MakeRadioButton(options, field)
     {
          var  option_list = options.split(','),
               radio_html = '<div class="col-xs-12 col-sm-8 col-md-8 pull-left"><div class="radio">';

          option_list.forEach(function(o)
          {
               radio_html+='<label class="radio-inline"><input type="radio" name="'+field+'" required="required" value="'+o+'">'+o+'</label>';
               
          });

          radio_html += '</div></div>';
          return radio_html;
     }

     return {
          InsertCustomField:_InsertCustomField
     };
});
