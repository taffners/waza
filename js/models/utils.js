define(function()
{
     var  _min_tested_ffversion = 38,
          _min_ffversion = 34,
          _min_chromeversion = 71; 

          // increased min version because of html datalist https://caniuse.com/#search=datalist

     function _typeRestrictionField(field_lens)
     {
          let  i,
               field_names = Object.keys(field_lens) 
               num_fields = field_names.length;
console.log(field_lens)          
          for (i=0; i<num_fields; i++)
          {          
               $('input[name="' + field_names[i] +'"]').attr('maxlength', field_lens[field_names[i]]);  
          }  
     }


     function _clone(obj) 
     {
          if (null == obj || "object" != typeof obj) return obj;
          var copy = obj.constructor();
          for (var attr in obj) 
          {
               if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
          }
        
          return copy;
     }

     function _dataAttrToList(str_data_attr)
     {
          return JSON.parse(str_data_attr.replace(/'/g, '"'));
     }

     function _adjustIMGHeight(divs,img_id, set_height)
     {
          // (array, id, int) -> resize img_id
          // Resize img to be the same size as screen minus sections of the layout added in divs array.  Include either #, or . in divs 
          // if necessary for ids and classes.  Provide an int for any set margins.
          var  window_height = $(window).height(),
               included_height = 0,
               i;

          for (i = 0; i < divs.length; i++)
          {
               included_height += $(divs[i]).height();

          }
          
          $('#'+img_id).height(window_height - included_height - set_height);

          _resizeIMGHeight(divs,img_id, set_height);
     }

     function _resizeIMGHeight(divs,img_id, set_height)
     {
          $(window).resize(function()
          {            
               _adjustIMGHeight(divs,img_id, set_height);
          });
     }


     function _saveAsSVG()
     {
          //Purpose: Save the entire SVG

          //vars
          var saveName; //name of save svg

          //add small footer to svg about file name and filter setting.
          // svg_.append('text')
          //      .attr('id', 'tempText')
          //      .attr('x',50 + 'px')
          //      .attr('y', 10 + 'px')
          //      .text('Micro Epitrack')
          //      .style('font-size', '7px')
          //      .attr('text-anchor', 'left');

          //Check if Blob is supported
          try
          {
               var isFileSaverSupported = !!new Blob();

          } 
          catch (e) 
          {
               alertPopUP('This function is not supported by your browser');
          };

          //Select svg and add some saving attributes
          var html = d3.select('svg')
               .attr('title','Micro Epitrack')
               .attr('version', 1.1) //svg version
               .attr('xmlns', 'http://www.w3.org/2000/svg') //XML namespace
               .node().parentNode.innerHTML;

          //convert to a saveable xml
          var blob = new Blob([html], {type: 'image/svg+xml'});

          //prompt use for a name to save svg as
          saveName = window.prompt('Enter file name', 'Micro_Epitrack_bed_trace') + '.svg';

          //if users pushes cancel then the file is not saved
          if (saveName != 'null.svg')
          {
               saveAs(blob, saveName);
          };

          //Delete temp text
          d3.select('#tempText').remove();
     }


     function _getNextMolNum(sample_type)
     {
          var  year;
          
          // get year.  If the sample is not being reported out set molnum year to 00
          if (sample_type === 'Sent Out' || sample_type === 'Validation')
          {
               year = '00';
          }
          else
          {
               year = new Date().getFullYear().toString().substr(-2); 
          }
          
          // use ajax to find last version used 
          $.ajax(
          {
               type:'POST',
               url: 'utils/get_next_mol_num.php?',

               dataType: 'text',
               data: {
                         'year': year
               },
               success: function(response)
               {
                    var  last_mol_num = parseInt(JSON.parse(response)[0]['last_mol_num']),
                         next_mol_num = last_mol_num + 1,
                         mol_num = year+'-MOL'+next_mol_num;

                    $('#order_num').val(mol_num);
               },
               error: function(textStatus, errorThrown)
               {
                    dialog_window('problem with ajax call ;( ' + errorThrown + ' ' + textStatus);
               }
          });
     }

     function _calculateMD5FileHash(input_file_id, md5_input_id) 
     {
          // read a file and make a md5 hash of it
          // (str) -> json (file_name, md5)
          var  reader = new FileReader(),
               md5_hash_result = [];
          
          reader.addEventListener('load',function () 
          {
               var  hash = CryptoJS.MD5(CryptoJS.enc.Latin1.parse(this.result)),
                    md5 = hash.toString(CryptoJS.enc.Hex),
                    filename = document.getElementById(input_file_id).value.split('/').pop().split('\\').pop(),
                    intermediate = {'file_name':filename, 'md5':md5};
                    
                    md5_hash_result.push(intermediate);
                    $('#'+md5_input_id).val(md5);
                    
          });
          
          reader.readAsBinaryString(document.getElementById(input_file_id).files[0]);

          return md5_hash_result;
     }

     function _calculateMulitpleMD5FileHash(multiple_input_file_id, md5_save_input_id, array_name) 
     {
          // read a file and make a md5 hash of the file 
          // Append each md5 hash in md5_save_input_id array.  Then the md5 hash can 
          // be checked in php to make sure entire upload was correct.
          // (str) -> json (file_name, md5)
          var  all_files = document.getElementById(multiple_input_file_id).files;
          
          $.each(all_files, function(i, curr_file_array)
          {
               var  reader = new FileReader();

               reader.addEventListener('load',function () 
               {
                    var  hash = CryptoJS.MD5(CryptoJS.enc.Latin1.parse(this.result)),
                         md5 = hash.toString(CryptoJS.enc.Hex),
                         filename = curr_file_array['name'],
                         md5_html_input;
                         
                         ////////////////////////////////////////// 
                         // append intermediate to the md5_save_input_id
                         //////////////////////////////////////////
                         md5_html_input = '<input type="hidden" name="'+array_name+'[' + filename + ']" value="' + md5 + '">';

                         $('#' + md5_save_input_id).append(md5_html_input);
               });

               reader.readAsBinaryString(curr_file_array)              
          });
     }

     function _isPositveNumber(n)
     {
          return String(Number(n)) === n && Math.floor(Number(n)) > 0;          
     }

     function _update_increament_local_storage_var(var_name, increase_val)
     {    
          // (str, bool) -> int

          ////////////////////////////////////////////// 
          // Add sample and Increase local_storage_var
          //////////////////////////////////////////////
          if (localStorage.getItem(var_name) === null && increase_val) 
          {
               local_storage_var = 1;
               window.localStorage.setItem(var_name, '1');
          }
          else if (localStorage.getItem(var_name) != null && increase_val)
          {
               local_storage_var = parseInt(window.localStorage.getItem(var_name)) + 1;
               window.localStorage.setItem(var_name, local_storage_var);
          }

          ////////////////////////////////////////////// 
          // Remove number and decrease local_storage_var
          //////////////////////////////////////////////
          if (localStorage.getItem(var_name) != null && !increase_val)
          {
               local_storage_var = parseInt(window.localStorage.getItem(var_name)) - 1;
               window.localStorage.setItem(var_name, local_storage_var);
          }

          return local_storage_var
     }

     function _CheckInArray(to_check, checked)
     {
          // (array, array) -> bool
          // (['a', 'b', 'c'], ['a', 'b', 'c']) -> true
          // (['a', 'b', 'c'], ['a', 'b']) -> false
          // (['a', 'b', 'c'], ['a', 'b', 'c', 'd']) -> true
          // (['a', 'b', 1], ['a', 'b', 1]) -> true
          // check if a array of strs or ints are in an array

          var  i = 0, 
               all_found = true;

          for (i; i < to_check.length; i++)
          {
               if (!checked.includes(to_check[i]))
               {                              
                    all_found = false;
                    break;
               }
          }

          return all_found;
     }

     function _GetTodaysDate()
     {
          // purpose: get today's date

          // vars
          var  today = new Date(),
               dd = today.getDate(),
               mm = today.getMonth()+1, //January is 0!
               yyyy = today.getFullYear();

          // add a zero before days and months under ten
          if(dd < 10)
          {
              dd = '0' + dd
          }

          if(mm < 10)
          {
              mm = '0' + mm
          }

          today = mm+'/'+dd+'/'+yyyy;

          return today;
     }

     function _scroll(div_ID)
     {
     	// purose: scroll to a div

          var  _len_find_div = $(div_ID).length;


          // find if div with id exists
          if (_len_find_div === 0)
          {
              
              return 'nope';
          }
          else if(_len_find_div > 1)
          {
               return 'not unique';
          }
     	else
          {
               // scroll to div
               $('html, body').animate(
               {
                    scrollTop: $(div_ID).offset().top
               }, 50);

               return true;
               
          }
     }

     function _GetCurrURL()
     {
          // Purpose:  Get the full current url
          return window.location.href;
     }

     function _PhpVars()
     {
          // Purpose: Get all the php vars if they exist

          // vars
               var	_i,
                    _all_php_vars,
                    _split_php_vars,
                    _split_var,
                    _url = _GetCurrURL(),
                    _curr_php_var = {};

          // find if php vars are present in url
          if (_url.indexOf('?') != -1)
          {
               // Get all of the php vars
               _all_php_vars = _url.split('?')[1];

               // get individual vars in a array

                    // if multiple vars are present
                    if (_all_php_vars.indexOf('&') != -1)
                    {
                         _split_php_vars = _all_php_vars.split('&');
                    }

                    // only page is present
                    else
                    {
                         _split_php_vars = [_all_php_vars];
                    }

               // add all vars in the split_php_vars array to the json php_vars as
               // key = var name and value = value of var
               for (_i=0; _i<_split_php_vars.length; _i++)
               {
                    _split_var = _split_php_vars[_i].split('=');
                    _curr_php_var[_split_var[0]] = _split_var[1];
               }
          }
          return _curr_php_var;
     }

     function _GetHeightClass(class_name)
     {
          // (str) -> int
          // Purpose: Input a class and find the tallest div with the input class and return number of pixels.  If the class wasn't found 0 will be returned

          var _max_h = 0;

          // select all everything with the input class
          $('.' + class_name).each(function()
          {

               // Find the max height
               if ($(this).height() > _max_h)
               {
                    _max_h = $(this).height();
               }
          });
    
          return _max_h;
     }

     function _SetHeightClass(class_name, class_height)
     {
          // (str, int) -> change div height
          // Purpose:  Set the height of an input class

          $('.' + class_name).each(function()
          {
               $(this).height(class_height);
          });
     }

     function _dialog_window(error_message, div_ID)
     {
          // Purpose: display an alert window and scroll to a div
          // prereqs:  must have a dive called AlertID to add this message to.

          // add message
          $('#AlertId').html(error_message);

          // make dialog window
          $('#AlertId').dialog(
          {
               maxHeight: 300,
               maxWidth: 400,
               modal: true,
               buttons:
               [
                    {
                         text: 'close',
                         icon: 'ui-icon-heart',
                         click: function() 
                         {
                              $(this).dialog('close');

                              // Make sure div exists before scrolling to it
                              if ($(div_ID).length > 0)
                              {
                                   _scroll(div_ID);
                              }
                         }
                    }
               ]
          });
     };

     function _confirmPopUp(confrimPopUpHTML, buttonArray)
     {
          // Purpose: display a confirm pop up

          $('#ConfirmId').html(confrimPopUpHTML)

          $('#ConfirmId').dialog(
          {
               resizable: false,
               maxHeight: 400,
               maxWidth: 500,
               modal: true,
               buttons: buttonArray
          });

          // change title
          $('#ConfirmId').dialog('option', 'title', 'WARNING!!!');
     };

     function _CheckBrowser()
     {
          // Purpose: Check if the browser is ok 

          // figure out which browser the user is using. If the browser is not chrome all login and replace with a warning.
          if (/Chrome[\/\s](\d+\.\d+)/.test(navigator.userAgent))
          {
               var chromeversion = new Number(RegExp.$1);

               if (chromeversion < _min_chromeversion)
               {
                    _dialog_window('Testing for on the chrome web browser was performed on version '+_min_chromeversion+'.  You are currently running version ' + chromeversion + '. It is required that you update your browser. If you do not know how to do this follow the steps at this link.  <a href="https://support.google.com/chrome/answer/95414?co=GENIE.Platform%3DDesktop&hl=en" target="_blank">How to upload chrome</a>', '#loginOutsideRow');

                    $('#home-page-id').empty();
               }
               return true;
               // capture x.x portion and store as a number
          }

          // otherwise hide login
          else
          {               
               $('#home-page-id').empty();
               $('#browser_warning').show().removeClass('d-none');
               return false;
          }
     }

     function _MergeArraysToKeyValPairs( keys, vals)
     {
          // (array, array) -> json
          // ([1,2,3], ['a','b','c']) -> {1:'a', 2:'b', 3:'c'}
          // Not tested with lists of different length
          var  myobj = {};

          vals.map(function(curr_val, i)
          {
              

               myobj[keys[i]] = curr_val;
               
               return myobj;
          });

          return myobj;
     }

     return {
          MergeArraysToKeyValPairs:_MergeArraysToKeyValPairs,
          PhpVars:_PhpVars,
          scroll:_scroll,
          GetCurrURL:_GetCurrURL,
          GetHeightClass:_GetHeightClass,
          SetHeightClass:_SetHeightClass,
          dialog_window:_dialog_window,
          confirmPopUp:_confirmPopUp,
          CheckBrowser:_CheckBrowser,
          update_increament_local_storage_var:_update_increament_local_storage_var,
          isPositveNumber:_isPositveNumber,
          CheckInArray:_CheckInArray,
          GetTodaysDate:_GetTodaysDate,
          calculateMD5FileHash:_calculateMD5FileHash,
          getNextMolNum:_getNextMolNum,
          calculateMulitpleMD5FileHash:_calculateMulitpleMD5FileHash,
          clone:_clone,
          saveAsSVG:_saveAsSVG,
          adjustIMGHeight:_adjustIMGHeight,
          dataAttrToList:_dataAttrToList,
          typeRestrictionField:_typeRestrictionField
     };
});
