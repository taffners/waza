define(function()
{
     function _BindEvents(php_vars)
     {
          // Add to tables with a class of dataTable the sorting ability from bootstraps datatables https://datatables.net/manual/styling/bootstrap
          require(['views/events/TableEvents'], function(TableEvents)
          {
               TableEvents.StartEvents();
          });

                    // // In the variant table cells larger than 20 characters are cut.  This will enable the more button and less button
          require(['views/events/toggle_teaser_complete_strs'], function(toggle_teaser_complete_strs)
          {
               toggle_teaser_complete_strs.StartEvents();
          });

     }

     function _SetView()
     {

     }

     function _start(php_vars)
     {
          _SetView();
          _BindEvents(php_vars);
     }

     return {
          start:_start
     };
});
