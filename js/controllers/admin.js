define(['models/utils','models/google_charts'],function(utils, google_charts)
{
     function _BindEvents(php_vars)
     {
         
          // Add to tables with a class of dataTable the sorting ability from bootstraps datatables https://datatables.net/manual/styling/bootstrap
          require(['views/events/TableEvents'], function(TableEvents)
          {
               TableEvents.StartEvents();
          });

          // enable submit button once a change has occurred in type=text fields
          require(['views/events/activate_submit_button'], function(activate_submit_button)
          {
               activate_submit_button.StartEvents();
          });

          // enable date field calendar popups
          require(['views/events/date_fields'], function(date_fields)
          {
               date_fields.StartEvents();
          });

          // // In the variant table cells larger than 20 characters are cut.  This will enable the more button and less button
          require(['views/events/toggle_teaser_complete_strs'], function(toggle_teaser_complete_strs)
          {
               toggle_teaser_complete_strs.StartEvents();
          });

          // change permissions on account
          $('.add-permission').on('click', function()
          {
               var  user_id = $(this).data('user-id'),
                    permission = $(this).data('permission'),
                    curr_permissions = $(this).data('curr-permissions');

               $.ajax(
               {
                    type:'POST',
                    url: 'utils/update_permission.php?',

                    dataType: 'text',
                    data: {
                              'user_id': user_id,
                              'permission': permission,
                              'curr_permissions':curr_permissions 

                    },
                    success: function(response)
                    {

                         window.location.reload(true)

                    },
                    error: function(textStatus, errorThrown)
                    {
                         dialog_window('problem with ajax call ;( ' + errorThrown + ' ' + textStatus);
                    }
               });

          });

          // lock account
          $('.lock-account').on('click', function()
          {
               var  email_address = $(this).data('email-address');

               $.ajax(
               {
                    type:'POST',
                    url: 'utils/lock_account.php?',

                    dataType: 'text',
                    data: {
                              'email_address': email_address 
                    },
                    success: function(response)
                    {

                         window.location.reload(true)

                    },
                    error: function(textStatus, errorThrown)
                    {
                         dialog_window('problem with ajax call ;( ' + errorThrown + ' ' + textStatus);
                    }
               });
          });

          // unlock account
          $('.unlock-account').on('click', function()
          {
               var  email_address = $(this).data('email-address');

               $.ajax(
               {
                    type:'POST',
                    url: 'utils/unlock_account.php?',

                    dataType: 'text',
                    data: {
                              'email_address': email_address 

                    },
                    success: function(response)
                    {

                         window.location.reload(true)

                    },
                    error: function(textStatus, errorThrown)
                    {
                         utils.dialog_window('problem with ajax call ;( ' + errorThrown + ' ' + textStatus);
                    }
               });
          });

          // add summary of logins Google horizontal bar chart
          $.ajax(
               {
                    type:'POST',
                    url: 'utils/get_user_access_info.php?',

                    dataType: 'text',
                    data: {
                              'access_filter': php_vars['access_filter']
                    },
                    success: function(response)
                    {
                       
                         if (response ==='Post Does not contain access_filter')
                         {

                         }
                         else
                         {
                              var  json_response = JSON.parse(response),
                                   chart_data = [['email','passed', 'failed']],
                                   curr_user;
                                  
                              // Get all the possible curr_status
                              json_response.forEach(function(val, i)
                              {
                                   curr_user = [val['email_address'], parseInt(val['passed']), parseInt(val['failed'])];
                                   chart_data.push(curr_user);
                              });   
                              console.log(chart_data);                           
                              google_charts.drawBarChart('login_details', 'Login Summary', chart_data, '', 'horizontal');
                         }

                    },
                    error: function(textStatus, errorThrown)
                    {
                         utils.dialog_window('problem with ajax call ;( ' + errorThrown + ' ' + textStatus);
                    }
               });
         
          // add a summary of time chart to see when people are using most often
          google_charts.drawQuantiyTimeBarChart();
         

     }

     function _SetView()
     {

     }

     function _start(test_status, php_vars)
     {
          _SetView();
          _BindEvents(php_vars);
     }

     return {
          start:_start
     };
});
