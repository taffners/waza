define(['models/utils'], function(utils)
{
     function _BindEvents(php_vars)
     {
          // if the browser is not chrome 
          utils.CheckBrowser();

          // Add to tables with a class of dataTable the sorting ability from bootstraps datatables https://datatables.net/manual/styling/bootstrap
          require(['views/events/TableEvents'], function(TableEvents)
          {
               TableEvents.StartEvents();
          });

          // // In the variant table cells larger than 20 characters are cut.  This will enable the more button and less button
          require(['views/events/toggle_teaser_complete_strs'], function(toggle_teaser_complete_strs)
          {
               toggle_teaser_complete_strs.StartEvents();
          });


          $('.btn-number').on('click',function(e)
          {
              e.preventDefault();
              
               var  fieldName = $(this).attr('data-field'),
                    type = $(this).attr('data-type'),
                    input = $("input[name='"+fieldName+"']"),
                    currentVal = parseInt(input.val()),
                    step = parseInt($(this).data('step'));
            
               if (!isNaN(currentVal)) 
               {
                    if(type == 'minus') 
                    {    
                         if(currentVal > input.attr('min')) 
                         {
                              input.val(currentVal - step).change();
                         } 
                         if(parseInt(input.val()) == input.attr('min')) 
                         {
                              $(this).attr('disabled', true);
                         }

                    } 
                    else if(type == 'plus') 
                    {
                         input.val(currentVal + step).change();

                    }
               } 
               else 
               {
                    input.val(step);
               }
          });

          $('.input-number').on('focusin',function()
          {
               $(this).data('oldValue', $(this).val());
          });

          $('.input-number').on('change',function() 
          {
    
               var  minValue =  parseInt($(this).attr('min')),
                    
                    valueCurrent = parseInt($(this).val()),
                    name = $(this).attr('name');

               if(valueCurrent >= minValue) 
               {
                    $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
               } 
               else 
               {
                    alert('Sorry, the minimum value was reached');
                    $(this).val($(this).data('oldValue'));
               }               
          });

          $(".input-number").on('keydown',function (e) 
          {
               // Allow: backspace, delete, tab, escape, enter and .
               if   (
                         $.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
                         // Allow: Ctrl+A
                         (e.keyCode == 65 && e.ctrlKey === true) || 
                         // Allow: home, end, left, right
                         (e.keyCode >= 35 && e.keyCode <= 39)
                    ) 
               {
                    // let it happen, don't do anything
                    return;
               }

               // Ensure that it is a number and stop the keypress
               if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) 
               {
                    e.preventDefault();
               }
          });
    
    
    
     }

     function _SetView()
     {

     }

     function _start(php_vars)
     {
          _SetView();
          _BindEvents(php_vars);
     }

     return {
          start:_start
     };
});
