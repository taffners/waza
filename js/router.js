define(['models/utils'],function(utils)
{
     // Set up a single page application router.
     var  _routes =   [
                         // Login user pages
                         {page: 'generic', controller:'generic'},
                         {page: 'home', controller:'home'},
                         {page: 'samples_pass_qc', controller:'generic'},
                         {page: 'bed_trace_timeline', controller:'bed_trace_timeline'},
                         {page: 'dev_progress_report', controller:'dev_progress_report'},
                         {page: 'add_epi_data', controller:'add_epi_data'},
                         {page: 'login', controller:'login'},
                         {page: 'admin', controller:'admin'},
                         
                         {page: 'add_sample_data', controller:'add_sample_data'},
                         {page: 'sample_book', controller:'generic'} ,
                         {page: 'add_batch_sample_data', controller:'add_batch_sample_data'},
                         {page: 'add_species', controller:'add_species'},
                         {page: 'add_hybrid_sample_data', controller:'add_hybrid_sample_data'}
                         
                         
                         
                    ],
          // _default_page = 'login', // use this as default once login is built
          default_page = 'home',
          _php_vars = utils.PhpVars(),
          _page = '',
          _browser_ok,
          _test_status;

     function _startRouting(test_status)
     {
          // Purpose: Set route to default and start the hash check which starts the correct controller
          _hashCheck();
     }

     function _SetTestStatus(test_status)
     {
          _test_status = test_status;
     }

     function _hashCheck()
     {
          // Purpose: Check to see the anchor part of the url
          let  _i,
               _current_route,
               // _curr_page = _GetCurrPage(),

               url = new URL(window.location.href),
               params = new URLSearchParams(url.search.slice(1)),
               _curr_page = params.get('page'),
               _found_router = false;

          // reset php_vars to current
          _php_vars = utils.PhpVars();

          // check if hash has changed
          if (_curr_page != _page)
          {
               // if it changed find the controller of the changed hash and load that controller
               for (_i = 0; _current_route = _routes[_i++];)
               {
                    if (_curr_page === _current_route.page)
                    {
                         // show uploader when submit button is pushed
                         $('form, .submit-show-loader').on('submit', function(e)
                         {
                              // do not activate rainbow loader if form did not validate
                              $('#init-hide-loader').show();
                         });

                         _loadController(_current_route.controller);
                         _found_router = true;
                    }
                    
               }

               // If a router was not found set to the default router generic
               if (!_found_router)
               {
                    // show uploader when submit button is pushed
                    $('form, .submit-show-loader').on('submit', function(e)
                    {
                         // do not activate rainbow loader if form did not validate
                         $('#init-hide-loader').show();
                    });


                    _loadController(_routes[0].controller);
               }



               // load common classes 
               require(['views/events/forms/common_form_classes'], function(common_form_classes)
               {
                    common_form_classes.StartEvents();
               });

               // set page to current page
               _page = _curr_page;
          }

     }

     function _loadController(controller_name)
     {
          // Purpose: Change the pages which are loaded if the hash was changed via require

          require(['controllers/' + controller_name], function(controller)
          {            

               controller.start(_test_status, _php_vars);
          });

     }

     function _SetLocalStorageVars()
     {
          //Purpose: Set Local Storage variables

          // make sure local storage is supported
          if (typeof(Storage) !== 'undefined')
          {

          }
          else
          {
               $('#message_center')
                    .attr('class', 'alert alert-danger show')
                    .append('Sorry, your browser does not support web storage...');
          }
     }

     function _GetCurrPage()
     {
          // Purpose: Get the current page in phpVars if page is not in php_vars make page the default_page.  If the home page or login is loaded make sure page is set to home or login.


          // load pages where page is defined and not equal to login or home
          if ( ('page' in _php_vars) && ( _php_vars['page'] != 'home'))
          {
               return _php_vars['page'];
          }

          // if page is not defined redirect to login
          else if (_php_vars['page'] != 'home')
          {
               window.location.href = "?page=home";
          }

     }

     return {
          startRouting: _startRouting,
          hashCheck:_hashCheck,
          SetLocalStorageVars:_SetLocalStorageVars
     };

});
