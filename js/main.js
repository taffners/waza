var site_version = $('#site-version-num').html();

require.config({
     urlArgs: 'version='+site_version
});

require(['router'], function(router)
{	     
	// stop submitting form with pressing enter for entire site.
     $('#make_run_template_form').on('keyup keypress', function(e) 
     {
          var keyCode = e.keyCode || e.which;
          
          if (keyCode === 13) 
          { 
               e.preventDefault();
               return false;
          }
     });
     
     // add title info for a standard format for the entire page for somethings
     $('.required-field').each(function()
     {
          $(this).prop('title','This is a required field');
     });

     $('.mulitple-field').each(function()
     {
          $(this).prop('title','Multiple can be selected');
     });


     $('.editable-select').editableSelect();

     // change check boxes required but only require one checked.
     var  requiredCheckboxes = $(':checkbox[required]');
     
     $(':checkbox[required]').on('change', function(e) 
     {
          var  checkboxGroup = requiredCheckboxes
                    .filter('[name="' + $(this).attr('name') + '"]'),
               isChecked = checkboxGroup
                    .is(':checked');               
               checkboxGroup.prop('required', !isChecked);
     });


     // add a function to d3 to find the last element of a selection
     d3.selection.prototype.last = function() 
     {
          return d3.select(
             this.nodes()[this.size() - 1]
          );
     };

     // start the router
     router.startRouting();

});
