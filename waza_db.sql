-- MySQL dump 10.14  Distrib 5.5.68-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: waza_dev
-- ------------------------------------------------------
-- Server version	5.5.68-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `login_table`
--

DROP TABLE IF EXISTS `login_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `login_table` (
  `login_id` int(11) NOT NULL AUTO_INCREMENT,
  `email_address` varchar(40) DEFAULT NULL,
  `ip_address` varchar(16) DEFAULT NULL,
  `ip_loc` varchar(30) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ip_x_forwarded_for` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`login_id`),
  UNIQUE KEY `login_id` (`login_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `login_table`
--

LOCK TABLES `login_table` WRITE;
/*!40000 ALTER TABLE `login_table` DISABLE KEYS */;
INSERT INTO `login_table` VALUES (1,'taffners@gmail.com','10.149.57.131','','passed','2021-03-24 14:43:22',''),(2,'taffners@gmail.com','10.149.57.131','','passed','2021-03-24 15:06:58',''),(3,'taffners@gmail.com','10.149.57.131','','passed','2021-03-24 15:21:03',''),(4,'taffners@gmail.com','10.149.57.131','','passed','2021-03-24 17:32:03',''),(5,'taffners@gmail.com','10.149.57.131','','passed','2021-03-25 15:01:05',''),(6,'taffners@gmail.com','10.149.57.131','','passed','2021-03-25 16:20:32',''),(7,'taffners@gmail.com','10.149.57.131','','passed','2021-03-25 18:16:13',''),(8,'taffners@gmail.com','10.149.57.131','','passed','2021-03-25 19:14:46','');
/*!40000 ALTER TABLE `login_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meal_table`
--

DROP TABLE IF EXISTS `meal_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meal_table` (
  `meal_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `twenty_four_hour_plan_id` int(11) DEFAULT NULL,
  `meal_type_id` int(11) DEFAULT NULL,
  `meal_description` varchar(255) DEFAULT NULL,
  `calorie_count` int(11) DEFAULT NULL,
  `wait_tile_hungry` varchar(1) DEFAULT NULL,
  `stop_when_satisfied` varchar(1) DEFAULT NULL,
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`meal_id`),
  UNIQUE KEY `meal_id` (`meal_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meal_table`
--

LOCK TABLES `meal_table` WRITE;
/*!40000 ALTER TABLE `meal_table` DISABLE KEYS */;
INSERT INTO `meal_table` VALUES (5,1,1,1,NULL,NULL,NULL,NULL,'2021-03-24 18:06:09'),(6,1,1,2,NULL,NULL,NULL,NULL,'2021-03-24 18:06:09'),(7,1,1,3,NULL,NULL,NULL,NULL,'2021-03-24 18:06:09'),(8,1,1,4,NULL,NULL,NULL,NULL,'2021-03-24 18:06:09'),(9,1,2,1,NULL,NULL,NULL,NULL,'2021-03-25 15:01:05'),(10,1,2,2,NULL,NULL,NULL,NULL,'2021-03-25 15:01:05'),(11,1,2,3,NULL,NULL,NULL,NULL,'2021-03-25 15:01:05'),(12,1,2,4,NULL,NULL,NULL,NULL,'2021-03-25 15:01:05');
/*!40000 ALTER TABLE `meal_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meal_type_table`
--

DROP TABLE IF EXISTS `meal_type_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meal_type_table` (
  `meal_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `meal_name` varchar(10) DEFAULT NULL,
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`meal_type_id`),
  UNIQUE KEY `meal_type_id` (`meal_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meal_type_table`
--

LOCK TABLES `meal_type_table` WRITE;
/*!40000 ALTER TABLE `meal_type_table` DISABLE KEYS */;
INSERT INTO `meal_type_table` VALUES (1,1,'Breakfast','2021-03-24 17:45:11'),(2,1,'Lunch','2021-03-24 17:45:11'),(3,1,'Dinner','2021-03-24 17:45:11'),(4,1,'Snack','2021-03-24 17:45:11');
/*!40000 ALTER TABLE `meal_type_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_table`
--

DROP TABLE IF EXISTS `permission_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_table` (
  `permission_id` int(11) NOT NULL AUTO_INCREMENT,
  `permission` varchar(15) DEFAULT NULL,
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`permission_id`),
  UNIQUE KEY `permission_id` (`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_table`
--

LOCK TABLES `permission_table` WRITE;
/*!40000 ALTER TABLE `permission_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `permission_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_user_xref`
--

DROP TABLE IF EXISTS `permission_user_xref`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_user_xref` (
  `permission_user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `permission_id` int(11) DEFAULT NULL,
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`permission_user_id`),
  UNIQUE KEY `permission_user_id` (`permission_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_user_xref`
--

LOCK TABLES `permission_user_xref` WRITE;
/*!40000 ALTER TABLE `permission_user_xref` DISABLE KEYS */;
/*!40000 ALTER TABLE `permission_user_xref` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `top_passwords_used_table`
--

DROP TABLE IF EXISTS `top_passwords_used_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `top_passwords_used_table` (
  `top_password_id` int(11) NOT NULL AUTO_INCREMENT,
  `email_address` varchar(40) DEFAULT NULL,
  `password` varchar(60) DEFAULT NULL,
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`top_password_id`),
  UNIQUE KEY `top_password_id` (`top_password_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `top_passwords_used_table`
--

LOCK TABLES `top_passwords_used_table` WRITE;
/*!40000 ALTER TABLE `top_passwords_used_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `top_passwords_used_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `twenty_four_hour_plan_table`
--

DROP TABLE IF EXISTS `twenty_four_hour_plan_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `twenty_four_hour_plan_table` (
  `twenty_four_hour_plan_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `eat_healthy_score` int(11) DEFAULT NULL,
  `move_daily_score` int(11) DEFAULT NULL,
  `mindset_score` int(11) DEFAULT NULL,
  `plan_date` date DEFAULT NULL,
  `follow_plan` varchar(1) DEFAULT NULL,
  `why_or_why_not_follow_plan` text,
  `excuses_using` varchar(255) DEFAULT NULL,
  `plan_for_success_tomorrow` varchar(255) DEFAULT NULL,
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `description_of_movement` varchar(255) DEFAULT NULL,
  `num_steps_completed` int(11) DEFAULT NULL,
  `num_work_outs_completed` int(11) DEFAULT NULL,
  `num_strength_training_completed` int(11) DEFAULT NULL,
  `goals` text,
  `whys` text,
  PRIMARY KEY (`twenty_four_hour_plan_id`),
  UNIQUE KEY `twenty_four_hour_plan_id` (`twenty_four_hour_plan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `twenty_four_hour_plan_table`
--

LOCK TABLES `twenty_four_hour_plan_table` WRITE;
/*!40000 ALTER TABLE `twenty_four_hour_plan_table` DISABLE KEYS */;
INSERT INTO `twenty_four_hour_plan_table` VALUES (1,1,NULL,NULL,NULL,'2021-03-24',NULL,NULL,NULL,NULL,'2021-03-24 16:49:01',NULL,NULL,NULL,NULL,NULL,NULL),(2,1,NULL,NULL,NULL,'2021-03-25',NULL,NULL,NULL,NULL,'2021-03-25 15:01:05',NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `twenty_four_hour_plan_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_table`
--

DROP TABLE IF EXISTS `user_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_table` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `email_address` varchar(40) DEFAULT NULL,
  `credentials` varchar(255) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `password_need_reset` varchar(1) DEFAULT '1',
  `password` varchar(60) DEFAULT 'ba816f76d6c8402851d3b849fa690892',
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_id` (`user_id`),
  UNIQUE KEY `email_address` (`email_address`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_table`
--

LOCK TABLES `user_table` WRITE;
/*!40000 ALTER TABLE `user_table` DISABLE KEYS */;
INSERT INTO `user_table` VALUES (1,'Samantha','Taffner','taffners@gmail.com','MS','Bioinformatics Analyst','0','65ec9863f2436a589491a25ccc139277','2021-03-24 14:39:39');
/*!40000 ALTER TABLE `user_table` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-03-25 17:12:52
