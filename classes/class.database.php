<?php

	class DataBase extends Utils
	{
		private $conn;
		private $ip;
		private $ip_loc;

		// cryptor methods
		private $cipher = 'AES-128-CBC'; // advanced encryption standard
		private $cryp_key;
		private $ip_x_forwarded_for;
	
		public function __construct($conn, $cryp_key, $site_title, $root_url)
		{
			$this->conn = $conn;
			$this->cryp_key = $cryp_key;
			$this->site_title = $site_title;
			$this->root_url = $root_url;
		}

		public function SetIP()
		{
			if(isset($_SERVER['HTTP_CLIENT_IP']))
			{
			// This will fetch the IP address when user is from Shared Internet services.
				$this->ip = $_SERVER['HTTP_CLIENT_IP'];
				
			}
			elseif (isset($_SERVER['REMOTE_ADDR']))
			{			
				// This isn't very reliable contains the real IP address of the client. That is the most reliable value you can find from the user.
				$this->ip = $_SERVER['REMOTE_ADDR'];			
			}
			else
			{
				$this->ip = 'unknown';
			}	

			if ($this->ip == '::1')
			{
				$this->ip = '128.151.71.16';
			}	

			$this->ip_x_forwarded_for = NULL;

			if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
			{
				$this->ip_x_forwarded_for = $_SERVER['HTTP_X_FORWARDED_FOR'];
			}
		}

		private function IPInfo($ip, $purpose = "location") 
		{		
			// Not being used!!
			$output = NULL;

			$purpose = str_replace(array("name", "\n", "\t", " ", "-", "_"), NULL, strtolower(trim($purpose)));
			$support    = array("country", "countrycode", "state", "region", "city", "location", "address");
			$continents = array(
				"AF" => "Africa",
				"AN" => "Antarctica",
				"AS" => "Asia",
				"EU" => "Europe",
				"OC" => "Australia (Oceania)",
				"NA" => "North America",
				"SA" => "South America"
			);

			if (filter_var($ip, FILTER_VALIDATE_IP) && in_array($purpose, $support)) 
			{
				// 
				$ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $this->ip));
				
				if (@strlen(trim($ipdat->geoplugin_countryCode)) == 2) 
				{
					switch ($purpose) 
					{
						case "location":
							$output = array(
							    "city"           => @$ipdat->geoplugin_city,
							    "state"          => @$ipdat->geoplugin_regionName,
							    "country"        => @$ipdat->geoplugin_countryName,
							    "country_code"   => @$ipdat->geoplugin_countryCode,
							    "continent"      => @$continents[strtoupper($ipdat->geoplugin_continentCode)],
							    "continent_code" => @$ipdat->geoplugin_continentCode,
							    "ip" 			 => $ip
							);

						break;

						case "address":
							$address = array($ipdat->geoplugin_countryName);
							if (@strlen($ipdat->geoplugin_regionName) >= 1)
							{
								$address[] = $ipdat->geoplugin_regionName;
							}
		    
							if (@strlen($ipdat->geoplugin_city) >= 1)
							{
								$address[] = $ipdat->geoplugin_city;
							}

							$output = implode(", ", array_reverse($address));
							break;
		
						case "city":
							$output = @$ipdat->geoplugin_city;
							break;
						
						case "state":
							$output = @$ipdat->geoplugin_regionName;
							break;
						
						case "region":
							$output = @$ipdat->geoplugin_regionName;
							break;
		
						case "country":
							$output = @$ipdat->geoplugin_countryName;
							break;
		
						case "countrycode":
							$output = @$ipdat->geoplugin_countryCode;
							break;
					}
				}
			}
		
			return $output;
		}

		public function GetTableColNames($table)
		{
			$cols = $this->listAll('get-table-col-names', $table);

			$collasped_cols = array();

			foreach ($cols as $key => $value)
			{

				array_push($collasped_cols, $value['COLUMN_NAME']);
			}

			return $collasped_cols;
		}

		public function updateRecord($table, $primary_key, $primary_key_val, $update_field, $update_val)
		{
			$update = 'UPDATE '.$this->sanitize($table).' SET '.$this->sanitize($update_field).' = "'.$this->sanitize($update_val).'"'.' WHERE '.$this->sanitize($primary_key).' = "'.$this->sanitize($primary_key_val).'"';

			$result = mysqli_query($this->conn, $update) or die('Oops!! Died '.$update);

			// get the Id of the inserted record
			$lastId = mysqli_insert_id($this->conn);

			return [$result, $lastId];
		}

		public function addOrModifyRecord($table, $inputs)
		{
			
			$count = 0;
			$replace = 'REPLACE '.$this->sanitize($table).' SET ';

			foreach($inputs as $name => $value)
			{
				// only include column if it is present in table
				$table_cols = $this->GetTableColNames($table);

				if (empty($table_cols))
				{
					die('Oops!! The following table is not found: '.$table);
				}

				$count++;

				if (in_array($name, $table_cols))
				{
					// if field is null, type int, and allows nulls do not sanitize use null as value 
					if (empty($value) && $value !== 0)
					{
						if (!isset($colInfo))
						{
							$colInfo = $this->listAll('db-column-info', $table);
						}

						// find column info in array of all field info in table $colInfo.  It is done this way to reduce queries of db.
						if (isset($colInfo) && !empty($colInfo))
						{
							foreach ($colInfo as $key => $col)
							{
								if ($col['COLUMN_NAME'] === $name)
								{
									if 	(
											(
												$col['DATA_TYPE'] === 'float' ||
												$col['DATA_TYPE'] === 'int' ||
												$col['DATA_TYPE'] === 'decimal'
											)
											&&

											(
												$col['IS_NULLABLE'] === 'YES'
											)
										)
									{
										$replace .= $this->sanitize($name).' = NULL';
									}
									else
									{
										$replace .= $this->sanitize($name).' = "'.$this->sanitize($value).'"';
									}
									
									break;
								}
							}
						}
						else
						{
							$replace .= $this->sanitize($name).' = "'.$this->sanitize($value).'"';
						}
					}
					else
					{
						$replace .= $this->sanitize($name).' = "'.$this->sanitize($value).'"';
					}					

					if($count < sizeof($inputs))
					{
						$replace .= ', ';
					}
				}
			}
			// remove comma at the end of replace statement
			$replace = rtrim($replace, ', ');

			// add to log
			// $this->AddLog('addOrModifyRecord', $table, $replace);		
			$result = mysqli_query($this->conn, $replace) or die('Oops!! Died '.$replace);

			// get the Id of the inserted record
			$lastId = mysqli_insert_id($this->conn);

			return [$result, $lastId];
		}

		public function deleteRecord($table, $inputs)
		{
			$count = 0;
			$delete = 'DELETE FROM '.$this->sanitize($table).' WHERE ';

			foreach($inputs as $name => $value)
			{
				$delete .= $this->sanitize($name).' = "'.$this->sanitize($value).'"';

				$count++;

				if($count < sizeof($inputs))
				{
					$delete .= ' AND ';
				}
			}

			// add to log
			$this->AddLog('Delete', $table, $delete);

			$result = mysqli_query($this->conn, $delete) or die($delete);
			$lastId = mysqli_insert_id($this->conn);

			return [$result, $lastId];
		}

		public function deleteRecordById($table, $id_name, $id)
		{
			$count = 0;

			// Search for deleted info so it can be stored in log table
			$to_delete = $this->listAll('search-table-and-id', array('table'=>$table,'id_name'=>$id_name,'id'=>$id));
						
			if (!empty($to_delete))
			{
				$record = json_encode($to_delete[0]);
			}
			else
			{
				$record = '';
			}


			$delete = 'DELETE FROM '.$this->sanitize($table).' WHERE '.$this->sanitize($id_name).' = '.$this->sanitize($id);

			// // add to log
			$this->AddLog('Delete', $table, $record);

			$result = mysqli_query($this->conn, $delete) or die($delete);
			$lastId = mysqli_insert_id($this->conn);

			return [$result, $lastId];
		}

		public function deleteExtraFieldWithDeleteStatus($extra_field_array)
		{
			// (array) -> bool
			// Delete fields with keep_status delete.  Make sure table, id_name, and id_value are not empty
			// Array format:
				// array (size=6)
				  // 1 => 
				  //   array (size=5)
				  //     'keep_status' => string 'keep' (length=4)
				  //     'table_name' => string 'previous_names_table' (length=20)
				  //     'id_name' => string 'previous_names_id' (length=17)
				  //     'id_val' => string '1' (length=1)
				  //     'field' => string 'previous_name' (length=13)
				  // 2 => 
				  //   array (size=5)
				  //     'keep_status' => string 'delete' (length=6)
				  //     'table_name' => string 'linked_samples_table' (length=20)
				  //     'id_name' => string 'linked_samples_id' (length=17)
				  //     'id_val' => string '1' (length=1)
				  //     'field' => string 'link_summary' (length=12)

			$delete_correctly = true;			

			foreach ($extra_field_array as $key => $field)
			{
				if ($field['keep_status'] === 'delete')
				{
					$del_result = $this->deleteRecordById($field['table_name'], $field['id_name'], $field['id_val']);

					if (!$del_result[0])
					{
						$delete_correctly = false;
					}
				}				
			}
			return $delete_correctly;
		}

		public function AddLog($action, $table, $record)
		{
			// Every action to the database needs to add a log to log_table
			// possible actions: 

			// What to log:
				// IP Address
				// user id
				// action (login, listAll, addOrModifyRecord, Delete)
				// table
				// record => JSON Encoded string of the record
				// time_stamp

			if (defined('USER_ID'))
			{
				$user_id = $this->sanitize(USER_ID);				
			}	
			else
			{
				$user_id = 0;
			}		
			
			$log_insert = 'INSERT INTO log_table (user_id, ip_address, action, table_name) VALUES';
			$log_insert .= '("'.$user_id.'", "';
			
			$log_insert .= isset($this->ip) ? $this->sanitize($this->ip): '';

			$log_insert .= '", "'.$this->sanitize($action).'", ';
			$log_insert .= '"'.$this->sanitize($table).'")';

			$result = mysqli_query($this->conn, $log_insert);
			
			// get the Id of the inserted record
			$lastId = mysqli_insert_id($this->conn);		
		}

		public function addDbSchema($db_schema)
		{
			// input example:
			 // 'original_header' => string 'Case Status' (length=11)
			 //  'col' => string 'sample_group' (length=12)
			 //  'add_table' => string 'epi_table' (length=9)
			 //  'col_type' => string 'VARCHAR(100)' (length=12)
			
			// create the add_table
			if (!empty($db_schema['add_table']) && !empty($db_schema['col']) && !empty($db_schema['col_type']))
			{
				$this->createTable($db_schema['add_table']);
			
				// add column if it does not exist
			
				$this->addDbCol($db_schema['add_table'], $db_schema['col'], $db_schema['col_type']);
			}
		}

		private function addDbCol($table, $col, $col_type)
		{
			// add a column to an existing table.

			if ($this->dbTableExists($table) && !$this->colExistsInTable($table, $col))
			{
				$alter_table = 'ALTER TABLE '.$table.' ADD '.$col.' '.$col_type;		

				$result = mysqli_query($this->conn, $alter_table) or die('Oops!! Died '.$alter_table);
			}
		}

		private function colExistsInTable($table, $col)
		{
			return $this->allValuesPresentInArray($this->GetTableColNames($table), array($col));
		}

		private function dbTableExists($table_name)
		{
			if ($this->listAll('check-table-exists', $table_name) !== FALSE)
			{
				return True;
			}
			else
			{
				return False;
			}
		}

		private function createTable($table_name)
		{
			// Create a table if it does not exist.

			if ($this->dbTableExists($table_name))		
			{
				return TRUE;
			}
			else
			{
				$table_id = str_replace('_table', '_id', $this->sanitize($table_name));
				$create_sql = 'CREATE TABLE IF NOT EXISTS '.$this->sanitize($table_name).'(
	'.$table_id.' int NOT NULL AUTO_INCREMENT, PRIMARY KEY ('.$table_id.'), UNIQUE KEY '.$table_id.' ('.$table_id.') )ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1';
				
				$result = mysqli_query($this->conn, $create_sql) or die('Oops!! Died '.$create_sql);

				// get the Id of the inserted record
				$lastId = mysqli_insert_id($this->conn);

				return [$result, $lastId];
			}
		}

		public function listAll($query, $where=NULL)
		{

			switch($query)
			{
				case '24-hour-plan-by-date-and-user':
					$select =
					'
					SELECT 	*

					FROM 	twenty_four_hour_plan_table

					WHERE 	user_id = "'.$this->sanitize($where['user_id']).'" AND
							plan_date = "'.$this->sanitize($where['plan_date']).'"
					';
					break;

				case 'meals-for-day':
					$select = 
					'
					SELECT 		mt.*,
								mtt.meal_name

					FROM 		meal_table mt 

					LEFT JOIN 	meal_type_table mtt 
					ON 			mtt.meal_type_id = mt.meal_type_id

					WHERE 		mt.user_id = "'.$this->sanitize($where['user_id']).'" AND
								mt.twenty_four_hour_plan_id = "'.$this->sanitize($where['twenty_four_hour_plan_id']).'"

					ORDER BY 	mt.meal_type_id
					';
					break;

				case 'meal-types':
					$select =
					'
					SELECT 		*

					FROM 		meal_type_table
					';
					break;

				case 'search-seq-pool':
					$select = 
					'
					SELECT 	*

					FROM 	sequencing_pool_table spt

					WHERE 	spt.sequencing_run_date = "'.$this->sanitize($where['sequencing_run_date']).'" AND
							spt.seqencing_status = "'.$this->sanitize($where['seqencing_status']).'" AND
							spt.backup_location = "'.$this->sanitize($where['backup_location']).'" 
					';
					break;

				case 'search-for-sample-by-ngs-sample-name':
					$select = 
					'
					SELECT 		sit.*,
								st.mrn,
								st.soft_id,
								st.patient_initials,
								CONCAT(ut.first_name," " ,ut.last_name) AS user_name 

					FROM 		sequencing_info_table sit

					LEFT JOIN 	user_table ut
					ON 			ut.user_id = sit.user_id

					LEFT JOIN 	sample_name_table snt 
					ON 			snt.sample_name_id = sit.sample_name_id

					LEFT JOIN 	sample_table st 
					ON 			st.sample_id = snt.sample_id

					WHERE 		sit.ngs_sample_name = "'.$this->sanitize($where).'"
					';				
					break;

				case 'search-table-and-id':
					$select = 
					'
					SELECT 	*

					FROM 	'.$this->sanitize($where['table']).'

					WHERE 	'.$this->sanitize($where['id_name']).' = "'.$this->sanitize($where['id']).'"
					';								
					break;

				case 'previous-comments':
					$select =
					'
					SELECT 	ct.comment,
							ct.time_stamp,
							CONCAT(ut.first_name," " ,ut.last_name) AS user_name

					FROM 	comment_table ct

					LEFT JOIN user_table ut 
					ON 		ut.user_id = ct.user_id 

					WHERE 	ct.comment_type = "'.$this->sanitize($where['comment_type']).'" AND 
							ct.comment_ref = "'.$this->sanitize($where['comment_ref']).'"

					ORDER BY 	ct.time_stamp ASC
					';
					break;

				case 'user':
					$select = 
					'
					SELECT 		ut.*

					FROM 		user_table ut

					WHERE 		ut.user_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'get-all-users-with-a-permission':
					$select =
					'
					SELECT 	pt.permission,
							CONCAT(ut.first_name," " ,ut.last_name) AS user_name,
							ut.email_address 

					FROM 	permission_table pt

					LEFT JOIN permission_user_xref pux
					ON 		pux.permission_id = pt.permission_id

					LEFT JOIN user_table ut
					ON 		ut.user_id = pux.user_id

					WHERE 	pt.permission = "'.$this->sanitize($where).'"
					';				
					break;

				case 'alt_species-by-name':
					$select = 
					'
					SELECT 	*

					FROM 	species_other_name_table sont

					WHERE 	other_genus = '.$this->sanitize($where['other_genus']).' AND
							other_species = '.$this->sanitize($where['other_species']).' AND 
							species_id = '.$this->sanitize($where['species_id']).'
					';
					break;

				case 'all-genus-species':
					$select = 
					'
					SELECT  	st.species_id,
								CONCAT(st.genus,"_" ,st.species) AS genus_species,
								st.abbrevation

					FROM 		species_table st

					ORDER BY 	st.genus ASC,
								st.species ASC
					';
				
					break;

				case 'all-species':
					$select =
					'
					SELECT  	st.*,
								CONCAT(ut.first_name," " ,ut.last_name) AS user_name,
								st.abbrevation AS abbrev_species,
								GROUP_CONCAT(CONCAT(sont.other_genus, "_", sont.other_species) SEPARATOR ", ") AS alt_species_name,
								spsc_vw.species_sample_count

					FROM 		species_table st

					LEFT JOIN 	user_table ut 
					ON 			ut.user_id = st.user_id

					LEFT JOIN 	species_other_name_table sont 
					ON 			st.species_id =sont.species_id

					LEFT JOIN 	sample_per_species_count_vw spsc_vw 
					ON 			spsc_vw.species_id = st.species_id

					GROUP BY 	st.species_id, 
								sont.species_id

					ORDER BY 	st.genus ASC,
								st.species ASC

					';
					break;

				case 'species-by-name':
					$select =
					'
					SELECT  * 

					FROM 	species_table

					WHERE 	genus = '.$this->sanitize($where['genus']).' AND
							species = '.$this->sanitize($where['species']).'
					';
					break;

				case 'species-by-id':
					$select =
					'
					SELECT  * 

					FROM 	species_table

					WHERE 	species_id = '.$this->sanitize($where).'
					';
					break;

				case 'all-genus':
					$select =
					'
					SELECT DISTINCT genus 

					FROM 			species_table
					';
					break;

				case 'version-history-desc':
					$select =
					'
					SELECT 	vht.version_comment,
							vht.version_num_a,
							vht.version_num_b,
							vht.version_num_c,
							vht.version_history_id,
							CONCAT("v",vht.version_num_a,"." ,vht.version_num_b,".",vht.version_num_c) AS version,
							CONCAT(ut.first_name," " ,ut.last_name) AS user_name,
							vht.time_stamp

					FROM 	version_history_table vht

					LEFT JOIN user_table ut
					ON 		ut.user_id = vht.user_id

					ORDER BY  vht.version_history_id DESC
					';		
					break;	
				case 'all-project-id-classes':
					$select =
					'
					SELECT 	CONCAT("project-", project_id) AS project_class,
							project_name

					FROM 	project_table
					';
					break;

				case 'user-login-summary':
					$select =
					'
					SELECT 	COUNT(
								CASE 
									WHEN 	status = "passed"
									THEN 	1
									ELSE 	NULL
								END
							) AS "passed",
							COUNT(
								CASE 
									WHEN 	status = "failed"
									THEN 	1
									ELSE 	NULL
								END
							) AS "failed",
							email_address

					FROM 	login_table

					'.$this->sanitize($where).'

					GROUP BY 	email_address

					ORDER BY 	email_address
							
					';			
					break;

				case 'all-users':
					$select = 
					'
					SELECT  	ut.user_id,
								ut.first_name,
								ut.last_name,
								ut.email_address,
								ut.password_need_reset,
								CONCAT(ut.first_name," " ,ut.last_name) AS user_name

					FROM 	user_table ut

					ORDER BY 	ut.last_name ASC					
					';					
					break;

				case 'bed-trace-info':
					$select =
					'
					SELECT 	btt.*,
							qst.st_type 

					FROM 	bed_trace_table btt

					LEFT JOIN qc_sample_table qst 
					ON 		btt.specimen_id = qst.sample_name 

					ORDER BY 	qst.st_type,
							btt.specimen_id
					';			
					break;

				case 'all-bed-trace-types':
					$select =
					'
					SELECT DISTINCT 	type

					FROM 			bed_trace_table
					';
					break;

				case 'all-bed-trace-st':
					$select = 
					'
					SELECT DISTINCT qst.st_type 

					FROM 	bed_trace_table btt

					LEFT JOIN qc_sample_table qst 
					ON 		btt.specimen_id = qst.sample_name 

					ORDER BY 	qst.st_type
					';
					break;

				case 'all-bed-trace-samples':
					$select =
					'
					SELECT DISTINCT	btt.specimen_id,
									qst.st_type 

					FROM 	bed_trace_table btt

					LEFT JOIN qc_sample_table qst 
					ON 		btt.specimen_id = qst.sample_name 

					ORDER BY 	qst.st_type,
							btt.specimen_id	
					';
					break;

				case 'get-table-col-names':
					$select =
					'
					SELECT DISTINCT 	COLUMN_NAME 

					FROM 			information_schema.columns 

					WHERE 			table_name = "'.$this->sanitize($where).'"
					';
					
					break;

				case 'check-table-exists':
					$select =
					'
					SELECT 	1

					FROM 	'.$this->sanitize($where).'

					LIMIT 	1
					';			
					break;
				case 'qc-sample':
					$select =
					'
					SELECT 	*

					FROM 	qc_sample_table

					WHERE 	sample_name = "'.$this->sanitize($where).'"
					';
					break;

				case 'passed-qc':
					$select =
					'
					SELECT 	*

					FROM 	qc_sample_table qst 

					WHERE 	qst.N50 >= 50000
					';
					break;

				case 'failed-qc':
					$select =
					'
					SELECT 	*

					FROM 	qc_sample_table qst 

					WHERE 	qst.N50 < 50000
					';
					break;

				case 'projects':
					$select =
					'
					SELECT 	pt.project_id,
							pt.project_name,
							pt.actively_recruiting,
							st.species,
							st.genus,
							pt.description,
							CONCAT(ut.first_name," " ,ut.last_name) AS user_name,
							CONCAT(ut_pi.first_name," " ,ut_pi.last_name) AS primary_investigator,
							sppc_vw.project_sample_count,
							GROUP_CONCAT(CONCAT(ut_mem.first_name, " ", ut_mem.last_name) SEPARATOR ", ") AS project_members,
							COUNT(papt.paper_id) AS num_papers


					FROM 		project_table pt 

					LEFT JOIN 	sample_per_project_count_vw sppc_vw
					ON 			sppc_vw.project_id = pt.project_id

					LEFT JOIN 	user_table ut 
					ON 			ut.user_id = pt.user_id

					LEFT JOIN 	species_table st 
					ON 			st.species_id = pt.species_id

					LEFT JOIN 	user_table ut_pi 
					ON 			ut_pi.user_id = pt.primary_investigator_id

					LEFT JOIN 	project_members_table pmt 
					ON 			pt.project_id = pmt.project_id

					LEFT JOIN 	user_table ut_mem 
					ON 			ut_mem.user_id = pmt.memeber_user_id

					LEFT JOIN 	paper_table papt
					ON 			papt.project_id = pt.project_id

					GROUP BY 	pmt.project_id,
								pt.project_id,
								papt.project_id
					';
					break;

				case 'previous-names-by-seq-info-id':
					$select =
					'
					SELECT 	pnt.*,
							CONCAT(ut.first_name," " ,ut.last_name) AS user_name

					FROM 	sequencing_info_table sit 

					LEFT JOIN 	previous_names_table pnt
					ON 			pnt.sequencing_info_id = sit.sequencing_info_id

					LEFT JOIN 	user_table ut
					ON 			ut.user_id = pnt.user_id

					WHERE 		sit.sequencing_info_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'all-sample-names-from-sequencing-info':
					$select =
					'
					SELECT 	*

					FROM 	sequencing_info_table
					';
					break;

				case 'extra-fields-by-sample-id':
					$select =
					'
					SELECT 	eft.*,
							CONCAT(ut.first_name," " ,ut.last_name) AS user_name,
							CONCAT("<b>Field Name:</b> ", eft.column_name, " <b>Field Value:</b> ",eft.text_value, " <b>Field Type:</b> ", eft.data_type) AS extra_field_summary

					FROM 	extra_fields_table eft

					LEFT JOIN 	user_table ut
					ON 			ut.user_id = eft.user_id

					WHERE 		eft.sample_id = "'.$this->sanitize($where).'"
					';					
					break;					

				case 'all-extra-field-names':
					$select =
					'
					SELECT DISTINCT eft.column_name

					FROM 	extra_fields_table eft

					ORDER BY eft.column_name
					';
					break;

				case 'previous-linked-samples-by-seq-info-id':
					$select = 
					'
					SELECT 	lst.*,
							CONCAT(ut.first_name," " ,ut.last_name) AS user_name,
							CONCAT(sit.ngs_sample_name, " Reason: ", lst.link_reason) AS link_summary

					FROM 	linked_samples_table lst

					LEFT JOIN 	sequencing_info_table sit 
					ON 			sit.sequencing_info_id = lst.sequencing_info_id_2

					LEFT JOIN 	user_table ut
					ON 			ut.user_id = lst.user_id

					WHERE 	lst.sequencing_info_id_1 = "'.$this->sanitize($where).'"

					UNION 

					SELECT 	lst.*,
							CONCAT(ut.first_name," " ,ut.last_name) AS user_name,
							CONCAT(sit.ngs_sample_name, " Reason: ", lst.link_reason) AS link_summary

					FROM 	linked_samples_table lst

					LEFT JOIN 	sequencing_info_table sit 
					ON 			sit.sequencing_info_id = lst.sequencing_info_id_1

					LEFT JOIN 	user_table ut
					ON 			ut.user_id = lst.user_id

					WHERE 	lst.sequencing_info_id_2 = "'.$this->sanitize($where).'"
					';
					break;

				case 'query-all-row-results-by-id':
					$select = 
					'
					SELECT 		* 

					FROM 		'.$this->sanitize($where['table_name']).' 

					WHERE 		'.$this->sanitize($where['id_name']).' = "'.$this->sanitize($where['id_val']).'"

					';
					
					break;

				case 'max-counts-by-sample-id':
					$select = 
					'
					SELECT 		MAX(sit.sample_num_count) AS max_sample_num_count,
								MAX(sit.repeat_num) AS max_repeat_num,
								MAX(sit.short_read_num) AS max_short_read_num,
								MAX(sit.long_read_num) AS max_long_read_num

					FROM 		sample_name_table snt

					LEFT JOIN 	sequencing_info_table sit 
					ON 			snt.sample_name_id = sit.sample_name_id

					WHERE 		sample_id = "'.$this->sanitize($where).'"

					GROUP BY 	sit.sample_name_id

					';
					break;

				case 'project-ids-by-sample-id':
					$select = 
					'
					SELECT 		pt.project_id,
								psxt.project_sample_xref_id

					FROM 		project_sample_xref_table psxt 

					LEFT JOIN 	project_table pt
					ON 			psxt.project_id = pt.project_id

					WHERE 		psxt.sample_id = "'.$this->sanitize($where).'"
					';				
					break;

				case 'project-names-by-sample-id':
					$select = 
					'
					SELECT 		GROUP_CONCAT(pt.project_name SEPARATOR ", ") AS project_names

					FROM 		project_sample_xref_table psxt 

					LEFT JOIN 	project_table pt
					ON 			psxt.project_id = pt.project_id

					WHERE 		psxt.sample_id = "'.$this->sanitize($where).'"

					GROUP BY 	psxt.sample_id
					';				
					break;

				case 'seq-info-by-ngs-sample-name':
					$select = 
					'
					SELECT 	*

					FROM 	sequencing_info_table 

					WHERE 	ngs_sample_name = "'.$this->sanitize($where).'"
					';
					break;

				case 'seq-info-samples':
					$select = 
					'
					SELECT 		sit.ngs_sample_name,
								sit.read_type,
								sit.sequencing_status,
								sit.include_in_project,
								sit.resequenced,
								sit.barcode,
								sit.include_in_project,
								GROUP_CONCAT(pt.project_name SEPARATOR ", ") AS project_names,
								CONCAT(ss_vw.genus, " ",ss_vw.species) AS genus_species,
								snt.sample_name_id,
								sit.sequencing_info_id,
								st.sample_id,
								snt.urmc_num_id,
								snt.primary_name,
								SUBSTRING_INDEX(SUBSTRING_INDEX(snt.primary_name, "_", 2), "_", -1) AS split_urmc_num_id

					FROM 		sequencing_info_table sit 

					LEFT JOIN 	sample_name_table snt
					ON 			snt.sample_name_id = sit.sample_name_id

					LEFT JOIN 	sample_table st 
					ON 			st.sample_id = snt.sample_id

					LEFT JOIN 	project_sample_xref_table psxt
					ON 			st.sample_id = psxt.sample_id 

					LEFT JOIN 	project_table pt 
					ON 			pt.project_id = psxt.project_id

					LEFT JOIN 	sample_species_vw ss_vw 
					ON 			st.sample_id = ss_vw.sample_id

					GROUP BY 	st.sample_id,
								sit.ngs_sample_name,
								sit.read_type,
								sit.sequencing_status,
								sit.resequenced,
								sit.barcode

					ORDER BY 	pt.project_name ASC,
								sit.sequencing_info_id ASC
					';
				
					break;

				case 'pending-seq-info-samples':
					$select = 
					'
					SELECT 		sit.ngs_sample_name,
								sit.read_type,
								sit.sequencing_status,
								sit.include_in_project,
								sit.resequenced,
								sit.barcode,
								sit.include_in_project,
								sit.time_stamp,
								GROUP_CONCAT(pt.project_name SEPARATOR ", ") AS project_names,
								CONCAT(ss_vw.genus, " ",ss_vw.species) AS genus_species,
								snt.sample_name_id,
								sit.sequencing_info_id,
								st.sample_id,
								snt.urmc_num_id,
								snt.primary_name,
								SUBSTRING_INDEX(SUBSTRING_INDEX(snt.primary_name, "_", 2), "_", -1) AS split_urmc_num_id

					FROM 		sequencing_info_table sit 

					LEFT JOIN 	sample_name_table snt
					ON 			snt.sample_name_id = sit.sample_name_id

					LEFT JOIN 	sample_table st 
					ON 			st.sample_id = snt.sample_id

					LEFT JOIN 	project_sample_xref_table psxt
					ON 			st.sample_id = psxt.sample_id 

					LEFT JOIN 	project_table pt 
					ON 			pt.project_id = psxt.project_id

					LEFT JOIN 	sample_species_vw ss_vw 
					ON 			st.sample_id = ss_vw.sample_id

					WHERE 		sit.sequencing_status = "pending"

					GROUP BY 	st.sample_id,
								sit.ngs_sample_name,
								sit.read_type,
								sit.sequencing_status,
								sit.resequenced,
								sit.barcode

					ORDER BY 	sit.time_stamp DESC,
								sit.ngs_sample_name ASC
					';			
					break;

				case 'sample-name-and-species':
					$select =
					'
					SELECT 		st.*,
								CONCAT(spt.genus,"_" ,spt.species) AS genus_species,
								spt.abbrevation AS abbrev_species	


					FROM 		sample_table st

					LEFT JOIN 	species_table spt 
					ON 			st.species_id = spt.species_id

					WHERE 		st.sample_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'project-members':
					$select = 
					'
					SELECT 	*

					FROM 	project_members_table pmt 

					WHERE 	pmt.project_id = "'.$this->sanitize($where).'"
					';
					break;

				case 'projects-by-id':
					$select =
					'
					SELECT 	pt.project_id,
							pt.project_name,
							pt.actively_recruiting,
							pt.description,
							st.species,
							st.genus,
							st.species_id,
							st.pathogen_type,
							st.subspecies,
							st.abbrevation,
							st.abbrevation AS abbrev_species,
							CONCAT(st.genus,"_" ,st.species) AS genus_species,
							CONCAT(ut.first_name," " ,ut.last_name) AS user_name,
							CONCAT(ut_pi.first_name," " ,ut_pi.last_name) AS primary_investigator,
							sppc_vw.project_sample_count,
							ut_pi.user_id AS primary_investigator_id,
							GROUP_CONCAT(CONCAT(ut_mem.first_name, " ", ut_mem.last_name) SEPARATOR ", ") AS project_members

					FROM 	project_table pt 

					LEFT JOIN 	sample_per_project_count_vw sppc_vw
					ON 			sppc_vw.project_id = pt.project_id

					LEFT JOIN 	user_table ut 
					ON 			ut.user_id = pt.user_id

					LEFT JOIN 	species_table st 
					ON 			st.species_id = pt.species_id

					LEFT JOIN 	user_table ut_pi 
					ON 			ut_pi.user_id = pt.primary_investigator_id


					LEFT JOIN 	project_members_table pmt 
					ON 			pt.project_id = pmt.project_id

					LEFT JOIN 	user_table ut_mem 
					ON 			ut_mem.user_id = pmt.memeber_user_id

					WHERE 		pt.project_id = "'.$this->sanitize($where).'"

					
					GROUP BY 	pmt.project_id,
								pt.project_id
					';
					break;

				case 'all-sample-groups':
					$select = 
					'
					SELECT 	sgt.*,
							CONCAT(ut.first_name," " ,ut.last_name) AS user_name

					FROM 	sample_group_table sgt 

					LEFT JOIN user_table ut 
					ON 		ut.user_id = sgt.user_id
					';
					break;

				case 'papers-by-id':
					$select =
					'
					SELECT 	papt.*,
							CONCAT(ut.first_name," " ,ut.last_name) AS user_name

					FROM 	project_table pt 

					LEFT JOIN paper_table papt
					ON 		papt.project_id = pt.project_id

					LEFT JOIN user_table ut 
					ON 		ut.user_id = papt.user_id

					WHERE 	pt.project_id = "'.$this->sanitize($where).'"
					';				
					break;				

				case 'project-array':
					$select =
					'
					SELECT 	pt.project_id,
							pt.project_name,
							pt.actively_recruiting,
							pt.description,
							CONCAT(ut.first_name," " ,ut.last_name) AS user_name,
							CONCAT(ut_pi.first_name," " ,ut_pi.last_name) AS primary_investigator,
							ut_pi.user_id AS primary_investigator_id

					FROM 	project_table pt 

					LEFT JOIN user_table ut 
					ON 		ut.user_id = pt.user_id

					LEFT JOIN user_table ut_pi 
					ON 		ut_pi.user_id = pt.primary_investigator_id

					WHERE 	pt.project_id = "'.$this->sanitize($where).'"
					';					
					break;

				case 'user-by-email':
					$select =
					'
					SELECT 	* 

					FROM 	user_table 

					WHERE  	email_address = "'.$this->sanitize($where['email_address']).'"
					';
					break;

				case 'login':
					$select = 
					'
					SELECT 	*

					FROM 	user_table

					WHERE 	email_address = "'.$this->sanitize($where['email_address']).'"
					AND 		password = "'.$this->sanitize($where['password']).'"
					';				
					break;

				case 'user-num-failed-attempts':
					$select =
					'
					SELECT 		COUNT(lt.login_id) as login_count

					FROM 		login_table lt

					WHERE 		lt.email_address = "'.$this->sanitize($where).'" AND
								lt.status = "failed" AND 
								lt.time_stamp > (now() - interval 30 minute)
					';
					
					break;

				case 'user-exist':
					$select = 
					'
					SELECT 			*

					FROM 			user_table ut

					WHERE 			ut.email_address = "'.$this->sanitize($where['email_address']).'" AND 
									ut.password = "'.$this->sanitize($where['password']).'"
					';
					break;

				case 'user-permissions':
					$select = 
					'
					SELECT 		GROUP_CONCAT(pt.permission SEPARATOR ", ") AS permissions

					FROM 		user_table ut

					LEFT JOIN 	permission_user_xref pux
					ON 			pux.user_id = ut.user_id

					LEFT JOIN 	permission_table pt
					ON 			pux.permission_id = pt.permission_id

					WHERE 		ut.user_id = "'.$this->sanitize($where).'"

					GROUP BY 		ut.user_id
					';
					
					break;
				
				case 'reporter-permission-list':
					$select = 
					'
					SELECT 	permission

					FROM 	permission_table

					ORDER BY 	permission
					';
					break;
				case 'failed-logins-last-30-mins':
					$select =
					'
					SELECT 		COUNT(lt.login_id) as login_count

					FROM 		login_table lt

					WHERE 		lt.status = "failed" AND 
								lt.time_stamp > (now() - interval 30 minute)
					';
					
					break;
					
			}

			if(isset($select))
			{
				$results = array();

				// perform query
				$searchResult = mysqli_query($this->conn, $select);

				if(!$searchResult)
				{
					return false;
				}

				// make results array
				else
				{

					while($row = mysqli_fetch_array($searchResult, $result=MYSQLI_ASSOC))
					{

						$results[] = $row;
					}

					return $results;
				}
			}
		}

		public function logoff()
		{
			session_start();
      		session_destroy();
      		mysqli_close($this->conn);
			header('Location:'.REDIRECT_URL.'?page=login');
		}

		public function close_db_conn()
		{
			mysqli_close($this->conn);
		}

		public function logLogin($user_info)
		{

			$this->addOrModifyRecord('login_table', $user_info);
		}

		public function unlockAccount($email_address, $random_password)
		{
			// update all failed logins for user in last 30 mins
			$update_query = 'UPDATE login_table lt SET lt.status = "update_fail" WHERE lt.email_address = "'.$this->sanitize($email_address).'" AND lt.status = "failed" AND lt.time_stamp > (now() - interval 30 minute)';
			
			$result = mysqli_query($this->conn, $update_query) or die('Oops!! Died '.$update_query);

			// get the Id of the inserted record
			$lastId = mysqli_insert_id($this->conn);

			// update password to random 10 chars 
			$this->updateRecord('user_table', 'email_address', $email_address, 'password', md5($random_password));

			// change password need reset to 1
			$this->updateRecord('user_table', 'email_address', $email_address, 'password_need_reset', 1);
		}
		public function lockAccount($email_address)
		{
			// change password need reset to 1
			$this->updateRecord('user_table', 'email_address', $email_address, 'password_need_reset', 2);
		}

		public function login($inputs)
		{
			// login to app.  Make sure that the password does not need to be reset
			// and that there are not more than 5 attempts in the last 30 mins
			// user_table (password_need_reset) controls locking the user account 
			// and reseting the password.
			// password_need_reset == 0 -> (allow login)
			// password_need_reset == 1 -> (reset password)
			// password_need_reset == 2 -> (account locked)

			$login_array = array(
					'email_address' 	=> 		$inputs['email_address'],
					'password'			=> 		md5($inputs['password']),
					'ip_loc'			=> 		null,
					'ip_address'		=> 		$this->ip,
					'ip_x_forwarded_for' =>		$this->ip_x_forwarded_for
				);

			// Find all user information.  An empty array will equal user and 
			// email does not exist.  This is used to increase the failed
			// attempts for the user.
			$user_info = $this->listAll('user-by-email', $login_array);
			
			// Find if email address matches md5 password.  Used to login.
			$login_info = $this->listAll('login', $login_array);

			//////////////////////////////////////////////////////////////////
			// If more than 5 attempts exist in the last 30 mins.  Redirect to 
			// to_many_login_attempts 
			//////////////////////////////////////////////////////////////////
			$user_login_attempts = $this->listAll('user-num-failed-attempts', $inputs['email_address']);

			// remove password from login_array.  It is not needed anymore.
			unset($login_array['password']);

			//////////////////////////////////////////////////////////////////
			// If email address does not exist a login attempt can not be 
			// logged however, still need to show failed login.
			//////////////////////////////////////////////////////////////////
			if (!isset($user_info) || empty($user_info))
			{			
				return False;
			}

			//////////////////////////////////////////////////////////////////
			// If password_need_reset is set to 2 this account is locked.  
			// Only users with admin rights can unlock accounts. 
			//////////////////////////////////////////////////////////////////
			else if ($user_info[0]['password_need_reset'] == 2)
			{
				header('Location:'.REDIRECT_URL.'/templates/account_locked.php');
			}

			//////////////////////////////////////////////////////////////////
			// If > 5 attempts exist in the last 30 mins do not allow in
			//////////////////////////////////////////////////////////////////
			else if (!empty($user_login_attempts) && $user_login_attempts[0]['login_count'] > 5)
			{

				$login_array['status'] = 'failed';
				$this->logLogin($login_array);
				header('Location:'.REDIRECT_URL.'/templates/to_many_login_attempts.php');
			}

			//////////////////////////////////////////////////////////////////
			// If password_need_reset is set to 1 redirect to password_reset
			//////////////////////////////////////////////////////////////////
			else if ($user_info[0]['password_need_reset'] == 1)
			{
				header('Location:'.REDIRECT_URL.'?page=password_reset');
			}

			//////////////////////////////////////////////////////////////////
			// If no user found.  message user of incorrect login and add failed
			// login to 
			//////////////////////////////////////////////////////////////////
			else if (empty($login_info))
			{
				$login_array['status'] = 'failed';
				$this->logLogin($login_array);

				return False;
			}			
			//////////////////////////////////////////////////////////////////
			// login and set session
			//////////////////////////////////////////////////////////////////
			else
			{
				$login_array['status'] = 'passed';
				$this->logLogin($login_array);

				// add a session
				$session_set = $this->setLoginSession($user_info[0]);			

				return $session_set;			
			}
		}
		public function setLoginSession($user_info)
		{
			session_start();
			$user_info['site_title'] = $this->site_title;
			$user_info['root_url'] = $this->root_url;
			$user_info['last_active_time'] = time();
			$_SESSION['user'] = $user_info;
			
			define('USER_ID', $_SESSION['user']['user_id']);

			session_write_close();

		     return true;
		}

		public function findDuplicate()
		{
			$searchQuery = 'SELECT '.$this->colName.' FROM '.$this->tableName.' WHERE '.$this->colName.' = "'.$this->postInput[$this->colName].'";';

			$searchResult = mysqli_num_rows(mysqli_query($this->conn, $searchQuery));
			return $searchResult;
		}

		private function sanitize($dirty_string)
		{
			$no_ticks_string = str_replace("`","'",$dirty_string);
			$clean_string = mysqli_real_escape_string($this->conn, $no_ticks_string);

			return $clean_string;
		}


		function mysqli_field_name($result, $field_offset)
		{
		    $properties = mysqli_fetch_field_direct($result, $field_offset);
		    return is_object($properties) ? $properties->name : null;
		}



	}

?>
