<?php

	class Utils
	{

		public function validateGetInt($get_name)
		{
			// Make sure a Get parameter is not malicious.
			// Make sure isset, not empty, and an integer
			// (str) -> bool


			if (isset($_GET[$get_name]) && !empty($_GET[$get_name]) && is_numeric($_GET[$get_name]))
			{
				return True;
			}
			else
			{
				return False;
			}
		}

		public function redirectUrl($get, $default_page)
		{
			// if redirect is set redirect to that page after submit

			if (isset($get['redirect']))
			{
				$url = '?page='.$get['redirect'];

				if (isset($get['redirect_id']))
				{
					$url.= '#'.$get['redirect_id'];
				}
				header('Location:'.REDIRECT_URL.$url);
			}
			else
			{
				header('Location:'.REDIRECT_URL.'?page='.$default_page);
			}
		}

		public function validateGetStr()
		{
			// Make sure a Get parameter is not malicious.
			// Make sure isset, not empty, does not contain a blackListFileType
			// does not contain a blackListCharacters
			// (str) -> bool
		}

		public function validatePageGet()
		{
			// Make sure a page is a validateGetStr and is an available page
		}

		public function validateDateGet()
		{
			// Make sure a Get parameter is not malicious.
			// Make sure isset, not empty, and is a date
			// (str) -> bool
		}

		public function binaryPresent($check_str)
		{
			// check if string contains non-printable characters which is
			// most likely binary
			// (str) -> bool

			// A hacky solution would be to search for NUL \0 chars.
			if (strpos($check_str, '\0') !== False)
			{
				return True;
			}
			
			else
			{
				return False;
			}

		}

		public function BlackListCharacters($check_str)
		{
			// This function will make sure the check_str does not contain any
			// characters which are blacklisted.  Black listed characters
			// could be added because they are malicious. 
			// All the control characters and Unicode ones should be removed from the filenames and their extensions without any exception. Also, the special characters such as ";", ":", ">", "<", "/" ,"\", additional ".", "*", "%", "$", and so on should be discarded as well. 
			// (':abc') -> False
			// ('abc>') -> False
			// ('..') -> False
			// (file.asax:.jpg) -> False

			$forbidden_chars = array(':', ';', '>', '<', '$', '..', '*', '?', '|', '"', "'", '%', '`');
		}

		function uploadMultipleFiles($ref_id, $ref_table, $file_array, $save_folder_name, $md5_hash_post_array)
		{
			// Upload multiple files to SERVER_SAVE_LOC.  Checking that they 
			// transfered completely.
			$return_array = array();

			foreach ($file_array['name'] as $key => $filename)
			{
				//////////////////////////////////////////////////////
				// Move file to server in a folder associated with save_folder_name and ROOT_URL
				//////////////////////////////////////////////////////
				$save_folder = SERVER_SAVE_LOC.$save_folder_name.'/'.ROOT_URL;

				if (!file_exists($save_folder))
				{
					mkdir($save_folder);
				}

				$save_folder.= '/'.$ref_id;
				
				if (!file_exists($save_folder))
				{
					mkdir($save_folder);
				}

				// Move file to server
				$destination = $save_folder.'/'.$filename;
				$temp_file = $file_array['tmp_name'][$key];

				// make sure not to replace a file.
				if (file_exists($destination))
				{
					if (!isset($message))
					{
						$return_array[$key] = array('message' => 'The following file(s) are already uploaded.','file_name' => $filename);						
					}
				}

				else if (move_uploaded_file($temp_file, $destination))
				{
					//////////////////////////////////////////////////////
					// Check if file transfered correctly
					//////////////////////////////////////////////////////

					// get md5 hash of file moved to server
					$server_md5 = md5_file($destination);

					// Check md5 hash against hashes made in javascript 
					if ($server_md5 === $md5_hash_post_array[$filename])
					{
						///////////////////////////////////////////////////
						// Return document to either add info to  instrument_validation_doc_table or doc_table  
						///////////////////////////////////////////////////
						$return_array[$key] = array(
							'ref_id' 				=> 	$ref_id,
							'user_id'				=> 	USER_ID,
							'file_loc'			=> 	$destination,
							'name_uploaded_as'		=> 	$filename,
							'md5_hash'			=> 	$server_md5,
							'file_size'			=> 	$file_array['size'][$key]
						);
					}
					else
					{
						$return_array[$key] =  array(
								'admin_message' =>	 __FILE__.' Error uploading file!! MD5 hashes do not match!! Line #: '.__LINE__.' $server_md5: '.$server_md5.' $md5_hash_post_array[$filename]: '.$md5_hash_post_array[$filename],
								'message'		=> 	'File upload problem: '.$filename
						);
					}					
				}
				else
				{
					$return_array[$key] = array(
						'admin_message' => __FILE__.' Error uploading validation file!! Line #: '.__LINE__.' $destination: '.$destination.' $temp_file: '.$temp_file,
						'message'		=> 	'File upload problem: '.$filename

					);
				}
			}
			
			return $return_array;

		}

		public function BlackListFileTypes($file_array)
		{
			// Check if file type is a type which is black listed from upload
			// These file types can be used for attacks on the server or program
			// File types and why?
				// Upload .jsp file into web tree - jsp code executed as the web user
				// Upload .gif file to be resized - image library flaw exploited
				// Upload huge files - file space denial of service
				// Upload file using malicious path or name - overwrite a critical file
				// Upload file containing personal data - other users access it
				// Upload file containing "tags" - tags get executed as part of being "included" in a web page
				// Upload .rar file to be scanned by antivirus - command executed on a server running the vulnerable antivirus software
				// Upload .exe file into web tree - victims download trojaned executable
				// Upload .html file containing script - victim experiences Cross-site Scripting (XSS)
				// Upload .jpg file containing a Flash object - victim experiences Cross-site Content Hijacking
				// Finding missed extensions that can be executed on the server side or can be dangerous on the client side (e.g. ".php5", ".pht", ".phtml", ".shtml", ".asa", ".cer", ".asax", ".swf", or ".xap").
				// The list of permitted extensions should be reviewed as it can contain malicious extensions as well. For instance, in case of having ".shtml" in the list, the application can be vulnerable to SSI attacks.
			// refs
				// https://www.owasp.org/index.php/Unrestricted_File_Upload
				// https://cheatsheetseries.owasp.org/
			$blocked_file_types = array('jsp', 'gif', 'rar', 'exe', 'dmg', 'html', 'jpg', 'pht', 'phtml', 'shtml', 'asa', 'cer', 'asax', 'swf', 'xap', 'php', 'xml', 'asp', 'php3', 'php5', 'php7');

			foreach ($file_array['name'] as $key => $filename)
			{
				/////////////////////////////////////////////////////////////////
				// Make sure files do not have two extensions (ex. image.jpg.php, .., ...php)
				/////////////////////////////////////////////////////////////////
				if (substr_count($filename, '.') > 1)
				{
					return False;
				}

				/////////////////////////////////////////////////////////////////
				// Make sure file type not a blocked type
				// blocked examples (file.aSp, file.asp, test.php)
				/////////////////////////////////////////////////////////////////
				$ext = strtolower(pathinfo($filename, PATHINFO_EXTENSION));
					
				if (in_array($ext,$blocked_file_types))
				{
					return False;								
				}
			}

			return True;
		}

		public function allowedFileTypes($file_array, $allowed_types_array)
		{
			foreach ($file_array['name'] as $key => $filename)
			{
				$ext = strtolower(pathinfo($filename, PATHINFO_EXTENSION));
					
				if (in_array($ext,$allowed_types_array))
				{
					return True;								
				}
			}

			return False;
		}

		public function CheckSecurityOfFile($file_array, $allowed_types_array='skip')
		{
			// check all files in file array for a malicious file upload
			// phpMussel is automatically activated to check all uploads 
			// and performs a virus scan.
			// (array) -> bool
			// in_array
				// allowed_types_array
			// refs
				// https://www.computerweekly.com/answer/File-upload-security-best-practices-Block-a-malicious-file-upload

			/////////////////////////////////////////////////////////////////
			// check if file is part of the Black list file types
			/////////////////////////////////////////////////////////////////
			if (!$this->BlackListFileTypes($file_array))
			{			
				return False;
			}
			
			///////////////////////////////////////////////////////////////
			// if allowed_types_array is present check that file type is allowed
			// if it isn't present use default allowed_types_array
			///////////////////////////////////////////////////////////////
			if ($allowed_types_array !== 'skip' && !$this->allowedFileTypes($file_array,$allowed_types_array))
			{
				return False;
			}	

			return True;		
		}

		public function textareaEmpty($textArea)
		{
			if (trim($textArea) == '' && strlen(trim($textArea)) == 0)
			{
				return True;
			}
			else
			{
				return False;
			}
		}

		public function GetValueForUpdateInput($in_array, $val_name, $default_val='')
		{
			// (array, str, str/int/date) -> str
			// when updating forms inputs previous values need to be entered into form.  
			// This function return the value for input

			if (isset($_POST[$val_name]) && !empty($_POST[$val_name]))
			{
				return $_POST[$val_name];
			}

			else if (isset($in_array[0][$val_name]))
			{
				return strval($in_array[0][$val_name]);
			}
			else
			{
				return $default_val;
			}
		}

		public function techCheckbox($ckbx_name, $input_array, $settings_array=False)
		{
			// (str, array) -> check box html
			// if settings_array is not supplied a general required check box will be returned
			// $settings_array example
			// {
			// 	'required-linked-fields-status' => True,
			//   'required_linked_field'  => extraction_aliquoted_date,
			//	'required' => False
			// }

			if (!$settings_array)
			{
				$settings_array = array(
					'required-linked-field-status' => False,
			  		'required_linked_field'  => False,
					'required' 			=> True,
					'column-count'			=> 1
				);
			}

			$ckbx_html = '';

			if (isset($settings_array['column-count']) && $settings_array['column-count'] !== 1)
			{
				$ckbx_html.= '<ul style="column-count:'.$settings_array['column-count'].';column-gap:2rem;list-style:none;">';
			}
			
			foreach ($input_array as $key => $user)
			{		

				if (isset($settings_array['column-count']) && $settings_array['column-count'] !== 1)
				{
					$ckbx_html.= '<li>';
				}

				$ckbx_html.='<label class="checkbox">';
				$ckbx_html.='<input type="checkbox" ';

				if ($settings_array['required-linked-field-status'])
				{
					$ckbx_html.=' class="required-linked-fields" data-required_linked_field="'.$settings_array['required_linked_field'].'" ';
				}
				

				// if a settings_array was supplied add class to activate making a field required if this check box is selected.

				$ckbx_html.='value="'.$user['user_id'].'" name="'.$ckbx_name.'" ';

				if ($settings_array['required'])
				{
					$ckbx_html.='" required="required"';
				}

				$ckbx_html.='>';

				// highlight current users name
				if ($user['user_id'] === USER_ID)
				{

					$ckbx_html.='<span class="yellow_background">';
							
				}
	
				$ckbx_html.=$user['first_name'].' '.$user['last_name'].' ('.$user['initials'].')';
							
				if ($user['user_id'] === USER_ID)
				{
							
					$ckbx_html.='</span>';
							
				}
	
				$ckbx_html.='</label>';

				if (isset($settings_array['column-count']) && $settings_array['column-count'] !== 1)
				{
					$ckbx_html.= '</li>';
				}
			}

			if (isset($settings_array['column-count']) && $settings_array['column-count'] !== 1)
			{
				$ckbx_html.= '</ul>';
			}

			return $ckbx_html;			
		}

		public function instrumentInputName($instrument)
		{
			// array is obtained by iterating over an array produced from listAll 'instrument-groups-description'

			$logo = $this->ManufacturerLogo($instrument['manufacturer']);
			
			$input = '';

			// find if a manufacturerlogo exisits
			if ($logo)
			{
				$input.= '<img src="'.$logo.'" height="30px;">';	
			}					
			else
			{
				$input.= $instrument['manufacturer'];
			}
								
			$input.= ' '.$instrument['model']; 
								
			$icon = $this->InstrumentDescriptionIcon($instrument['description']);
			
			if ($icon)
			{
				$input.= ' '.$instrument['description'].' <img src="'.$icon.'" height="30px;">';
			}
			else
			{
				$input.= ' '.$instrument['description'];
			}
			
			$input.= ' <span class="yellow_background">Yellow Tag: '.$instrument['yellow_tag_num'].'</span>';

			if (!empty($instrument['serial_num']))
			{
				$input.= ' Serial#:'.$instrument['serial_num'];
			}

			if (!empty($instrument['clinical_engineering_num']))
			{
				$input.= ' Clin Eng#:'.$instrument['clinical_engineering_num'];
			}
			return $input;
		}

		public function ManufacturerLogo($manufacturer)
		{
			switch ($manufacturer) 
			{
				case 'Applied Biosystems':
					return 'images/ABI_logo.png';
					break;

				case 'Eppendorf':
					return 'images/logoEppendorf.png';
					break;
				
				case 'Roche':
					return 'images/roche_icon.png';
					break;

				case 'Beckman':
					return 'images/beckman.jpg';
					break;

				default:
					return False;
					break;
			}
		}

		public function InstrumentDescriptionIcon($description)
		{
			switch ($description) 
			{
				case 'Mars':
					return 'images/mars-planet-png-8.png';
					break;
				case 'Venus':
					return 'images/venus.jpg';
					break;
				case 'Pluto':
					return 'images/pluto.jpg';
					break;			
				default:
					return False;
					break;
			}
		}

		public function GetValueForUpdateSelect($in_array, $val_name, $expected_val)
		{
			// (array, str, str/int/date) -> str
			// when updating forms selects previous values need to be selected in the form.  
			// This function return selected if value equals expected value equals 

			if (isset($_POST[$val_name]) && !empty($_POST[$val_name]) && $_POST[$val_name] == strval($expected_val))
			{
				return 'selected';
			}


			else if (isset($in_array[0][$val_name]) && strval($in_array[0][$val_name]) == strval($expected_val))
			{
				return 'selected';
			}
			else
			{
				return '';
			}
		}

		public function GetValueForUpdateRadioCBX($in_array, $val_name, $expected_val)
		{
			// (array, str, str/int/date) -> str
			// when updating forms radio buttons or cbx previous values need to be checked in the form.  
			// This function return checked if value equals expected value equals 
			// LIMIATATION ONLY WORKS IF ONE CBX IS CHECKED USE GetValueForUpdateCBX INSTEAD

			if (isset($_POST[$val_name]) && !empty($_POST[$val_name]) && $_POST[$val_name] == strval($expected_val))
			{
				return 'checked';
			}

			else if (isset($in_array[0][$val_name]) && strval($in_array[0][$val_name]) == strval($expected_val))
			{
				return 'checked';
			}
			else
			{
				return '';
			}
		}

		public function GetValueForUpdateCBX($in_array, $val_name, $expected_val)
		{
			// (array, str, str/int/date) -> str
			// when updating forms radio buttons or cbx previous values need to be checked in the form.  
			// This function return checked if value equals expected value equals 
	
			if (isset($_POST[$val_name]) && $this->in_array_r($expected_val, $_POST[$val_name]))
			{
				return 'checked';
			}

			else if (isset($in_array) && $this->NestedArrayContainingKeyValPair($in_array, $val_name, $expected_val))
			{
				return 'checked';
			}
			else
			{
				return '';
			}
		}

		public function DefaultRadioBtn($in_array, $val_name, $expected_val)
		{
			// (array, str, str/int/date) -> str
			// Return checked for two reasons if expected_val is found or if the the val_name is not found or empty since this is the default radio button option

			if (isset($_POST[$val_name]) && !empty($_POST[$val_name]) && $_POST[$val_name] == strval($expected_val))
			{
				return 'checked';
			}
	
			// expected value found
			else if (isset($in_array[0][$val_name]) && strval($in_array[0][$val_name]) == strval($expected_val))
			{
				return 'checked';
			} 

			// No value found
			else if (!isset($in_array[0][$val_name]) || empty($in_array[0][$val_name]))
			{
				return 'checked';
			}

			// a Value besides expected value found
			else
			{
				return '';
			}
		}

		public function mean($input_array)
		{
			// $arr = array of all values to take mean
			$total = 0;
			
			foreach ($input_array as $value)
			{
				$total += $value;
			}
			
			return ($total / count($input_array));
		}

		public function standard_deviation($arr)
		{
			// $arr = array of all values to take standard Deviation over

			if (!count($arr))
			{
				return 0;
			}
			
			$mean = $this->mean($arr);
			$sos = 0; // Sum of squares
			
			for ($i = 0; $i < count($arr); $i++)
			{
				$sos += ($arr[$i] - $mean) * ($arr[$i] - $mean);
			}
			
			return sqrt($sos / (count($arr) - 1));
		}

		public function z_score($var, $arr)
		{
			// Z-score Number of standard deviations a given data point ($var) lies from the mean.  

			return ($var - $this->mean($arr)) / $this->standard_deviation($arr);
		}

		public function toggleMoreLess($cell_val, $col_name, $row_id, $show_len=11)
		{
			// Takes a string and strings larger than 20 length will be returned 
			// with a toggle teaser.  Needs to be activated in JavaScript.
			// Entire cell is returned. place in a row in a table

				$cell_val = str_replace('<br />', '<br>', $cell_val);

	          ////////////////////////////////////////////////////
	          // Larger strings need to be toggled so more information
	          // can fit in smaller places
	          ////////////////////////////////////////////////////
               if (strlen($cell_val) >$show_len)
               {
                                   
                    // get the first 20 char of string
                    $substr_cell_val = substr($cell_val, 0, $show_len);

                    $end_id = $col_name.'_'.$row_id;

                    $td = '<td>';
                    	$td.= '<span id="teaser_'.$end_id.'" class="teaser">'.$substr_cell_val.'</span>';
                    	$td.= '<span id="complete_'.$end_id.'" class="complete d-none">'.$cell_val.'</span>';
                         $td.= '<br><span id="show_complete_'.$end_id.'" class="show-complete">more ...</span>';    
                        	$td.= '<span id="show_teaser_'.$end_id.'" class="show-teaser d-none">less ...</span>'; 
                    $td.= '</td>';           

                    return $td;
               }

               ////////////////////////////////////////////////////
               // Smaller strings can just be added directly to the cell
               ////////////////////////////////////////////////////
               else
               {
               	return '<td>'.$cell_val.'</td>';
               }
		}

		public function toggleDropDownMenu($cell_val, $col_name, $row_id, $dropDownHTML)
		{
			// (str, str, int, html str) -> html str
			// This function will make the html to toggle a dropdown menu.
			// It will have to be activated in javascript.		
               $end_id = $col_name.'_'.$row_id;

               $td = '<td>';
               	$td.= '<span id="teaser_'.$end_id.'" class="teaser">'.$cell_val.'</span>';
               	$td.= '<span id="complete_'.$end_id.'" class="complete hide">'.$cell_val.'<br><span>'.$dropDownHTML.'</span></span>';
                    $td.= '<br><span id="show_complete_'.$end_id.'" class="show-complete"><span class="glyphicon glyphicon-plus"></span></span>';    
                   	$td.= '<span id="show_teaser_'.$end_id.'" class="show-teaser hide"><span class="glyphicon glyphicon-minus"></span></span>'; 
               $td.= '</td>';

               return $td;
		}

		public function readExcelRow($row)
		{
			// Rows from phpexcel seem to be split between two entries in a row
			// combine into one string and then split into array.
			// 1 => 
				//   array (size=2)
				//     'A' => string 'contig_id	contig_srt	contig_end	region_id	attributes	gc_count	overlaps	fwd_e2e	rev_e2e	total_reads	fwd_reads	rev_reads	cov20x	cov100x	cov500x' (length=141)
				//     'B' => null
			// 11 => 
				// array (size=2)
				//  'A' => string 'chr7	55227951	55228057	ON_EGFR_2A	GENE_ID=EGFR;PURPOSE=CNV' (length=58)
				//  'B' => string 'Hotspot;CNV_ID=EGFR;CNV_HS=1	41	937	277	319	937	402	535	107	107	107' (length=67)
			// $elems = array();

			$elems = array();
			foreach ($row as $key => $row_elem)
			{
				$curr_elems = explode("\t",$row_elem);

				if (!empty($row_elem))
				{
					$elems = array_merge($elems, $curr_elems);				
				}
			}

			return $elems;
		}

		public function generateCaptcha($captcha_file)
		{
			$captcha = $this->randomStr(8, False);
			session_start();
			$_SESSION['code'] = $captcha;
			session_write_close(); 

			$font_size = 60;
			$img_width = 500;
			$img_height = 100;

			$image = imagecreate($img_width, $img_height); // create background image with dimensions
			imagecolorallocate($image, 255, 255, 255); // set background color
			$text_color = imagecolorallocate($image, 255, 0, 0); // set captcha text color

			imagettftext($image, $font_size, 0, 30, 80, $text_color, 'fonts/V-de-vacia-deFharo.ttf', $captcha);
		
			imagejpeg($image, $captcha_file);
		}

		public function strandBiasCellClass($sb)
		{
			$sb = round($sb,4);
                    
               // strand bias color
               if ($sb == 0)
               {
                    return array(
                    	'sb_class'	=>	'alert-na-qc',
                    	'sb'			=>	'QC not done'
                    );
               }

               else if ($sb > 4.0)
               {
                    return array(
                    	'sb_class'	=>	'alert-flagged-qc',
                    	'sb'			=>	$sb
                    );
               }                        
               else
               {
                    return array(
                    	'sb_class'	=>	'alert-passed-qc',
                    	'sb'			=>	$sb,
                    );
               }
		}

		public function strandOddsRatio($allele_counts)
		{
			// https://gatkforums.broadinstitute.org/gatk/discussion/5533/strandoddsratio-computation
 
			// https://gatkforums.broadinstitute.org/gatk/discussion/9316/how-to-calculate-annotation-sor
			
			// https://software.broadinstitute.org/gatk/documentation/tooldocs/3.8-0/org_broadinstitute_gatk_tools_walkers_annotator_StrandOddsRatio.php
			// Pasted
			// The dev of strandOddsRatio recommends throwing out SNPs with SOR > 4.0 and indels with SOR > 10.0. These are meant to be conservative, in line with the rest of the recommendations. For more stringency, you'll want to lower the values.

			// calculate strandOddsRatio
			// a = ref_pos	+ 1
			// b = ref_neg + 1
			// c = variant_pos + 1
			// d = variant_neg + 1

			// R = (a * d ) / ( b * c )
			// sum_R = ( R + ( 1 / R ) )
			// ref_ratio = ( min { a , b } ) / (max { a, b })
			// alt_ratio = ( min { c , d } ) / (max { c , d })
			// scale_factor = ( ref_ratio / alt_ratio )
			// scaled_sor = ( sum_R * scale_factor )
			// LN_scaled = (ln(scaled_sor)) 

			if (isset($allele_counts['ref_pos']) && isset($allele_counts['ref_neg']) && isset($allele_counts['variant_neg']) && isset($allele_counts['variant_pos']) && !empty($allele_counts['ref_neg']) && !empty($allele_counts['ref_pos']) && !empty($allele_counts['variant_neg']) && !empty($allele_counts['variant_pos']))
			{
				$a = (int)$allele_counts['ref_pos'] + 1;	
				$b = (int)$allele_counts['ref_neg']  + 1;
				$c = (int)$allele_counts['variant_pos'] + 1;
				$d = (int)$allele_counts['variant_neg'] + 1;

				$R = ($a * $d ) / ( $b * $c );

				$sum_R = ( $R + ( 1 / $R ) );

				$ref_ratio = ( min ($a , $b) ) / (max($a, $b));
				$alt_ratio = ( min ($c , $d) ) / (max($c , $d));
				$scale_factor = ( $ref_ratio / $alt_ratio );
				$scaled_sor = ( $sum_R * $scale_factor );
				$LN_scaled = (log($scaled_sor));

				$allele_counts['p'] = $scaled_sor;
				$allele_counts['strand_bias'] = $LN_scaled;

				///////////////////////////////////////////////////////////
				// Add status of strand bias
				// greater than 4 flagged
				// need to add later indel cut off LN_scaled > 10.0 flagged
				///////////////////////////////////////////////////////////

				if ($LN_scaled > 4.0)
				{
					$allele_counts['status'] = 'flagged';
				}
				else
				{
					$allele_counts['status'] = 'passed';
				}
			}
			else
			{
				$allele_counts['p'] = '';
				$allele_counts['strand_bias'] = '';
				$allele_counts['status'] = 'na';
			}

			return $allele_counts;
		}	

		public function strandBias($allele_counts)
		{
			// calculate strandBias
			// a = ref_pos	
			// b = ref_neg
			// c = variant_neg
			// d = variant_pos

			if (isset($allele_counts['ref_pos']) && isset($allele_counts['ref_neg']) && isset($allele_counts['variant_neg']) && isset($allele_counts['variant_pos']) && !empty($allele_counts['ref_neg']) && !empty($allele_counts['ref_pos']) && !empty($allele_counts['variant_neg']) && !empty($allele_counts['variant_pos']))
			{
				$a = (int)$allele_counts['ref_pos'];	
				$b = (int)$allele_counts['ref_neg'];
				$c = (int)$allele_counts['variant_neg'];
				$d = (int)$allele_counts['variant_pos'];

				// ref_ratio = b/(a+b) 
				$ref_ratio = $b/($a+$b);
				
				// variant_ratio = d/(c+d)	
				$variant_ratio = $d/($c + $d);
				
				// abs_var = abs(ref_ratio - variant_ratio)	
				$abs_var = abs($ref_ratio - $variant_ratio);
			
				// $every_val_ratio = (b+d)/(a+b+c+d)	
				$every_val_ratio = ($b+$d) / ($a+$b+$c+$d);
							
				// test statistic P = (abs_var) / $every_val_ratio
				$p = $abs_var / $every_val_ratio;
				$allele_counts['p'] = $p;

				// strand bias = 10*log10(P)
				$strand_bias = 10 * log10($p);
				$allele_counts['strand_bias'] = $strand_bias;
			}
			else
			{
				$allele_counts['p'] = '';
				$allele_counts['strand_bias'] = '';

			}

			return $allele_counts;

		}

		public function addEditableTableCell($table_val, $col_name, $id, $id2_nam=False, $id2=False)
		{
			//

			$cell = '<div class="row_data" edit_type="click" col_name="';
			$cell.= $col_name.'" id="'.$col_name.'_'.$id.'"';

			if ($id2_nam != False && $id2 != False)
			{
				$cell.= $id2_nam.'="'.$id2.'">';
			}
			else
			{
				$cell.='>';
			}

			if (empty($table_val))
			{
				$cell.='__';
			}
			else
			{
				$cell.=$table_val;
			}
			
			$cell.='</div>';

			return $cell;
		}

		public function getVirtualImageSRC($img_file)
		{
			// This function will read an image file and produce a src to include in an src
			// <img src="">
			// This is useful for files outside of the web directory but also useful to hide
			// location of file. 
			// https://stackoverflow.com/questions/4286677/show-image-using-file-get-contents

			$img_data = base64_encode(file_get_contents($img_file));
			$src = 'data: '.mime_content_type($img_file).';base64,'.$img_data;
			return $src;
		}

		public function countInDir($dir)
		{
			// Count the number of files or directories in a folder

			// get everything inside the directory.  This will return . and ..
			// example of empty dir 
				// 0 => string '.' (length=1)
				// 1 => string '..' (length=2)
			$dir_contents = scandir($dir);
			
			return count($dir_contents)-2;
		}

		public function getAllFilesInDir($dir)
		{
			// Get only files in dir ignoring . and ..
			
			// get everything in a dir
			$dir_contents = scandir($dir);

			$result = array();

			foreach ($dir_contents as $key => $content)
			{
				// skip . .. files and skip directories
				if (!in_array($content, array('.', '..')) && !is_dir($dir.DIRECTORY_SEPARATOR.$content))
				{
					// add file name only.
					$result[$key] = $content;
				}
			}

			return $result;
		}

		public function randomStr($len_str, $withSpecialChrs=True)
		{
			if ($withSpecialChrs)
			{
				$possible_chrs = 'abcdefghijklmnopqrstuvwxyz'
			.'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.'0123456789!@#$%&*';
			}
			else
			{
				$possible_chrs = 'abdefghijkmnqrstxyz'.'ABDEFGHIJKLMNQRSTUVWXYZ'.'123456789!@#$%&*';
			}


			$seed = str_split(
			$possible_chrs); // and any other characters
			shuffle($seed); // probably optional since array_is randomized; this may be redundant
			$rand = '';
			foreach (array_rand($seed, $len_str) as $k) 
			{
				$rand .= $seed[$k];
			}
			return $rand;
		}

		public function possibleAlternateCoding($coding)
		{
			// Sometimes the input of the coding is not the same as cosmic
			// check if coding meets a regex which should be checked
			// For example:
				// c.1900_1922delAGAGAGGCGGCCACCACTGCCAT found in db is c.1900_1922del23
			
			////////////////////////////////////////////////////////////
			// find if variant includes a del followed by letters
			////////////////////////////////////////////////////////////
			$regex = '(del[ATCGNatcgn]{6,})';
			preg_match($regex, $coding, $dels);

			if (!empty($dels))
			{
				$num_nucleotides = strval(strlen($dels[0])-3);
				return str_replace($dels[0], 'del'.$num_nucleotides, $coding);
			}



			else
			{
				return False;
			}
		}

		public function varSetNotEmpty($var)
		{
			// check if a variable is set and it is not empty

			if (isset($var) && !empty($var))
			{				
				return true;
			}
			else
			{
				return false;
			}
		}

		public function arraysHaveSameNumNonEmptyEntries($arr_1, $arr_2)
		{		
			return $this->numNonEmptyEntriesInArray($arr_1) === $this->numNonEmptyEntriesInArray($arr_2);
		}

		public function numNonEmptyEntriesInArray($arr)
		{
			$count = 0;

			foreach ($arr as $key => $val)
			{
				if (!empty($val))
				{
					$count++;
				}
			}
			return $count;
		}

		public function nestedArrayNotEmpty($in_array)
		{
			// (arr) -> bool
			// example: returns False
			// 'linked_sample' => 
			//    array (size=2)
			//      0 => string '' (length=0)
			//      1 => string '' (length=0)

			foreach ($in_array as $key => $val)
			{
				if (!empty($val))
				{
					return true;
				}
			}

			return false;
		}

		public function array_combine($key_array, $value_array)
		{
			// combines two arrays making into key value pairs.
			// If key_array is longer than value array empty string 
			// is included as value.`
			// >>> (['A', 'B', 'C'], ['D', 'E']) -> 
			// { 	
			// 		'A' 	=> 	'D',
			//		'B' 	=> 	'E',
			// 		'C' 	=> 	''
			// }
			$combined = array();

			foreach ($key_array as $i => $key)
			{
				$combined[$key] = isset($value_array[$i]) ? $value_array[$i] :'';
			}

			return $combined;			
		}
		
		public function has_prefix($string, $prefix) 
		{	
			return ((substr($string, 0, strlen($prefix)) == $prefix) ? true : false);
		}
		public function convert_AA_to_long_hand($aa)
		{
			// purpose: Convert short amino acid nomenclature to long form
			// (p.F751fs*28) -> p.Phe751fsTer28

			$aa_table = array(
				'A' => 'Ala', 
				'C' => 'Cys', 
				'D' => 'Asp', 
				'E' => 'Glu', 
				'F' => 'Phe', 
				'G' => 'Gly', 
				'H' => 'His', 
				'I' => 'Ile', 
				'K' => 'Lys', 
				'L' => 'Leu', 
				'M' => 'Met', 
				'N' => 'Asn', 
				'P' => 'Pro', 
				'Q' => 'Gln', 
				'R' => 'Arg', 
				'S' => 'Ser', 
				'T' => 'Thr', 
				'V' => 'Val', 
				'W' => 'Trp',
				'Y' => 'Tyr', 
				'*' => 'Ter'
			);
			
			// iterate over the entire short hand AA
			for ($i=0; $i < strlen($aa); $i++)
			{
				// if the current position in short hand AA is in the $aa_table as a key
				// it needs to be updated to long hand
				if (array_key_exists($aa[$i], $aa_table))
				{
					$long_aa = $aa_table[$aa[$i]];
					$aa = substr_replace($aa, $long_aa, $i, 1);				
				}
			}
			return $aa;			
		}	

		public function convert_AA_to_short_hand($aa)
		{
			$aa_table = array(
				'Ala'=> 'A',
				'Cys'=> 'C',
				'Asp'=> 'D',
				'Glu'=> 'E',
				'Phe'=> 'F',
				'Gly'=> 'G',
				'His'=> 'H',
				'Ile'=> 'I',
				'Lys'=> 'K',
				'Leu'=> 'L',
				'Met'=> 'M',
				'Asn'=> 'N',
				'Pro'=> 'P',
				'Gln'=> 'Q',
				'Arg'=> 'R',
				'Ser'=> 'S',
				'Thr'=> 'T',
				'Val'=> 'V',
				'Trp'=> 'W',
				'Tyr'=> 'Y',
				'Ter'=> '*'
			);
			
			foreach ($aa_table as $key => $val)
			{
				if (strpos($aa, $key) !== false)
				{
					$aa = str_replace($key, $val, $aa);
				}
			}
			return $aa;
		}

		public function IsFileDelimiterSeperated($delimiter, $check_file, $file_type=NULL, $min_num_cols_expected=3)
		{
			// Purpose: check if file is separated by delimiter excluding any lines that start with #

			// read thru file and skip all lines that start with #
			$of = fopen($check_file, 'r') or die('unable to open file!');

			$checked_lines = 0;

			$num_delimiter_last_time = 0;

			while (!feof($of) && $checked_lines < 5)
			{
				// read 5 lines or until the end of the file after the # lines
				$line = fgets($of);

				if(strpos($line, '#') !== 0 && $line != '')
				{
					$curr_num_cols = substr_count($line, $delimiter);	

					$checked_lines .= 1;

					// Make sure $curr_num_cols != 0 and if $num_delimiter_last_time != 0 make sure they are the same number otherwise return False
					if ($curr_num_cols === 0 || $curr_num_cols < $min_num_cols_expected)
					{
						return False;
					}

					else if ($num_delimiter_last_time === 0)
					{
						$num_delimiter_last_time = $curr_num_cols;
					}
				}
			}
			
			return True;
		}

		public function CovFileTypeCorrect($check_file, $possible_genes, $sample_name)
		{
			// (depth_coverage file, array, name sample)
			// check that format of coverage file is correct
			// for myleoid panel
			$of = fopen($check_file, 'r') or die('unable to open file!');
			// get the header of the file
			$line = rtrim(fgets($of));
			$header = explode("\t", $line); 

			///////////////////////////////////////////////////////
			// Check that the header has at least 5 columns
			///////////////////////////////////////////////////////
			if (!sizeOf($header) >= 5)
			{
				return 'Header has too few columns';
			}

			///////////////////////////////////////////////////////
			// Check header contains all columns necessary
			///////////////////////////////////////////////////////
			$necessary_cols_bills = array('Gene', 'Amplicon', 'Exon', 'Coding Region', 'Depth', 'Sample');
			
			$bills_cols_present = $this->allValuesPresentInArray($header, $necessary_cols_bills);

			$necessary_cols_torrent = array("contig_id", "contig_srt", "contig_end", "region_id", "attributes", "gc_count", "overlaps", "fwd_e2e", "rev_e2e", "total_reads", "fwd_reads", "rev_reads", "cov20x", "cov100x",  "cov500x");
			$torrent_cols_present = $this->allValuesPresentInArray($header, $necessary_cols_torrent);

			// Since Bills code has problems and does not give a consistent output check many things in file before uploading file
			if ($bills_cols_present)
			{
				$sample_found = False;
				$sample_names_found = '';

				while(!feof($of))
				{
					$line = rtrim(fgets($of));
					
					$split_line = explode("\t", $line);

					if ($line != '' && sizeOf($split_line) >= 5)
					{
						$amp_linked_array = $this->array_combine($header, $split_line);

						///////////////////////////////////////////////////////
						// Check that sample_name starts with input sample name if not make 
						// a list of possible names 
						///////////////////////////////////////////////////////
						if (strpos($amp_linked_array['Sample'], $sample_name) !== False)
						{
							$sample_found = True;
						}
						else if (strpos($sample_names_found, $amp_linked_array['Sample'])=== False)
						{
							if ($sample_names_found !== '')
							{
								$sample_names_found.= ', ';	
							}
							
							$sample_names_found.= $amp_linked_array['Sample'];
						}
						///////////////////////////////////////////////////////
						// Check if gene is in possible_genes array
						///////////////////////////////////////////////////////
						if (!$this->checkStrInArrayStartsWithStr($amp_linked_array['Gene'], $possible_genes))
						{					
							return 'The Gene '.$amp_linked_array['Gene'].' is not an expected gene';
						}

						///////////////////////////////////////////////////////
						// Check that amplicon starts with gene in $possible_genes
						///////////////////////////////////////////////////////
						if (!$this->checkStrInArrayStartsWithStr($amp_linked_array['Amplicon'], $possible_genes))
						{
							
							return 'The Amplicon '.$amp_linked_array['Amplicon'].' does not contain an expected gene';
						}

						///////////////////////////////////////////////////////
						// Check that exon and Depth is an integer
						///////////////////////////////////////////////////////
						if (!preg_match("/^\d+$/", $amp_linked_array['Exon']))
						{				
							return 'Exon is expected to be an integer.  This was found as an exon '. $amp_linked_array['Exon'];
						}
						
						if (!preg_match("/^\d+$/", $amp_linked_array['Depth']))
						{				
							return 'Depth is expected to be an integer.  This was found as a Depth '. $amp_linked_array['Depth'];
						}

						///////////////////////////////////////////////////////
						// Check that Coding Region has the format num-num
						///////////////////////////////////////////////////////
						if (!preg_match("/^\d+-\d+$/", $amp_linked_array['Coding Region']))
						{				
							return 'Coding Region is expected to be of the format number-number.  This was found as a Coding Region '. $amp_linked_array['Coding Region'];
						}
					}				
				}

				// return error if sample name was never found
				if (!$sample_found)
				{
					return 'The sample name '.$sample_name.' was not found in your coverage file.  Here are possible sample names in your coverage file: '.$sample_names_found;
				}
			}


			if (!$bills_cols_present && !$torrent_cols_present)
			{
				return "Header does not contain expected columns.";
			}

			return True;
		}

		public function checkStrInArrayStartsWithStr($in_str, $in_array)
		{
			// ( 'ABC_23', (1 => 'ABC', 2 => 'DEF') ) -> True
			// ( 'BC_23', (1 => 'ABC', 2 => 'DEF') ) -> False

			foreach ($in_array as $key => $a_str)
			{
				if (preg_match("/^".$a_str."/", $in_str))
				{				
					return True;
				}
			}
			return False;
		}

		public function allValuesPresentInArray($check_array, $required_array)
		{
			// ( ('A', 'B', 'C'), ('B', 'C')) -> True 
			// ( ('A', 'B', 'C'), ('B', 'D')) -> False

			foreach ($required_array as $req)
			{
				if (!in_array($req, $check_array))
				{
					return False;					
				}				
			}
			return True;
		}

		public function MakeNestedInputs($group_num, $field_name, $val, $visibility='hide')
		{
			// Purpose:  Sometimes hidden inputs need to be automatically generated. This function will return a formated input which makes a nested post.  
			// (int, str, str) -> <input type="text" class="hide" id="Group1_field_name" name="Group1[field_name]" value="val">

			$input = '<input type="text" class="'.$visibility.'" id="Group'.$group_num.'_'.$field_name.'" name="Group'.$group_num.'['.$field_name.']" value="'.$val.'">';
			return $input;
		}

		public function ConvertDatabaseVar($row_name, $value, $extra_val=Null)
		{
			// (str, str) -> str
			// Purpose: depending on the row_name some formating might be needed.  
			// Example:
			// There might be a row_name of refs convert this to include https://www.ncbi.nlm.nih.gov/pubmed/27070783,23777544


			switch($row_name)
			{

				case 'loc':
					return '<a href="http://www.ensembl.org/Homo_sapiens/Location/View?r='.$extra_val.':'.$value.'" target="_blank">'.$value.'</a>';

					break;

				case 'cosmic':
					// input     [cosmic] => 12366:24268:13553:26129
					// get each cosmic id and add url
					$split_value = explode(':', $value);
					$to_return = '';

					foreach($split_value as $cosmic_id)
					{
						$to_return .= '<a href="http://cancer.sanger.ac.uk/cosmic/mutation/overview?id='.$cosmic_id.'"target="_blank">'.$cosmic_id.'</a> ';
					}
					return $to_return;
					break;

				case 'dbsnp':

					// input rs397517129:rs121434568:rs121913443

					$split_value = explode(':', $value);
					$to_return = '';

					foreach($split_value as $rs)
					{
						$to_return .= '<a href="https://www.ncbi.nlm.nih.gov/projects/SNP/snp_ref.cgi?rs='.str_replace('rs', '',$rs).'"target="_blank">'.$rs.'</a> ';
					}
					return $to_return;

					break;

				case 'pmid':


					// most of the time there are a lot of pmids change the shown data to a count of number of references
					$count = sizeOf(explode(',', $value));

					if ($value != '')
					{
						return '<a href="https://www.ncbi.nlm.nih.gov/pubmed/'.$value.'"target="_blank">'.(string)$count.'  references</a>';
					}
					else
					{
						return '';
					}
					break;

				case 'genes':
				
					$link = $value.': <a href="http://www.genecards.org/cgi-bin/carddisp.pl?gene='.$value.'"target="_blank" title="gene cards"><img src="images/logo_genecards.png"></a>';
					if (isset($this->halmark_gene[$value]) && $this->halmark_gene[$value] == 'yes')
					{
						$link .= '<a href="http://cancer.sanger.ac.uk/cosmic/census-page/'.$value.'"target="_blank" title="comsic halmark"><img src="images/gene_cosmic_logo.png"></a>';
					}
					
					$link .= ' <a href="https://www.genenames.org/cgi-bin/gene_symbol_report?hgnc_id=HGNC:'.$extra_val.'" target="_blank" title="HUGO Gene Nomenclature Committee"><img src="images/hugo.png" ></a>';



					return $link;
					break;

				case 'pfam':

					$split_value = explode(':', $value);
					$to_return = '';

					foreach($split_value as $pfam)
					{
						$to_return .= '<a href="http://pfam.xfam.org/family/'.$pfam.'"target="_blank">'.$pfam.'</a> ';
					}
					return $to_return;
					break;

				case 'transcript':
					// check if an ensembl or NM number
					if (strpos($value, 'NM_') !== false)
					{
						return '<a href="https://www.ncbi.nlm.nih.gov/nuccore/'.$value.'"target="_blank">'.$value.'</a>';
					}
					elseif (strpos($value, 'ENST') !== false)
					{
						return '<a href="http://www.ensembl.org/Homo_sapiens/Transcript/Summary?t='.$value.'"target="_blank">'.$value.'</a>';
					}
					

					break;

				default:
					return $value;
			}
		}

		public function DoubleAscSort($row_1_nam, $row_2_nam, $in_array)
		{
			if (!empty($in_array))
			{
				// sort an array of arrays
				foreach ($in_array as $key => $row)
				{
				    $row_1[$key] = $row[$row_1_nam];
				    $row_2[$key] = $row[$row_2_nam];
				}

				array_multisort($row_1, SORT_ASC, $row_2, SORT_ASC, $in_array);
			}

			return $in_array;			
		}

		public function TimeDayGreeting()
		{
			// () -> str
			// purpose: get random greeting

			$hour = date('H');

			/* If the time is less than 1200 hours, show good morning */
			if ($hour < '12')
			{
				$time = 'Good morning';
			}

			/* If the time is grater than or equal to 1200 hours, but less than 1700 hours, so good afternoon */
			elseif ($hour >= '12' && $hour < '17')
			{
				$time = 'Good afternoon';
			}

			/* Should the time be between or equal to 1700 and 1900 hours, show good evening */
			elseif ($hour >= '17' && $hour < '19')
			{
			   $time = 'Good evening';
			}

			/* Finally, show good night if the time is greater than or equal to 1900 hours */
			elseif ($hour >= '19')
			{
				$time = 'Good night';
			}

			// pick one of the possible greetings below.
			$poss_greetings = array('Welcome to Turbo Storage', 'Howdy', 'Hello there,', 'Bonjour', 'Hello! Welcome to Turbo Storage', 'Hi there', 'Nice to see you', $time);
			$i = rand(0, count($poss_greetings)-1);
			$greeting = $poss_greetings[$i];

			return $greeting;
		}

		public function MySQLTimeStampToDate($time_stamp)
		{
			// convert a mysql time stamp to a date
			// (str) -> str
			// (2017-09-26 10:15:29) -> Sep 26 2017

			return date('M j Y', strtotime($time_stamp));
		}

		public function TextToUrls($string)
		{
			// (str) -> str
			// take a string of text and convert string urls to html links

			$regex = '/\b(https:\/\/|ftp:\/\/|http:\/\/|www\.)[-A-Z0-9+&@#\/%?=~_|$!:,.;]*[A-Z0-9+&@#\/%=~_|$]/i';
			preg_match_all($regex, $string, $matches);
			$urls = $matches[0];

			if (!empty($urls))
			{
				// go over all links and change all links in string to an external link
				foreach($urls as $url) 
				{
					// add http to links to make an external links 
					if (strpos($url, 'http') === false)
					{
						$updated_url = 'http://'.$url;
						$string = str_replace($url, '<a href="'.rtrim($updated_url, '/).$/').'" target="_blank">'.$url.'</a>', $string);
					}
					else
					{
						$string = str_replace($url, '<a href="'.rtrim($url, '/).$/').'" target="_blank">'.$url.'</a>', $string);
					}
					
				}
				return $string;
			}
			else
			{
				return $string;
			}

		}

		public function AddPMIDUrls($string)
		{
			// (string) -> string
			// Find in a large string PMID: then take whatever is after it as the number
			// Example: PMID:20008640
			$regex = '/(PMID: |PMID :)[0-9]*/';
			preg_match_all($regex, $string, $matches);
			$pmids = $matches[0];

			if (!empty($pmids))
			{
				foreach($pmids as $i=>$pmid)
				{
					$pmid = explode(':',str_replace(' ', '', $pmid))[1];

					$url = 'https://www.ncbi.nlm.nih.gov/pubmed/'.$pmid;

					$string = str_replace($pmid, '<a href="'.rtrim($url, '/).$/').'" target="_blank">'.$pmid.'</a>', $string);

					return $string;
				}
			}
			else
			{
				return $string;
			}
		}

		public function AddItalicGenes($string)
		{
			// () -> 

			$regex = '/(';
			// make regex of all genes
			foreach($this->halmark_gene as $gene=>$halmark)
			{
				$regex.=$gene.'|';
			}

			$regex .= ')*/';
			
			preg_match_all($regex, $string, $matches);
			$genes = $matches[0];

			if (!empty($genes))
			{
				// make a unique list of all genes found in string
				$unique_genes = array();

				foreach($genes as $i=>$gene)
				{
					if (!in_array($gene, $unique_genes) && $gene !== '')	
					{
						array_push($unique_genes, $gene);
					}
					
				}			


				foreach($unique_genes as $gene)
				{
					$italic_gene = '<i>'.$gene.'</i>';

					$string = str_replace($gene, $italic_gene, $string);	
					
				}

				return $string;
				
			}
			else
			{
				return $string;
			}


			return $string;
		}

		public function AddPMIDLinkUrls($string)
		{
			$string = $this->TextToUrls($string);
			$string = $this->AddPMIDUrls($string);
		
			return $string;
		}

		public function RunDateToUSDate($run_date_chip)
		{
			// (str) -> str
			// purpose: 170809B -> 08/09/2017

			// remove letters
			$date_string = $this->ReplaceLetters($run_date_chip);

			$return_string = substr($date_string, 4,5);
			$return_string .='/';
			$return_string .= substr($date_string, 2,2);
			$return_string .= '/20'.substr($date_string, 0,2);

			return $return_string;
		}

		public function PickRandomFileFromFolder($folder_address)
		{
			// (str) -> str
			// Purpose: pick a random file from a folder.  Use this to pick a a random img from a folder of img

			// get all files in folder removing hidden folders and files
			$files = array_diff(scandir($folder_address), array('.', '..'));

			// pick random number starting from the first index in array
			$i = rand(array_keys($files)[0], count($files)-1);

			// provide random address
			return $folder_address.'/'.$files[$i];
		}

		public function oddEvenSectionMark($num)
		{
			// (int) -> str
			// if the number entered is odd return a thick blue line 
			// if the number entered is even return a think red line

			if ($this->isEven($num))
			{
				return '<hr class="thick-red-line">';
			}
			else
			{
				return '<hr class="thick-blue-line">';
			}
		}

		public function oddEvenBackground($num)
		{
			if ($this->isEven($num))
			{
				return 'style="background-color:#fbeded;"';
			}
			else
			{
				return 'style="background-color:#eaf1f7;"';				
			}			
		}

		public function isEven($num)
		{
			if ($num % 2 == 0)
			{
				return True;
			}
			else
			{
				return False;
			}
		}

		public function zipFolderDownload($in_folder, $zip_file)
		{
			// zip a folder into a file and download it.

			// Get real path for our folder
			$rootPath = realpath($in_folder);

			// Initialize archive object
			$zip = new ZipArchive();
			$zip->open($zip_file, ZipArchive::CREATE | ZipArchive::OVERWRITE);

			// Create recursive directory iterator
			/** @var SplFileInfo[] $files */
			$files = new RecursiveIteratorIterator(
			    new RecursiveDirectoryIterator($rootPath),
			    RecursiveIteratorIterator::LEAVES_ONLY
			);

			foreach ($files as $name => $file)
			{
			    // Skip directories (they would be added automatically)
			    if (!$file->isDir())
			    {
			        // Get real and relative path for current file
			        $filePath = $file->getRealPath();
			        $relativePath = substr($filePath, strlen($rootPath) + 1);

			        // Add current file to archive
			        $zip->addFile($filePath, $relativePath);
			    }
			}

			// Zip archive will be created only after closing object
			$zip->close();

			header('Content-Description: File Transfer');
			header('Content-Type: application/octet-stream');
			header('Content-Disposition: attachment; filename='.basename($zip_file));
			header('Content-Transfer-Encoding: binary');
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Pragma: public');
			header('Content-Length: ' . filesize($zip_file));
			readfile($zip_file);
		}

		public function arrayToCommaSepStr($combine_array, $ignore_str)
		{
			// (arr, str) -> comma separated str
			// Take an array and add all values into a comma separated string except if the string is in the ignore str
			
			// $combine_array Example
				// array (size=14)
				//   0 => string 'micro-infotrack-dev' (length=19)
				//   1 => string 'micro-pipeline-dev' (length=18)
				//   2 => string 'validation' (length=10)
				//   3 => string 'pending' (length=7)
				//   4 => string 'completed' (length=9)
				//   5 => string 'not-discussed' (length=13)
				//   6 => string 'seq-problem' (length=11)
				//   7 => string 'project-1' (length=9)
				//   8 => string 'project-12' (length=10)
				//   9 => string 'project-13' (length=10)
				//   10 => string 'project-14' (length=10)
				//   11 => string 'project-15' (length=10)
				//   12 => string 'project-16' (length=10)
				//   13 => string 'project-17' (length=10)

			// ignore Example 'not-discussed'
			// Example result:

			$out_str = '';
			foreach ($combine_array as $key => $val)
			{
				if ($val !== $ignore_str)
				{
					if ($out_str != '')
					{
						$out_str.= ',';
					}
					$out_str.=$val;
				}
			}	
			return $out_str;
		}

		public function UnderscoreCaseToHumanReadable($in_str)
		{
			// str -> str
			// last_name -> Last Name

			return ucwords(str_replace('_', ' ', $in_str));
		}

		public function DashCaseToHumanReadable($in_str)
		{
			return ucwords(str_replace('-', ' ', $in_str));
		}

		public function ReplaceSpecialChars($string)
		{
			$string = preg_replace('/[^a-zA-Z0-9_.]/', '', $string);

			$string = str_replace('.', '_', $string);

			return $string;
		}

		public function ReplaceLetters($string)
		{
			return preg_replace("/[^0-9,.]/", "", $string);
		}


		public function InputAID($in_key)
		{
			// if given a key see if it is an id.  Return TRUE if it is an id otherwise return false

			return in_array($in_key, $this->ignore_ids);
		}

		public function InputNotIgnore($in_key)
		{
			return in_array($in_key, $this->ignore_fields);
		}

		public function SearchPostArray($redirect_page, $post_array)
		{

			$url = 'Location:'.REDIRECT_URL.'?page='.$redirect_page;

			// remove submit and all empty searches.  If array ends up empty notify user with message
			foreach($post_array as $key => $val)
			{
				if ($val === '' || $val === 'Submit')
				{
					unset($post_array[$key]);
				}

				else
				{
					$url .= '&'.$key.'='.$val;

				}
			}

			if (empty($post_array))
			{
				return 'OOPS! Please enter a search parameter.';
			}

			else
			{
				// add all remaining $post_array to url
				return $url;
			}
		}
		
		public function in_array_r($needle, $haystack, $strict=false) 
		{
			// (str or int, multi dimensional array, bool) -> bool

    			foreach ($haystack as $item) 
    			{
        			if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && $this->in_array_r($needle, $item, $strict))) 
        			{
            			return true;
            		}
	        	}
	    

    			return false;
		}

		public function FindSubArrayContaining($needle, $haystack, $strict=false)
		{
			// (str or int, multi dimensional array, bool) -> bool or array

    			foreach ($haystack as $item) 
    			{
        			if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && $this->in_array_r($needle, $item, $strict))) 
        			{
            			return $item;
            		}
	        	}
	    

    			return false;
		}

		public function NestedArrayContainingKeyValPair($haystack, $key, $val, $strict=false)
		{
			// (str or int, multi dimensional array, bool) -> bool or array

    			foreach ($haystack as $item) 
    			{

        			if 	(
        					isset($item[$key]) && 
        					($strict ? $item[$key] === $val : $item[$key] == $val)
        				) 
        			{
            			return true;
            		}
	        	}
	    

    			return false;
		}

		public function calc_age($dob, $today)
		{
			// (date, date) -> int
			// input in yyyy-mm-dd 
			$dob = date_create($dob);
			$today = date_create($today);
			$diff = date_diff($dob, $today);
			return $diff->format('%y');

		}

		public function days_past($start_date, $end_date)
		{
			// (date, date) -> int
			// input in yyyy-mm-dd 
			// Calculating the difference in timestamps
			$diff = strtotime($start_date) - strtotime($end_date);

			// 1 day = 24 hours 
			// 24 * 60 * 60 = 86400 seconds 
			return abs(round($diff / 86400));
		}

     }
?>
