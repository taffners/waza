<?php
	
	class ExtraFieldsUtils extends utils
	{
		private $field_count = 0;

		public function MakeExtraFieldUpdate($in_array, $table_name, $id_name, $field)
		{
			// (array, str, str, str) -> html

			if (!isset($in_array) || empty($in_array))
			{
				return '';
			}

			$html ='<hr class="thick-grey-line">';
			$html.='<h3 class="text-center">Already Added '.$this->UnderscoreCaseToHumanReadable($field).'</h3>';
			$html.= '<div class="make-extra-field-update">';
			
			foreach ($in_array as $key => $val)
			{
				$label = 'Added';

				$this->field_count++;

				$curr_shared_name = $field.'_'.$this->field_count;

				if (isset($val['user_name']))
				{
					$label.= ' By '.$val['user_name'];
				}

				if (isset($val['time_stamp']))
				{
					$label.= ' On '.$val['time_stamp'];
				}
				$html.='<div class="form-group row">';
					$html.='<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">';
						$html.='<label for="previous_sample_id" class="form-control-label">'.$label.':</label>';
					$html.='</div>';
					$html.='<div class="col-xs-12 col-sm-4 col-md-4" id="'.$curr_shared_name.'_shown_to_user_data">';
						$html.=$val[$field];
					$html.='</div>';
					
					$html.='<div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 ml-0 pl-0">';
						$html.='<button type="button" class="btn btn-danger btn-danger-hover extra-field-toggle" data-shared_name="'.$curr_shared_name.'" data-toggle_type="minus" title="Delete '.$field.' with value '.$val[$field].'">';
		        			$html.='<span class="fas fa-minus-circle"></span>';
		            	$html.='</button>';
					
						$html.='<button type="button" class="btn btn-success btn-success-hover extra-field-toggle" data-shared_name="'.$curr_shared_name.'" data-toggle_type="plus" title="Keep '.$field.' with value '.$val[$field].'">';
		        			$html.='<span class="fas fa-plus-circle"></span>';
		            	$html.='</button>';
					$html.='</div>';

					// Add input that will change from del_toggle to keep_toggle name with the plus and minus buttons
					$html.='<div class="d-none">';
						// keep_status
						$html.='<input type="text" class="form-control" id="'.$curr_shared_name.'_keep_status" name="extra_fields_update['.$this->field_count.'][keep_status]" value="keep"/>';
						// table name
						$html.='<input type="text" class="form-control" id="'.$curr_shared_name.'_table_name" name="extra_fields_update['.$this->field_count.'][table_name]" data-shared_name="'.$curr_shared_name.'" value="'.$table_name.'"/>';
						// id_name
						$html.='<input type="text" class="form-control" id="'.$curr_shared_name.'_id_name" name="extra_fields_update['.$this->field_count.'][id_name]" data-shared_name="'.$curr_shared_name.'" value="'.$id_name.'"/>';
						// value
						$html.='<input type="text" class="form-control" id="'.$curr_shared_name.'_id_val" name="extra_fields_update['.$this->field_count.'][id_val]" data-shared_name="'.$curr_shared_name.'" value="'.$val[$id_name].'"/>';
						// field
						$html.='<input type="text" class="form-control" id="'.$curr_shared_name.'_field" name="extra_fields_update['.$this->field_count.'][field]" data-shared_name="'.$curr_shared_name.'" value="'.$field.'"/>';
					$html.='</div>';
				$html.='</div>';
				
			}
			$html.='</div>';
			$html.='<hr class="thick-grey-line">';
			return $html;

		}
	}

?>