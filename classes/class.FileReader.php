<?php

	class FileReader extends utils
	{
		private $db;
		private $of;

		private $password;
		private $return_data;
		private $gene_name;

		public function __construct($settings_array)
		{
			$this->settings_array = $settings_array;

			$this->db = isset($this->settings_array['db']) ? $this->settings_array['db'] : '';

			// open file 
			$this->of = fopen($this->settings_array['file'], 'r') or die('unable to open file!');

			$this->header_info = array();

			switch ($this->settings_array['type'])
			{
				case 'Archer VariantPlex Myeloid Assay':
				case 'Myeloid/CLL Mutation Panel':
					$this->readVariantStudioTSV();
					break;
				case 'depth_coverage':
					$this->readDepthCoverage();
					break;
				case 'torrent_coverage_file':
					$this->readTorrentCoverage();
					break;
				case 'Oncomine Focus Hotspot Assay':
					$this->readIonTorrentTSV();
					break;
				case 'torrent_header':
					$this->TorrentHeaderReader();
					break;
				case 'genes_in_assays':
					$this->readGenesInAssays();
					break;
				case 'update_cosmic_knowledge_base':
					$this->gene_name = $this->settings_array['gene_name'];		
					break;
				case 'accession_in_cosmic':
					$this->findAccessionForGene();
					break;
				case 'ofa_amplicons':
					$this->findOFAAmplicons();
					break;
			}
		}

		private function findOFAAmplicons()
		{
			// Purpose: Read ofa amplicon file and get all amplicon info for each gene
			// {'gene'   =>  'amp_name' => }

			$this->settings_array['amp_array'] = array();
			$header = rtrim(fgets($this->of));

			while(!feof($this->of))
			{
				$line = rtrim(fgets($this->of));
				$split_line = explode("\t", $line);
				
				if ($line !== '')
				{
					$info = explode(';', $split_line[5]);
					$info_array = array();

					// split at = and put in info array
					// example GENE_ID=MTOR;PURPOSE=Hotspot;CNV_ID=MTOR;CNV_HS=0
					foreach ($info as $key => $amp_info)
					{
						$info_explode = explode("=", $amp_info);
						$info_array[$info_explode[0]] = $info_explode[1];
					}

					$amp_info_array = array(
						$split_line[3] => array(
							'chr' 	=> 	$split_line[0],
							'start' 	=> 	$split_line[1],
							'end' 	=> 	$split_line[2]
						)
					);

					// find if gene in $gene_amp_link_array

					if (!isset($this->settings_array['amp_array'][$info_array['GENE_ID']]))
					{
						$this->settings_array['amp_array'][$info_array['GENE_ID']] = array();			
					}
					array_push($this->settings_array['amp_array'][$info_array['GENE_ID']], $amp_info_array);						

				}			
			}
		}

		public function getOFAAmplicons()
		{
			return $this->settings_array['amp_array'];
		}

		private function findAccessionForGene()
		{
			ini_set('max_execution_time', 300);
			// add transcript LOC to $this->settings_array
			$this->settings_array['transcript'] = [];

			$line = rtrim(fgets($this->of));
			$header = explode("\t", $line);

			while(!feof($this->of))
			{
				$line = rtrim(fgets($this->of));
				$split_line = explode("\t", $line);
				$cosmic_row = $this->array_combine($header, $split_line);

				if ($this->settings_array['gene'] === $cosmic_row['Gene name'])
				{
					array_push($this->settings_array['transcript'], $cosmic_row['Accession Number']);
					$this->settings_array['transcript'];
					break;
				}				
			}			
		}
		public function getAccessionForGene()
		{
			return $this->settings_array['transcript'];
		}

		public function close_file()
		{
			fclose($this->of);
		}

		public function updateCosmicKnowledgeBase()
		{
			$line = rtrim(fgets($this->of));
			$header = explode("\t", $line);
			$line_num = 0;
			$num_inserted_snvs = 0;

			while(!feof($this->of))
			{
				$line = rtrim(fgets($this->of));
				$line_num++;
				
				// Ignore empty line and only get snvs relevant to the current gene updating
				// Some entries have genename_transcript These seem to be the least used
				// transcript.  I'm going to ignore these and only enter the genes without 
				// underscore
				if (!empty($line) && preg_match("/^".$this->gene_name."\t/", $line)) 
				{		
					
					//////////////////////////////////////////////////////////////////// 
					// trim data to relevant knowledge base data and get in a snv_array
					////////////////////////////////////////////////////////////////////
					$split_line = explode("\t", $line);
					$snv_linked_array = $this->array_combine($header, $split_line);
					$long_hand_AA = $this->convert_AA_to_long_hand($snv_linked_array['Mutation AA']);
					$chr_loc = explode(':',$snv_linked_array['Mutation genome position']);
					$snv_array = array(
						'transcript' 		=> 	$snv_linked_array['Accession Number'],
						'fathmm_prediction' => 	$snv_linked_array['FATHMM prediction'],
						'fathmm_score' 	=> 	$snv_linked_array['FATHMM score'],
						'genes' 			=> 	$snv_linked_array['Gene name'],
						'hgnc' 			=> 	$snv_linked_array['HGNC ID'],
						'amino_acid_change'	=> 	$long_hand_AA,
						'amino_acid_change_abbrev'=> 	$snv_linked_array['Mutation AA'],
						'coding'			=> 	$snv_linked_array['Mutation CDS'],
						'mutation_type'	=> 	$snv_linked_array['Mutation Description'],
						'cosmic'			=> 	str_replace('COSM','',$snv_linked_array['Mutation ID']),
						'pmid'			=> 	$snv_linked_array['Pubmed_PMID'],
						'chr'			=> 	isset($chr_loc[0]) ? $chr_loc[0] : '',
						'loc'			=> 	isset($chr_loc[1]) ? $chr_loc[1] : '',
					);	

					///////////////////////////////////////////////////////////////////////
					// remove snv if coding or amino_acid_change contains ?
					///////////////////////////////////////////////////////////////////////
					if (!preg_match("/\?/", $snv_array['coding']) && !preg_match("/\?/", $snv_array['amino_acid_change']) && !preg_match("/unknown/i", $snv_array['mutation_type']))
					{
						///////////////////////////////////////////////////////////////////
						// Find if variant is in knowledge base already.  
						///////////////////////////////////////////////////////////////////

						$found_snv = $this->db->listAll('kbt-snv', $snv_array);
						
						if (empty($found_snv))
						{
							// count number of inserts
							$num_inserted_snvs++;
							$insert_response = $this->db->addOrModifyRecord('knowledge_base_table', $snv_array);
						}
					
						///////////////////////////////////////////////////////////////////
						// If it is check if it needs there are empty values.  
						// ONLY UPDATE EMPTY COLUMNS
						///////////////////////////////////////////////////////////////////
					}			
				}		
			}//while
			
			///////////////////////////////////////////////////////////////////
			// Add/update gene to gene_update_table
			///////////////////////////////////////////////////////////////////
			$update_gene_array = array('gene' 	=> $this->gene_name);
			$update_gene_response = $this->db->addOrModifyRecord('gene_update_table', $update_gene_array);

			return array(
				'num_inserted_snvs' 	=> 	$num_inserted_snvs, 
				'line_num' 			=> 	$line_num,
				'gene'				=> 	$this->gene_name,
				'add_id' 				=>   isset($insert_response) ? $insert_response : 'N/a',
				'snv'				=> 	$snv_array,
				'update_gene_response'	=> 	$update_gene_response
			);
		}

		private function readGenesInAssays()
		{
			$line = rtrim(fgets($this->of));
			$header = explode(",", $line);

			$this->return_data = array();
			
			while(!feof($this->of))
			{
				$line = rtrim(fgets($this->of));

				$split_line = explode(",", $line);
				if (!empty($split_line))
				{
					$gene_array = $this->array_combine($header, $split_line);

					if (isset($this->settings_array['panel_type'])  && $gene_array[$this->settings_array['panel_type']] === 'yes')
					{
						array_push($this->return_data, $gene_array['gene']);
					}
					else if (!isset($this->settings_array['panel_type']))
					{
						array_push($this->return_data, $gene_array['gene']);
					}
				}			
					
			}
		}

		public function return_data()
		{
			return $this->return_data;
		}

		private function readTorrentCoverage()
		{
			// Read thru a Torrent Coverage file formated file and add all amplicons which have a Coverage of less than 500 
			// And are the same sample.  
			// Torrent Coverage file Format
				// Contains all samples on chip
				// Might contain all data or just the low coverage data depending on which file the user selected.  The user is instructed to provide the low coverage only amplicon coverage file.
				// header
					// array (size=7)
					// 	0 => string 'Gene' (length=4)
					// 	1 => string 'Amplicon' (length=8)
					// 	2 => string 'Sample' (length=6)
					// 	3 => string 'Coverage' (length=8)
					// 	4 => string 'chrom' (length=5)
					// 	5 => string 'start' (length=5)
					// 	6 => string 'stop' (length=4)

			// get the header of the file
			$line = rtrim(fgets($this->of));
			$header = explode("\t", $line); 

			while(!feof($this->of))
			{
				$line = rtrim(fgets($this->of));
				$split_line = explode("\t", $line);
				$amp_linked_array = $this->array_combine($header, $split_line);

				// Make sure coverage falls below 500 reads.  This is MD lab cut off.  Also make sure this is the correct sample.  
				// this coverage file contains all samples on the chip
				if 	(
						$this->varSetNotEmpty($amp_linked_array['Coverage']) && 
						intval($amp_linked_array['Coverage']) < 500 &&
						$this->settings_array['sample_name'] === $amp_linked_array['Sample'] 
					)
				{		

					$low_cov_array = array(
						'gene' 		=> 	$amp_linked_array['Gene'],
						'Amplicon' 	=>	$amp_linked_array['Amplicon'],
						'depth' 		=>	$amp_linked_array['Coverage'],
						'run_id'		=> 	$this->settings_array['run_id'],
						'chr'		=> 	$amp_linked_array['chrom'],
						'amp_start'	=> 	$amp_linked_array['start'],
						'amp_end'		=> 	$amp_linked_array['stop']
					);	

					// check if amplicon already added
					$amp_cov_exists = $this->db->listAll('low-coverage-by-run-id-gene-amplicon', $low_cov_array);

					if (empty($amp_cov_exists))
					{
						$low_cov_result = $this->db->addOrModifyRecord('low_coverage_table', $low_cov_array);						
					}									
				}
			}
		}

		private function readDepthCoverage()
		{
			// add coverage information for myeloid panel
			// get the header of the file
			$line = rtrim(fgets($this->of));
			$header = explode("\t", $line); 

			while(!feof($this->of))
			{
				$line = rtrim(fgets($this->of));
				$split_line = explode("\t", $line);
				$amp_linked_array = $this->array_combine($header, $split_line);

				// Add low coverage amplicons for the input $this->settings_array['sample_name'] 
				if (isset($amp_linked_array) && strpos($amp_linked_array['Sample'], $this->settings_array['sample_name']) !== false )
				{	
					$low_cov_array = array(
						'gene' 		=> 	$amp_linked_array['Gene'],
						'Amplicon' 	=>	$amp_linked_array['Amplicon'],
						'exon' 		=>	$amp_linked_array['Exon'] == '0'? 'N/A':$amp_linked_array['Exon'],
						'codons' 		=>	$amp_linked_array['Coding Region'] == '0-0'? 'intron':$amp_linked_array['Coding Region'],
						'depth' 		=>	$amp_linked_array['Depth'],
						'run_id'		=> 	$this->settings_array['run_id']
					);
				
					// check if amplicon already added
					$amp_cov_exists = $this->db->listAll('low-coverage-by-run-id-gene-amplicon', $low_cov_array);

					if (empty($amp_cov_exists))
					{
						$low_cov_result = $this->db->addOrModifyRecord('low_coverage_table', $low_cov_array);
					}
				}
			}
		}

		private function readDepthTres()
		{
			// Most likely this function is useless.  It was to read a file that Bill made.
			// However, he keeps on changing the format so the new function is readDepthCoverage to 
			// read a different file format

			while(!feof($this->of))
			{
				$line = rtrim(fgets($this->of));
				$split_line = explode(",", $line);

				if (isset($split_line[2]) && strpos($split_line[2], $this->settings_array['sample_name']) !== false)
				{
					// Add low coverage amplicons 
					$low_cov_array = array(
						'gene' 		=> 	$split_line[7],
						'Amplicon' 	=>	$split_line[8],
						'exon' 		=>	$split_line[10],
						'codons' 		=>	$split_line[11],
						'depth' 		=>	$split_line[12],
						'run_id'		=> 	$this->settings_array['run_id']
					);

					// check if amplicon already added
					$amp_cov_exists = $this->db->listAll('low-coverage-by-run-id-gene-amplicon', $low_cov_array);

					if (empty($amp_cov_exists))
					{
						$low_cov_result = $this->db->addOrModifyRecord('low_coverage_table', $low_cov_array);
					}									
				}
			}
		}

		public function readCommonPasswords()
		{

			while(!feof($this->of))
			{
				$line = rtrim(fgets($this->of));
				$split_line = explode(",", $line);
					
				if ($line !== '')
				{
					$edited_regex = "/(".$split_line[1].")/i";
					$edited_regex = str_replace('a', '[a@]', $edited_regex);
					$edited_regex = str_replace('s', '[s$]', $edited_regex);
					$edited_regex = str_replace('o', '[o0]', $edited_regex);

					$html_return = "<strong>Password's can not be one of the most common passwords or contain one of the most common passwords. </strong><br><br> ";

					if (preg_match("/(".$split_line[1].")/i", $this->settings_array['password']))
					{
						
						if (!isset($split_line[3]) || $split_line[3] === '')
						{
							if ($split_line[0] === 'Default ngs password')
							{
								$html_return = 'The password '.$this->settings_array['password'].' is '.'the default ngs reporter password. Password needs to be different than the default.';
							}

							else
							{
								$html_return .= 'The password '.$this->settings_array['password'].' is '.$split_line[0].'/300 most common passwords.  '.$split_line[2];
							}
							
							return $html_return .= '<br><br><video width="80%" controls autoplay><source src="videos/Username_is_Password_Silicon_Valley.mp4" 	type="video/mp4"></video>';


						}	
						else
						{
							return '<strong>The password '.$this->settings_array['password'].' is '.$split_line[0].' out of the 300 most common passwords.  '.$split_line[2].'</strong><br><br><img src="images/'.$split_line[3].'">';
						}						
					}

					else if (preg_match($edited_regex, $this->settings_array['password']))
					{
						
						return '<strong>The password '.$this->settings_array['password'].' is just an edited version of '.$split_line[1].' which is '.$split_line[0].' out of the 300 most common passwords.  '.$split_line[2].'</strong><br><br><img src="images/Bart-Chalkboard-for-Blog-Post.png">';
					}
				}
	

			}
			return False;
		}
		
		private function TorrentHeaderReader()
		{
			// read header of ofa tsv file.  If filter_chain and/or work_flow_name
			// are present extract it.
				// filter_chain	(##filterChain=OFAv170807)
				// work_flow_name	(##workflowName=OFA_DNA_530_v170808)

			while(!feof($this->of))
			{
				$line = rtrim(fgets($this->of));
				
				// break loop if line does not start with #
				if (strpos($line, '#') === 0)
				{										
					// get filter_chain
					if (strpos($line, '##filterChain=') !== False)
					{
						$samp_name = str_replace('##filterChain=', '', $line);
						$this->header_info['filter_chain'] = $samp_name;
					}	

					// get work_flow_name
					if (strpos($line, '##workflowName=') !== False)
					{
						$samp_name = str_replace('##workflowName=', '', $line);
						$this->header_info['work_flow_name'] = $samp_name;
					}									
				}
				else
				{
					break;
				}		
			}				
		}

		public function GetTorrentheader()
		{
			return $this->header_info;
		}

		private function readIonTorrentTSV()
		{
			////////////////////////////////////////////////////////////////////
			// Read thru the Ion reporter API pulled TSV.
				// The variant tsvs are made via pool_qc_backup_data.php -> Class.thermo_backup(backupIonReporterData) -> thermo_backup_ion_reporter_run.py -> ir_api_per_sample -> curl to ion reporter analysis API
			// tsv file format
				// ## skip contains reference info only
				// # header
					// array (size=36)
					  // 0 => string '# locus' (length=7)
					  // 1 => string 'type' (length=4)
					  // 2 => string 'ref' (length=3)
					  // 3 => string 'length' (length=6)
					  // 4 => string 'genotype' (length=8)
					  // 5 => string 'filter' (length=6)
					  // 6 => string 'pvalue' (length=6)
					  // 7 => string 'coverage' (length=8)
					  // 8 => string 'allele_coverage' (length=15)
					  // 9 => string 'allele_ratio' (length=12)
					  // 10 => string '%_frequency' (length=11)
					  // 11 => string 'maf' (length=3)
					  // 12 => string 'gene' (length=4)
					  // 13 => string 'transcript' (length=10)
					  // 14 => string 'location' (length=8)
					  // 15 => string 'function' (length=8)
					  // 16 => string 'codon' (length=5)
					  // 17 => string 'exon' (length=4)
					  // 18 => string 'protein' (length=7)
					  // 19 => string 'coding' (length=6)
					  // 20 => string 'sift' (length=4)
					  // 21 => string 'polyphen' (length=8)
					  // 22 => string 'grantham' (length=8)
					  // 23 => string 'normalizedAlt' (length=13)
					  // 24 => string '5000Exomes' (length=10)
					  // 25 => string 'NamedVariants' (length=13)
					  // 26 => string 'clinvar' (length=7)
					  // 27 => string 'dbsnp' (length=5)
					  // 28 => string 'dgv' (length=3)
					  // 29 => string 'drugbank' (length=8)
					  // 30 => string 'exac' (length=4)
					  // 31 => string 'go' (length=2)
					  // 32 => string 'pfam' (length=4)
					  // 33 => string 'phylop' (length=6)
					  // 34 => string 'Oncomine Variant Annotator v2.4' (length=31)
					  // 35 => string 'MyVariantDefaultDb_hg19' (length=23)
			////////////////////////////////////////////////////////////////////
			while(!feof($this->of))
			{
				$line = rtrim(fgets($this->of));

				if (empty($line))
				{
					break;
				}

				// Get header 
				// Header is the line that starts with # 
				if ( substr($line, 0, 1) === '#')
				{
					$header = explode("\t", $line);				
				}

				// get variants
				else if 	(	
								strpos($line, '##') !== 0 && 
								isset($header) &&
								strpos($line, '#') !== 0
							)
				{
					$split_line = explode("\t", $line);

					///////////////////////////////////////////////////////////////////////////////////
					// Make a linked array of header and split line
					
					$variant = $this->array_combine($header, $split_line);

					// extract the column information from the data rows of the OFA tsv
					// which are saved in the MySQL DB.  If $variant_array is equal to 
					// false then either genes, coding, and/or amino_acid_change was empty
					$variant_array = $this->IonTorrentVaraintToKeepInfo($variant);
									
					// add variant array to observed variant table if genes, coding, and/or amino_acid_change were not empty
					if ($variant_array !== False)
					{
						$observed_result = $this->db->addOrModifyRecord('observed_variant_table', $variant_array);

						// Extract strand_bias_table info from ref_var_strand_counts 				
						$allele_counts = $this->OFAGetRefVarCounts($variant_array['ref_var_strand_counts'], $variant_array['ref_allele'], $variant_array['variant_allele']);

						if ($allele_counts !== false)
						{
							// add observed_variant_id and -1 as user_id since
							// it is computer added.  The user will be able to 
							// edit later then user_id will reflex a user
							$allele_counts['observed_variant_id'] = $observed_result[1];
							$allele_counts['user_id'] = -1;
							$strand_result = $this->db->addOrModifyRecord('strand_bias_table', $allele_counts);
						
						}						
					}
				}
			}
		}

		private function readIonTorrentWebsiteTSV()
		{
			////////////////////////////////////////////////////////////////////
			// Read thru the Ion Torrent website downloaded TSV.  This is old and no longer used 2/12/2020
			////////////////////////////////////////////////////////////////////
			while(!feof($this->of))
			{
				$line = rtrim(fgets($this->of));

				// Get header 
				// Header is the first line that does not start with ##
				if ((strpos($line, '##') !== 0) && !isset($header))
				{
					$header = explode("\t", $line);
				}

				// get variants
				else if ((strpos($line, '##') !== 0) && isset($header))
				{
					$split_line = explode("\t", $line);

					// Make a linked array of header and split line
					$variant = $this->array_combine($header, $split_line);

					// extract the column information from the data rows of the OFA tsv
					// which are saved in the MySQL DB.  If $variant_array is equal to 
					// false then either genes, coding, and/or amino_acid_change was empty
					$variant_array = $this->IonTorrentVaraintToKeepInfo($variant);
										
					// add variant array to observed variant table if genes, coding, and/or amino_acid_change were not empty
					if ($variant_array !== False)
					{

						$observed_result = $this->db->addOrModifyRecord('observed_variant_table', $variant_array);

						// Extract strand_bias_table info from ref_var_strand_counts 				
						$allele_counts = $this->OFAGetRefVarCounts($variant_array['ref_var_strand_counts'], $variant_array['ref_allele'], $variant_array['variant_allele']);

						if ($allele_counts !== false)
						{
							// add observed_variant_id and -1 as user_id since
							// it is computer added.  The user will be able to 
							// edit later then user_id will reflex a user
							$allele_counts['observed_variant_id'] = $observed_result[1];
							$allele_counts['user_id'] = -1;
							$strand_result = $this->db->addOrModifyRecord('strand_bias_table', $allele_counts);
						}
						
					}
				}
			}
		}

		private function OFAGetRefVarCounts($counts, $ref, $variant)
		{
			// The OFA tsv has a column called Ref+/Ref-/Var+/Var-.  This column 
			// contains coverage counts for reference and variant on both strands.
			// Sometimes there are inputs that contain other possibilities 
			// set to 0.  For instance:
				// 'T=4333/4911, A=346/501, G=0/0'  
				// 'GGAATTAAGAGAAGCAACATCTCCGA=402/266, GAGACCAACATCTCCGA=0/0, GATATCTCCGA=0/0, GATCCCGA=0/0, GATCTCGA=0/0, GATTCCGA=0/0, GATTCCTTCTCCGA=0/0, GCAACCAACATCTCCGA=0/0, GCAATCTCCGA=0/0, GCTATCTCCGA=0/0, GCTCTCGA=0/0, GGAACCAACATCTCCGA=110/57, GG=0/0, GGCATCTCCGA=0/0, GGCTCCGA=0/0, GTCATCTCCGA=0/0'
			// This function will need to only keep values which are not 0/0 and are about a combination of 10 reads.
			// Possible outputs:
				// False => if there are more than two non zero inputs
				// False => if ref and variant are not the two possible
				// linked array with strand_bias_table info 

			// explode at ,
	
			// remove all whitespace
			$counts = preg_replace('/\s+/', '', $counts);
			$counts_exploded = explode(',', $counts);

			$keeps = array();
			$add_count = 0;

			foreach ($counts_exploded as $key => $allele)
			{
				// allele example  C=1519/1355
				
				// explode at = (ex. 1519/1355)
				$explode_equal = explode('=', $allele);

				// explode at / (ex. array (size=2) 0 => string '1519' (length=4) 1 => string '1355')
				$explode_slash = explode('/', $explode_equal[1]);

				// find if len is 2 
				// see if both are are not zero otherwise ignore
				if (sizeof($explode_slash) === 2 && 
					($explode_slash[0] !== '0' && $explode_slash[1] !== '0'))
				{		

					// the first allele for strands is the reference
					if ($add_count === 0)
					{

							$keeps['ref_pos'] = $explode_slash[0];
							$keeps['ref_neg'] = $explode_slash[1];
							$add_count++;						
					}

					// All alleles found after reference are possible
					// variants.  Make sure the addition of both of them is >= 10
					else if($add_count >= 1)
					{
						if (intval($explode_slash[0]) + intval($explode_slash[1]) >= 20)
						{
							$keeps['variant_neg'] = $explode_slash[0];
							$keeps['variant_pos'] = $explode_slash[1];
							$add_count++;
						}
					}						
				}	
			}

			// make sure size is only 4.  This will remove instances where there are more than 2 possibilities for ref and var
			if (sizeof($keeps) === 4)
			{
				return $keeps;
			}
			else
			{			
				return false;				
			}
		}

		private function ionTorretFreq($in_string)
		{
			// The input of the frequency column can either be a float or of the following format: 
				//AAG=0.00, AGG=0.00, ATG=0.00, CAG=0.05, CGA=0.00, CGG=47.8, CGT=0.00 -> 47.8

			// for floats
			if (is_numeric(trim($in_string)))
			{

				return floatval($in_string);
			}

			// format AAG=0.00, AGG=0.00, ATG=0.00, CAG=0.05, CGA=0.00, CGG=47.8, CGT=0.00
			else if (strpos($in_string, ',') !== false) 
			{
				$freqs = explode(',', str_replace(' ', '', $in_string));
				$largest_freq = 0.0;

				foreach ($freqs as $f)
				{
					$seq_val = explode('=', $f);

					if (isset($seq_val[1]) && floatval($seq_val[1]) > 0)
					{

						$curr_freq = floatval($seq_val[1]);
						if ($curr_freq > $largest_freq)
						{
							$largest_freq = $curr_freq;
						}	
					}							
				}
			
				return $largest_freq;
			}	
			else
			{			
				return '';
			}		
		}

		private function ionTorretFreqsGreaterThanZero($in_string)
		{
			// AAG=0.00, AGG=0.00, ATG=0.00, CAG=0.05, CGA=0.00, CGG=47.8, CGT=0.00 -> CAG=0.05,CGG=47.8
			$freqs = explode(',', str_replace(' ', '', $in_string));
			$keep_freqs = '';

			foreach ($freqs as $f)
			{
				$seq_val = explode('=', $f);

				if (isset($seq_val[1]) && floatval($seq_val[1]) > 0)
				{
					if ($keep_freqs !== '')
					{
						$keep_freqs .= ',';
					}

					$keep_freqs .= $f;	
				}							
			}

			return $keep_freqs;
		}

		private function readVariantStudioTSV()
		{
			// header is the first line
			// header columns of interest 

			// genes				=>	Gene
			// coding				=>	HGVSc (after :)
			// amino_acid_change	=>	HGVSp (after :)
			// frequency			=>	Alt Variant Freq
			// genotype			=>	Reference Allele + / + Alternate Allele
			// phred_qual_score	=>	
			// allele_coverage	=>	Allelic Depths
			// allele_ratio		=>	
			// coverage			=>	Read Depth

			$header = explode("\t", rtrim(fgets($this->of)));

			while(!feof($this->of))
			{
				$line = rtrim(fgets($this->of));
				$split_line = explode("\t", $line);

				// make a key value array where column header is keys and values are 
				// associated with current variant
				$variant = $this->array_combine($header, $split_line);

				/////////////////////////////////////////////////////////////////
				// Extract data from $variant and add variants which are not Benign
				// Seq Error as an observed variant.  Add all classifications which are 
				// not blank to classification table
				/////////////////////////////////////////////////////////////////

				$variant_array = $this->VariantStudioVaraintToKeepInfo($variant);

				// add all classifications if a positive or negative control
				// For some reason Bill is using Benign variants as 35 of 
				// the variants he is looking for
				if ( isset($this->settings_array['sample_type']) && ($this->settings_array['sample_type'] === 'Positive Control' || $this->settings_array['sample_type'] === 'Negative Control'))
				{
					if ($variant_array !== False)
					{
						$observed_result = $this->db->addOrModifyRecord('observed_variant_table', $variant_array);
					}
				}

				// find classification which are possibly pathogenic to report.
				else if (isset($variant['Classification']) && (preg_match("/(Presumed)/i", $variant['Classification']) || !preg_match("/(benign|error)/i", $variant['Classification'])) )
				{
					if ($variant_array !== False)
					{
						$observed_result = $this->db->addOrModifyRecord('observed_variant_table', $variant_array);
					}
				}

				// Add all classifications which are not in the database to classification table
				if (isset($variant['Classification']) &&  $variant['Classification'] !== '')
				{

					if ($variant_array !== False)
					{

						$classify_array = array(
								'genes' 			=> 	$variant_array['genes'],
								'coding' 			=> 	$variant_array['coding'],
								'amino_acid_change' => 	$variant_array['amino_acid_change'],
								'classification'	=> 	$variant['Classification'],
								'panel' 			=> 	$this->settings_array['type']
							);
	
						$classify_exist = $this->db->listAll('classify-by-panel-gene-coding-protein', $classify_array);

						if (empty($classify_exist))
						{
							$classify_result = $this->db->addOrModifyRecord('classification_table', $classify_array);
						}
					}
				}
			}
		}

		private function renameVariantStudioGenes($gene)
		{
			// variant studio sometimes names genes incorrectly.  
			// rename these.  For example: SRSF2,MFSD11 should be SRSF2
			switch ($gene) 
			{
				case 'SRSF2,MFSD11':
					$gene = 'SRSF2';
					break;
			}

			return $gene;
		}

		private function IonTorrentVaraintToKeepInfo($variant_array)
		{
			// (linked array) -> linked array
			// extract the column information from the data rows of the OFA tsv
			// which are saved in the MySQL DB. 
			// return out_array which are not empty for genes, coding, and amino_acid_change otherwise return false
			// Example array from the Ion Reporter API file:
				// array (size=36)
				//   '# locus' => string 'chr7:55241677' (length=13)
				//   'type' => string 'INDEL' (length=5)
				//   'ref' => string 'GAA' (length=3)
				//   'length' => string '1' (length=1)
				//   'genotype' => string 'GAA/GAAA' (length=8)
				//   'filter' => string 'PASS' (length=4)
				//   'pvalue' => string '0.09203224157292343' (length=19)
				//   'coverage' => string '4575' (length=4)
				//   'allele_coverage' => string 'GAA=4463,AAA=1,CAA=0,CAT=0,GAAA=111' (length=35)
				//   'allele_ratio' => string 'GAA=0.9755,AAA=2.0E-4,CAA=0.0,CAT=0.0,GAAA=0.0243' (length=49)
				//   '%_frequency' => string 'AAA=0.02,CAA=0.00,CAT=0.00,GAAA=2.43' (length=36)
				//   'maf' => string '' (length=0)
				//   'gene' => string 'EGFR' (length=4)
				//   'transcript' => string 'NM_005228.4' (length=11)
				//   'location' => string 'EGFR:exonic:NM_005228.4' (length=23)
				//   'function' => string 'frameshiftInsertion' (length=19)
				//   'codon' => string 'AAC' (length=3)
				//   'exon' => string '18' (length=2)
				//   'protein' => string 'p.Thr710fs' (length=10)
				//   'coding' => string 'c.2128_2129insA' (length=15)
				//   'sift' => string '' (length=0)
				//   'polyphen' => string '' (length=0)
				//   'grantham' => string '' (length=0)
				//   'normalizedAlt' => string 'GA' (length=2)
				//   '5000Exomes' => string '' (length=0)
				//   'NamedVariants' => string '' (length=0)
				//   'clinvar' => string '' (length=0)
				//   'dbsnp' => string 'rs397517087:rs397517085:rs397517086:rs727504256' (length=47)
				//   'dgv' => string '' (length=0)
				//   'drugbank' => string 'N-[4-(3-BROMO-PHENYLAMINO)-QUINAZOLIN-6-YL]-ACRYLAMIDE:Icotinib:Vandetanib:Necitumumab:Panitumumab:Zalutumumab:Osimertinib:Canertinib:Varlitinib:Matuzumab:Olmutinib:Rindopepimut:Depatuxizumab mafodotin:Erlotinib:Afatinib:S-{3-[(4-ANILINOQUINAZOLIN-6-YL)AMINO]-3-OXOPROPYL}-L-CYSTEINE:Lapatinib:Cetuximab:Lidocaine:Gefitinib:IGN311:Trastuzumab:Alvocidib' (length=352)
				//   'exac' => string 'AF_OTH=0.0:AF_Adj=1.648E-5:AF_AFR=0.0:AF_EAS=0.0:AF_FIN=3.024E-4:AF_SAS=0.0:AF_NFE=0.0:AF_AMR=0.0' (length=97)
				//   'go' => string 'GO:1901185:GO:0007494:GO:0014066:GO:0003690:GO:1901224:GO:0046854:GO:1903800:GO:0043586:GO:0021795:GO:0000186:GO:0030307:GO:0001942:GO:0097489:GO:0045893:GO:0046982:GO:0050679:GO:0035556:GO:0030665:GO:0033138:GO:0019901:GO:0019903:GO:1900020:GO:0050729:GO:0005088:GO:0006970:GO:0045930:GO:1902722:GO:0043235:GO:0043234:GO:0048408:GO:0005006:GO:0005886:GO:0031901:GO:0051015:GO:0005634:GO:0005516:GO:0005515:GO:0031625:GO:0045780:GO:0071364:GO:0070435:GO:0004709:GO:0034614:GO:0051897:GO:0008283:GO:0043406:GO:004'... (length=1660)
				//   'pfam' => string 'PF00757:PF01030:PF00069:PF14843:PF07714' (length=39)
				//   'phylop' => string '9.84,9.25,0.57' (length=14)
				//   'Oncomine Variant Annotator v2.4' => string '' (length=0)
				//   'MyVariantDefaultDb_hg19' => string '' (length=0)
			///////////////////////////////////////////////////////////////////////////////////

			///////////////////////////////////////////////////////////////////////////////////
			// the Ion Reporter website downloaded file contains 3 columns that I was using that the IR API file does not contain.
				//  Observed Allele
					// Example: AAA,CAA,CAT,GAAA
					// Solution:
						// Added as variant_allele with thermo_backup_ion_reporter_run.py func(addStrandInfoToTSV)
				// Strand
					//  Example:  +
					//  Solution: Can Ignore this column not used in myeloid.  Not in vcf file used in thermo_backup_ion_reporter_run.py func(addStrandInfoToTSV) 
				// Ref+/Ref-/Var+/Var-
					// Example GAA=2332/2131, AAA=0/1, CAA=0/0, CAT=0/0, GAAA=5/106
					// Solution:
						// Added with thermo_backup_ion_reporter_run.py func(addStrandInfoToTSV) 
			///////////////////////////////////////////////////////////////////////////////////
			$out_array = array(
				'genes'	=>	isset($variant_array['gene']) ? $this->OFAGeneKeepOnlyAssayGene($variant_array['gene']):'',
				'coding'	=>	isset($variant_array['coding']) ? $this->removePipe($variant_array['coding']):'', 
				'amino_acid_change'	=>	isset($variant_array['protein']) ? $this->removePipe($variant_array['protein']):'',
				'frequency'			=>	isset($variant_array['%_frequency']) ? $this->ionTorretFreq($variant_array['%_frequency']):'',
				'genotype'			=>	isset($variant_array['genotype']) ? $variant_array['genotype']:'', 
				'allele_coverage'	=>	isset($variant_array['allele_coverage']) ? $this->ionTorretFreqsGreaterThanZero($variant_array['allele_coverage']):'',	
				'coverage'		=>	isset($variant_array['coverage']) ? $variant_array['coverage']:'',
				'ref_allele'	=> 	isset($variant_array['ref']) ? $variant_array['ref']:'',				
				'variant_allele'	=> 	isset($variant_array['variant_allele']) ? $variant_array['variant_allele']:'',				
				'ref_var_strand_counts' => 	isset($variant_array['Ref+/Ref-/Var+/Var-']) ? $variant_array['Ref+/Ref-/Var+/Var-']:''
			);

			/////////////////////////////////////////////////////////////////////////////////////////
			// Format to use with the readIonTorrentWebsiteTSV function
			/////////////////////////////////////////////////////////////////////////////////////////
			// $out_array = array(
			// 	'genes'	=>	isset($variant_array['Genes']) ? $this->OFAGeneKeepOnlyAssayGene($variant_array['Genes']):'',
			// 	'coding'	=>	isset($variant_array['Coding']) ? $variant_array['Coding']:'', 
			// 	'amino_acid_change'	=>	isset($variant_array['Amino Acid Change']) ? $variant_array['Amino Acid Change']:'',
			// 	'frequency'			=>	isset($variant_array['% Frequency']) ? $this->ionTorretFreq($variant_array['% Frequency']):'',
			// 	'genotype'			=>	isset($variant_array['Genotype']) ? $variant_array['Genotype']:'', 
			// 	'allele_coverage'	=>	isset($variant_array['Allele Coverage']) ? $this->ionTorretFreqsGreaterThanZero($variant_array['Allele Coverage']):'',	
			// 	'coverage'		=>	isset($variant_array['Coverage']) ? $variant_array['Coverage']:'',
			// 	'ref_allele'	=> 	isset($variant_array['Ref']) ? $variant_array['Ref']:'',
			// 	'variant_allele'	=> 	isset($variant_array['Observed Allele']) ? $variant_array['Observed Allele']:'',
			// 	'strand'	=> 	isset($variant_array['Strand']) ? $variant_array['Strand']:'',
			// 	'ref_var_strand_counts' => 	isset($variant_array['Ref+/Ref-/Var+/Var-']) ? $variant_array['Ref+/Ref-/Var+/Var-']:''
			// );

			return $this->KeepOutArrayOrNot($out_array);
		}

		private function removePipe($in_string)
		{
			// It was found that sometimes the IR API adds a | at the end of coding and amino_acid_change.  It was not found int the IR 
			// downloaded tsv.  Example: c.2317_2318insGTC|

			return str_replace('|', '', $in_string);
		}

		private function OFAGeneKeepOnlyAssayGene($gene)
		{
			// sometimes the ion reporter puts two genes together for instance CSDE1,NRAS instead of NRAS in the case of IR downloaded
			// tsv.  In the case of IR API made TSV the format is EGFR|EGFR-AS1

			switch ($gene)
			{
				case 'CSDE1,NRAS':
				case 'CSDE1|NRAS':
					return 'NRAS';
				
				case 'EGFR,EGFR-AS1':
				case 'EGFR|EGFR-AS1':
					return 'EGFR';

				default:
					return $gene;
			}
		}

		private function VariantStudioVaraintToKeepInfo($variant_array)
		{
			// Get keep info to add to observered variant data.  If Gene, coding, or amino_acid change is the empty string return False otherwise return array for insertion 
			// genes				=>	Gene
			// coding				=>	HGVSc (after :)
			// amino_acid_change	=>	HGVSp (after :)
			// frequency			=>	Alt Variant Freq
			// genotype			=>	Reference Allele + / + Alternate Allele
			// phred_qual_score	=>	
			// allele_coverage	=>	Allelic Depths
			// allele_ratio		=>	
			// coverage			=>	Read Depth

			$out_array = array(
				'genes'	=>	isset($variant_array['Gene']) ? $this->renameVariantStudioGenes($variant_array['Gene']):'',
				'coding'	=>	isset($variant_array['HGVSc']) ? $this->VaraintStudioCP($variant_array['HGVSc'], 'coding'):'', 
				'amino_acid_change'	=>	isset($variant_array['HGVSp']) ? $this->VaraintStudioCP($variant_array['HGVSp'], 'protein'):'',
				'frequency'			=>	isset($variant_array['Alt Variant Freq']) ? $variant_array['Alt Variant Freq']:'',
				'genotype'			=>	isset($variant_array['Reference Allele']) && isset($variant_array['Alternate Allele'])? $variant_array['Reference Allele'].'/'.$variant_array['Alternate Allele']:'', 
				'allele_coverage'	=>	isset($variant_array['Allelic Depths']) ? $variant_array['Allelic Depths']:'',	
				'coverage'		=>	isset($variant_array['Read Depth']) ? $variant_array['Read Depth']:'',
				'ref_allele'		=>	isset($variant_array['Reference Allele']) ? $variant_array['Reference Allele']:'',
				'variant_allele'		=>	isset($variant_array['Alternate Allele']) ? $variant_array['Alternate Allele']:'',
				'strand'		=>	'',
				'ref_var_strand_counts'		=>	''
			);

			// return out_array which are not empty for genes, coding, and amino_acid_change otherwise return false
			return $this->KeepOutArrayOrNot($out_array);
		}

		private function VaraintStudioCP($in_string, $AA_type)
		{
			// Split at : and make sure result is of expected type
			// (str, str) -> str
			// >>> ('NM_005089.3:c.557+83T>G', 'coding') -> {'c.557+83T>G'}

			if (strpos($in_string, ':') !== false)
			{
				$split_str = explode(':', $in_string);

				// make sure the split str is len 2
				if (sizeof($split_str) === 2)
				{
					switch ($AA_type)
					{
						case 'coding':
							if ($this->has_prefix($split_str[1], 'c'))
							{
								return $split_str[1];
							}
							else
							{
								return '';
							}
							break;
						
						case 'protein':
							if (strpos($split_str[1], '(p.=)') !== false)
							{
								return 'p.=';
							}
							else if($this->has_prefix($split_str[1], 'p'))
							{
								return $split_str[1];
							}
							else
							{
								return '';
							}
							break;							
					}
				}
				else
				{
					return '';
				}				
			}
			else
			{
				return '';
			}
		}

		private function KeepOutArrayOrNot($out_array)
		{
			// return out_array which are not empty for genes, coding, and amino_acid_change otherwise return false
			if (!empty($out_array['genes']) && !empty($out_array['coding']) && !empty($out_array['amino_acid_change']))
			{
				$out_array['run_id'] = $this->settings_array['run_id'];

				return $out_array;
			}
			else
			{
				return False;
			}
		}

		

	}
?>