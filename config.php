<?php
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);

	// backup dev db
	//  mysqldump -u urmc_app_user -p --databases urmc_reporter_dev_db > /var/www/html/devs/backup_urmc_reporter_dev_db_06_13_18.sql 

	// import into live db
	// mysql -u urmc_app_user -p urmc_reporter_db < backup_urmc_reporter_dev_db_06_13_18.sql

	// get session info and keep open for the least amount of time to reduce 
	// open session collisions
	session_start();
	$copy_session = $_SESSION;
	
	session_write_close(); 
 	
	define('SITE_TITLE', 'waza');
	
	define('APP_LOC', explode('?', $_SERVER['REQUEST_URI'])[0]);
	

	// redirect to https
	if (empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] === "off") 	
	{
	    $location = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
	    header('HTTP/1.1 301 Moved Permanently');
	    header('Location: ' . $location);
	    exit;
	}

	if (!defined('ROOT_URL'))
	{
		define('ROOT_URL',explode('/', $_SERVER['REQUEST_URI'])[1]);
	}

	$project_folder_name = 'waza';

	// set location of class dir & redirect address for dev vs live code
	if (ROOT_URL === 'devs')
	{
		define('CLASS_DIR', ($_SERVER['DOCUMENT_ROOT'].'/devs/'.$project_folder_name.'/classes/'));
		define('REDIRECT_URL', '/devs/'.$project_folder_name);
		define('DB_PASSWORD', 'DefaultPasswordMySQL123456'); // DefaultPasswordMySQL123456
		
		define('DB_NAME', $project_folder_name.'_dev');

	}
	elseif (ROOT_URL === $project_folder_name)
	{
		define('CLASS_DIR', ($_SERVER['DOCUMENT_ROOT'].'/'.$project_folder_name.'/classes/'));
		define('REDIRECT_URL', '/'.$project_folder_name);
		define('DB_PASSWORD', 'DefaultPasswordMySQL123456'); // DefaultPasswordMySQL123456
		define('DB_NAME', $project_folder_name);
	}

	define('DEFAULT_DATE_TIME', '1970-01-01 12:00:00');

	/*  Database Information - Required!!  */
	/* -- Configure the Variables Below --*/
	define('DB_HOST', 'localhost');
	define('DB_USER', 'urmc_app_user');

	/* Database Stuff, do not modify below this line */
	$conn = mysqli_connect(DB_HOST,DB_USER,DB_PASSWORD) or die ("Couldn't connect to server.");

	$db = mysqli_select_db($conn, DB_NAME) or die("Couldn't select database.");

	require_once('classes/class.utils.php');

	// Add every class in the class dir
	if($handle = opendir(CLASS_DIR))
	{
		while(false !== ($file = readdir($handle)))
		{
	    		if($file === '.' || $file === '..' || is_dir(CLASS_DIR.$file))
	    		{
	        		continue;
	        	}

			require_once(CLASS_DIR.$file);
	    }

    		closedir($handle);
	}

	$db = new DataBase($conn, md5('md5 code not active'), SITE_TITLE, ROOT_URL);

	// set ip
	$db->SetIP();

	// make a date functions object
	$dfs = new DateFuncs();

	// Make a utils object
	$utils = new Utils();

	// Make emailerHandler Object
	$email_init_array = array(
		'db'	=> 	$db
	);
	$email_utils = new EmailUtils($email_init_array);


	$extra_fields_utils = new ExtraFieldsUtils();

	// Get version history here because the footer uses VERSION on each page even login
	$versionArray = $db->listAll('version-history-desc');

	define('VERSION', $versionArray[0]['version']);

	$all_pdfs = $utils->getAllFilesInDir('pdfs');

	// define('USER_ID', 1);

	//////////////////////////////////////////////////
	// Ajax calls will come the utils folder.  These will not have a $_GET['page']
	// set page to ajax_call
	//////////////////////////////////////////////////
	if (in_array('utils', explode('/', $_SERVER['REQUEST_URI'])))
	{
		$_GET['page'] = 'ajax_call';
	}

	//////////////////////////////////////////////////
	// redirect if page is not defined to login page
	//////////////////////////////////////////////////
	else if (!isset($_GET['page']))
	{
		header('Location:'.REDIRECT_URL.'?page=login');
	}

	//////////////////////////////////////////////////
	// define page if page is login or password_reset
	//////////////////////////////////////////////////
	if ($_GET['page'] == 'login' || $_GET['page'] == 'password_reset')
	{
		$page = $_GET['page'];

		//////////////////////////////////////////////////////////////////
		// Automatic emails are triggered at logins and recorded in 
		// automatic_email_table.  It is done this way to only run this once
		// and I was unable to figure out how to run a chron job on this server
		//////////////////////////////////////////////////////////////////
		// $email_utils->runAutoEmailAlerts();
	}

	//////////////////////////////////////////////////
	// If page not equal to login or password_reset and $_SESSION[user]['user_empty'] 
	// redirect to login or  $_SESSION['user']['last_active_time'] empty
	//////////////////////////////////////////////////
	else if (($_GET['page'] != 'login' || $_GET['page'] != 'password_reset') && (empty($copy_session['user']['user_id']) ) ) 
	{
		header('Location:'.REDIRECT_URL.'?page=login&error=redirect_empty_user');	
	}

	//////////////////////////////////////////////////
	// If user has been inactive for more than 23 minutes.  
	// redirect user to login and log them out. 
	//////////////////////////////////////////////////
	else if ( time() - $copy_session['user']['last_active_time'] >= 1400)
	{
		$inactive_time = time() - $copy_session['user']['last_active_time'];
		$db->logoff();
		header('Location:'.REDIRECT_URL.'?page=login&inactive_time='.$inactive_time);		
	}

	else if (
				$_GET['page'] !== 'login' &&
				(
					!isset($copy_session['user']['site_title']) || 
					SITE_TITLE !== $copy_session['user']['site_title'] ||
								
					!isset($copy_session['user']['root_url']) ||
					ROOT_URL !== $copy_session['user']['root_url']
				)
			)
	{	
		$db->logoff();
		header('Location:'.REDIRECT_URL.'?page=login&error=redirect_wrong_app');
	}

	//////////////////////////////////////////////////
	// Set up environment to use app
	//////////////////////////////////////////////////
	else if(!empty($copy_session['user']['user_id']) && isset($_GET['page']))
	{
		////////////////////////////////////////////////////////////////
		// set page so correct page template and logic can be imported in
		// the index page
		////////////////////////////////////////////////////////////////
		$page = $_GET['page'];
		

		///////////////////////////////////////////////////////////////
		// load phpMussel-1
			// Designed to detect trojans, viruses, malware and other threats 
			// phpMussel activates when $_FILES not empty.			
		///////////////////////////////////////////////////////////////		  		
		require_once('/var/www/phpMussel-1/loader.php');

		////////////////////////////////////////////////////////////////
		// Set user id for application
		////////////////////////////////////////////////////////////////
		define('USER_ID', $copy_session['user']['user_id']);

		////////////////////////////////////////////////////////////////
		// Update last active time
		////////////////////////////////////////////////////////////////
		session_start();
		$_SESSION['user']['last_active_time'] = time();
		session_write_close(); 

		// get users permissions
		$user_permssions = $db->listAll('user-permissions', USER_ID);

		// for users that do not have any advanced permissions
		if (!isset($user_permssions[0]['permissions']))
		{
			$user_permssions = '';
		}
		else
		{

			$user_permssions = $user_permssions[0]['permissions'];
		}

	}
?>
